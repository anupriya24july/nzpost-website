# NZPost Web Frontend

This is the main website that is deployed. It acts as a parent project that encompasses and consumes shared React components from other projects. 

## Installing dependencies 
```bash
$ npm install
```

## Start the backend locally

Login to aws registry
```bash
$ $(aws ecr get-login --no-include-email --region ap-southeast-2)
```

Start up all backend services the first time
```bash
$ docker-compose up
```

If backend containers are available then just run
```bash
$ docker-compose start
```
## Running in development mode

```bash
$ npm start
```

## Building a release version
A release build optomises for a deployment environment by transpiling, minifying, and uglifying your codebase.

```bash
$ npm run build
```

