/**
 * @src https://swagger.io/docs/specification/api-host-and-base-path/
 */
const PARCEL_SERVICE_URL = "https://sit-tools.cf.nzpost.co.nz/api/parcel-service/";
export default {
  services: {
    trackingNumberCheck: {
      serverUrl: PARCEL_SERVICE_URL + "v1/trackingnumbercheck",
    },
    parcelAddress: {
      serverUrl: PARCEL_SERVICE_URL + "v1/parceladdress",
    },
    redeliveryDetails: {
      serverUrl: PARCEL_SERVICE_URL + "v1/redeliverydetails",
    },
    postshopLocator: {
      serverUrl: "https://sit-tools.cf.nzpost.co.nz/api/postshop-locations/v2",
    },
    suggestAddress: {
      serverUrl: "https://sit-tools.cf.nzpost.co.nz/api/notifications/address-suggestions",
    },
  },
};
