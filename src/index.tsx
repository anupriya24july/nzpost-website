/**
 * We use babel/polyfill to provide modern js support for old browsers e.g. Array.prototype.includes method which is used
 * https://babeljs.io/docs/en/babel-polyfill
 */
import "@babel/polyfill";
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {AppStore} from './redux/AppStore';
import App from "./App/App";
import jssPreset from "@material-ui/core/styles/jssPreset";
import {create} from "jss";
import createGenerateClassName from "@material-ui/core/styles/createGenerateClassName";
import JssProvider from 'react-jss/lib/JssProvider';
import {unregister} from "./registerServiceWorker";
import "./styles/index.scss";
// @ts-ignore
window.__forceSmoothScrollPolyfill__ = true
// @ts-ignore
import smoothscroll from 'smoothscroll-polyfill';

const generateClassName = createGenerateClassName();

// @ts-ignore
const jss = create({
  ...jssPreset(),
  // We define a custom insertion point that JSS will look for injecting the styles in the DOM.
  insertionPoint: document.getElementById('jss-insertion-point')
});

ReactDOM.render(
  <div>
    <JssProvider jss={jss} generateClassName={generateClassName}>
      <Provider store={AppStore}>
        <App/>
      </Provider>
    </JssProvider>
  </div>,
  document.getElementById('root') as HTMLElement
);

// TODO: We temporarily disable service worker to prevent the empty resources found for the legacy /apf site.
// Once we merge /apf to this then we need to
// registerServiceWorker();
unregister();

// requestAnimationFrame polyfill required for the smooth scrolling polyfill
require('raf/polyfill');

// kick off the polyfill!
smoothscroll.polyfill();
