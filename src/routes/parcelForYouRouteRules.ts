import {
  getTrackingNumber,
  selectDeliveryAddress,
  selectDeliveryAddressId,
  selectRedeliveryAddress,
  selectRedeliveryAddressId,
  getSelectedRedeliveryOption,
  selectContactDetails,
} from "../redux/selectors/parcelForYouSelectors";
import {AppStore} from "../redux/AppStore";
import {replace} from "connected-react-router";
import {trackingNumberCheckActions} from "../redux/actions/parcel-for-you/trackingNumberCheckActions";
import {paths} from "./routes";
import {reset} from "redux-form";
import {redeliveryOptionsActions} from "../redux/actions/parcel-for-you/redeliveryOptionsActions";
import {deliveryDetailsActions} from "../redux/actions/parcel-for-you/deliveryDetailsActions";
import {contactDetailsActions} from "../redux/actions/parcel-for-you/contactDetailsActions";
import {default as contactDetailsValidation} from "../components/ParcelForYou/ContactDetails/ContactDetailsForm/ContactDetailsFormValidation";

// reset the redux form
const resetForm = () => {
  AppStore.dispatch(reset("trackingNumberForm"));
};

// reset the values for trackingNumberCheckReducer on browser back
export function resetTrackingNumber(): boolean {
  try {
    resetForm();
    AppStore.dispatch(trackingNumberCheckActions.resetTrackingNumberCheck());
    AppStore.dispatch(redeliveryOptionsActions.resetRedeliveryDetails());
    AppStore.dispatch(deliveryDetailsActions.resetDeliveryDetails());
    AppStore.dispatch(contactDetailsActions.resetContactDetails());

    return true;
  } catch (e) {
    AppStore.dispatch(replace(paths.PARCEL_FOR_YOU));
    return false;
  }
}

// check for original delivery address page navigation
export function trackingNumberExists(): boolean {
  try {
    if (getTrackingNumber(AppStore.getState()) != "") {
      return true;
    }
    AppStore.dispatch(replace(paths.PARCEL_FOR_YOU));
    return false;
  } catch (e) {
    AppStore.dispatch(replace(paths.PARCEL_FOR_YOU));
    return false;
  }
}

// check for delivery details page navigation
export function deliveryAddressIdExists(): boolean {
  try {
    if (!!selectDeliveryAddressId(AppStore.getState()) && !!selectDeliveryAddress(AppStore.getState())) {
      return true;
    }
    AppStore.dispatch(replace(paths.PARCEL_FOR_YOU));
    return false;
  } catch (e) {
    AppStore.dispatch(replace(paths.PARCEL_FOR_YOU));
    return false;
  }
}

// check for new delivery details page navigation
export function newDeliveryAddressSelected(): boolean {
  try {
    if (getSelectedRedeliveryOption(AppStore.getState()) === 'differentAddress' && deliveryAddressIdExists()) {
      return true;
    }
    AppStore.dispatch(replace(paths.PARCEL_FOR_YOU));
    return false;
  } catch (e) {
    AppStore.dispatch(replace(paths.PARCEL_FOR_YOU));
    return false;
  }
}

// check if redelivery details exist
export function redeliveryAddressIdExists(): boolean {
  try {
    if (!!selectRedeliveryAddressId(AppStore.getState()) && !!selectRedeliveryAddress(AppStore.getState())) {
return true;
    }
    AppStore.dispatch(replace(paths.PARCEL_FOR_YOU));
    return false;
  } catch (e) {
    AppStore.dispatch(replace(paths.PARCEL_FOR_YOU));
    return false;
  }
}

// check for contact details page navigation
export function redeliveryOptionsExists(): boolean {
  try {
    if (deliveryAddressIdExists() || redeliveryAddressIdExists()) {
      return true;
    }
    AppStore.dispatch(replace(paths.PARCEL_FOR_YOU));
    return false;
  } catch (e) {
    AppStore.dispatch(replace(paths.PARCEL_FOR_YOU));
    return false;
  }
}

// check for contact details page navigation
export function contactDetailsExists(): boolean {
  try {
    const errors = contactDetailsValidation(selectContactDetails(AppStore.getState()));
    if (!errors.legnth) {
      return true;
    }
    AppStore.dispatch(replace(paths.PARCEL_FOR_YOU));
    return false;
  } catch (e) {
    AppStore.dispatch(replace(paths.PARCEL_FOR_YOU));
    return false;
  }
}


export const successPageConditions = [
  trackingNumberExists,
  deliveryAddressIdExists,
  redeliveryOptionsExists,
  contactDetailsExists
];