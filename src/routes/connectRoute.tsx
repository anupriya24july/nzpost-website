import * as React from "react";
import "raf/polyfill";
// @ts-ignore
import smoothscroll from "smoothscroll-polyfill";

// kick off the polyfill!
smoothscroll.polyfill();

/**
 * This is a workaround to prevent Route from rendering the component upon componentWillReceiveProps.
 * The component will always be rendered by ConnectedRouter
 *
 * https://github.com/ReactTraining/react-router/issues/5738
 * @param WrappedComponent
 */
// FIXME: Use correct types
export default function connectRoute(WrappedComponent: any) {
  return class extends React.Component {
    constructor(props: any) {
      super(props);
    }

    shouldComponentUpdate(nextProps: any) {
      // @ts-ignore
      return nextProps.location !== this.props.location;
    }

    render() {
      window.scrollTo(0, 0);
      return <WrappedComponent {...this.props} />;
    }
  };
}