import * as React from "react";

export const withPreConditions = (conditions: Function[], WrappedComponent: any) => {
  return () => {
    try {
      for (let condition of conditions) {
        if (!condition()) {
          return null;
        }
      }
      return <WrappedComponent/>;
    } catch (e) {
      console.log("One of the preconditions failed to execute!", e);
      return null;
    }
  }
};

export default withPreConditions;
