import SuggestAddressPage from "../pages/SuggestAddressPage";
import SubmittedAddressPage from "../pages/SubmittedAddressPage";
import ErrorPage from "../pages/ErrorPage/ErrorPage";
import PostshopLocatorPage from "../pages/PostshopLocatorPage";
import {ParcelForYouPage} from "../pages/ParcelForYou/ParcelForYouPage";
import {ParcelForYouDetailsPage} from "../pages/ParcelForYou/ParcelForYouDetailsPage";
import ParcelRedeliveryOptionsPage from "../pages/ParcelForYou/ParcelRedeliveryOptionsPage";
import ParcelRedeliveryAddressPage from "../pages/ParcelForYou/ParcelRedeliveryAddressPage";
import ParcelRedeliveryDateTimePage from "../pages/ParcelForYou/ParcelRedeliveryDateTimePage";
import ParcelForYouContactDetailsPage from "../pages/ParcelForYou/ParcelForYouContactDetailsPage";
import ParcelRedeliverySuccessPage from "../pages/ParcelForYou/ParcelRedeliverySuccessPage";

import HomePage from "../pages/HomePage/HomePage";

export {
  HomePage,
  SuggestAddressPage,
  ErrorPage,
  SubmittedAddressPage,
  PostshopLocatorPage,
  ParcelForYouDetailsPage,
  ParcelRedeliveryAddressPage,
  ParcelRedeliveryOptionsPage,
  ParcelRedeliveryDateTimePage,
  ParcelRedeliverySuccessPage,
  ParcelForYouPage,
  ParcelForYouContactDetailsPage,
};