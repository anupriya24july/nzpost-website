import * as React from "react";

/**
 * Generic wrapper for providing routing conditions for rendering components
 *
 * @param condition
 * @param WrappedComponent the component that should be rendered if the condition passes
 */
// FIXME: Use correct types
const withPreAction = (action: Function, WrappedComponent: any) => {
  return (props: any) => {
    try {
      action();
      return <WrappedComponent {...props} />;
    } catch (e) {
      console.log("Preaction failed to execute!", e);
      return null;
    }
  }
};

export default withPreAction;
