import {Route, Switch} from "react-router";
import '../App/App.scss'
import * as React from "react";
import {AppStore} from "../redux/AppStore";
import {getFullName} from "../redux/selectors/suggestAddressSelectors";
import {replace} from "connected-react-router";
import withPreCondition from "./withPreCondition";
import withPreConditions from "./withPreConditions";
import withPreAction from "./withPreAction";
import {reset} from "redux-form";
import connectRoute from "./connectRoute";
import {
  ErrorPage,
  HomePage,
  ParcelForYouDetailsPage,
  ParcelForYouPage,
  ParcelRedeliveryAddressPage,
  ParcelRedeliveryDateTimePage,
  ParcelRedeliveryOptionsPage,
  ParcelRedeliverySuccessPage,
  PostshopLocatorPage,
  SubmittedAddressPage,
  SuggestAddressPage,
  ParcelForYouContactDetailsPage
} from "./external";
import {
    deliveryAddressIdExists,
    redeliveryOptionsExists,
    resetTrackingNumber,
    trackingNumberExists,
    newDeliveryAddressSelected,
    successPageConditions
} from "./parcelForYouRouteRules";
import PaymentRequestPage from "../pages/PaymentRequestPage";

export const paths = {
  HOME: "/",
  SUGGEST_ADDRESS: "/suggest-address",
  SUGGEST_ADDRESS_SUBMITTED: "/suggest-address/submitted",
  POSTSHOP_LOCATOR: "/postshop-locator",
  /**
   * Naming conventions for P4U
   * PARCEL_{CAPITALIZED_COMPONENT_NAME}: /parcel-for-you/{dashed-component-name}
   * see examples below
   */
  PARCEL_FOR_YOU: "/parcel-for-you",
  PARCEL_DELIVERY_DETAILS: "/parcel-for-you/delivery-details",
  PARCEL_REDELIVERY_OPTIONS: "/parcel-for-you/redelivery-options",
  PARCEL_REDELIVERY_ADDRESS: "/parcel-for-you/redelivery-address",
  PARCEL_REDELIVERY_DATETIME: "/parcel-for-you/redelivery-datetime",
  PARCEL_REDELIVERY_CONTACT_DETAILS: "/parcel-for-you/contact-details",
  PARCEL_REDELIVERY_SUCCESS: "/parcel-for-you/redelivery-success",
  SERVER_ERROR: "/oops",

  PAYMENT_PROTOTYPE: "/payment-prototype/request-page",
};

const validateFormExists = (): boolean => {
  try {
    getFullName(AppStore.getState());
    return true;
  } catch (e) {
    AppStore.dispatch(replace(paths.SUGGEST_ADDRESS));
    return false;
  }
};

const resetForm = () => {
  // reset form values
  // FIXME: make this form name a constant
  AppStore.dispatch(reset("suggestAddress"));
};

// parcel for you functions for user journey flow

/**
 * Default routes
 * IMPORTANT: Please wrap all your page in connectRoute which will add a few common functionalities such as:
 * - Stop Switch from rendering the component on deprecated stage componentWillReceiveProps
 * - Scroll window to top of the page
 *
 * @author MaxT
 * @author NickT
 */

const routes =
  <Switch>
    <Route exact path={paths.HOME} component={HomePage}/>
    <Route exact path={paths.SUGGEST_ADDRESS} component={withPreAction(resetForm, connectRoute(SuggestAddressPage))}/>
    <Route exact path={paths.SUGGEST_ADDRESS_SUBMITTED} component={withPreCondition(validateFormExists, connectRoute(SubmittedAddressPage))}/>
    <Route exact path={paths.POSTSHOP_LOCATOR} component={withPreAction(resetForm, connectRoute(PostshopLocatorPage))}/>
    <Route exact path={paths.PARCEL_FOR_YOU} component={withPreAction(resetTrackingNumber, connectRoute(ParcelForYouPage))}/>
    <Route exact path={paths.PARCEL_FOR_YOU} component={withPreAction(resetTrackingNumber, connectRoute(ParcelForYouPage))}/>
    <Route exact path={paths.PARCEL_DELIVERY_DETAILS} component={withPreCondition(trackingNumberExists, connectRoute(ParcelForYouDetailsPage))}/>
    <Route exact path={paths.PARCEL_REDELIVERY_OPTIONS} component={withPreCondition(deliveryAddressIdExists, connectRoute(ParcelRedeliveryOptionsPage))}/>
    <Route exact path={paths.PARCEL_REDELIVERY_ADDRESS} component={withPreCondition(newDeliveryAddressSelected, ParcelRedeliveryAddressPage)}/>
    <Route exact path={paths.PARCEL_REDELIVERY_DATETIME} component={ParcelRedeliveryDateTimePage}/>
    <Route exact path={paths.PARCEL_REDELIVERY_CONTACT_DETAILS} component={withPreCondition(redeliveryOptionsExists, connectRoute(ParcelForYouContactDetailsPage))}/>
    <Route exact path={paths.PARCEL_REDELIVERY_SUCCESS} component={withPreConditions(successPageConditions, connectRoute(ParcelRedeliverySuccessPage))}/>
    <Route exact path={paths.SERVER_ERROR} component={connectRoute(ErrorPage)}/>
    <Route exact path={paths.PAYMENT_PROTOTYPE} component={connectRoute(PaymentRequestPage)}/>
  </Switch>
;

export default routes;
