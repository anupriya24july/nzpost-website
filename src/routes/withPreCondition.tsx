import * as React from "react";

/**
 * Generic wrapper for providing routing conditions for rendering components
 *
 * @param condition
 * @param WrappedComponent the component that should be rendered if the condition passes
 */
// FIXME: Use correct types
const withPreCondition = (condition: Function, WrappedComponent: any) => {
  return () => {
    try {
      if (condition()) {
        return <WrappedComponent/>;
      }

      return null;
    } catch (e) {
      console.log("Precondition failed to execute!", e);
      return null;
    }
  };
};

export default withPreCondition;
