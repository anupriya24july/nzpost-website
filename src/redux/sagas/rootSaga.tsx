import {all, fork} from "redux-saga/effects";
import suggestAddressSaga from "./suggestAddressSaga";
import postshopLocatorSaga from "./postshop-locator/postshopLocatorSaga";
import trackingNumberCheckSaga from "./parcel-for-you/trackingNumberCheckSaga";
import parcelForYouSaga from "./parcel-for-you/parcelForYouSaga";

/**
 * All application sagas
 */
export default function* rootSaga() {
  yield all([
    fork(suggestAddressSaga),
    fork(trackingNumberCheckSaga),
    fork(postshopLocatorSaga),
    fork(parcelForYouSaga),
  ])
}