import {all, fork} from "redux-saga/effects";
import suggestionsSaga from "./suggestionsSaga";
import resultsSaga from "./resultsSaga";

/**
 * Export all postshop locator related sagas as a bundle
 */
export default function* postshopLocatorSaga() {
  yield all([fork(suggestionsSaga), fork(resultsSaga)])
}