import {all, call, cancel, fork, put, take, takeLatest} from "redux-saga/effects";
import {
  FetchRequestedAction,
  locatorSuggestionActions,
  LocatorSuggestionsActionType
} from "../../actions/postshop-locator/locatorSuggestionActions";
import {postshopLocatorService} from "../../../services/postshopLocatorService";
import {Suggestion} from "../../../models/AddressResultCollection";
import {push} from "connected-react-router";
import {paths} from "../../../routes/routes";

function* loadBackgroundSuggestions(fetchAction: FetchRequestedAction) {
  try {
    // Start loading
    yield put(locatorSuggestionActions.suggestionsLoadingAction());

    // Fetch data from API(s)
    const suggestions: Suggestion[] = yield call(postshopLocatorService.getSuggestions, fetchAction.payload.value);

    // Load results
    yield put(locatorSuggestionActions.setSuggestionsAction(suggestions));

    // Set loading to finished
    yield put(locatorSuggestionActions.suggestionsLoadedAction());
  } catch (e) {
    yield put(push(paths.SERVER_ERROR));
  }
}

/**
 * Suggest address form saga middleware layer
 *
 * @author MaxT
 * @author NickT
 */
function* findSuggestionResults(fetchAction: FetchRequestedAction) {
  const bgSuggestionsLoadingTask = yield fork(loadBackgroundSuggestions, fetchAction);

  yield take(LocatorSuggestionsActionType.SEARCH_CANCELLED);
  yield cancel(bgSuggestionsLoadingTask);
}

/**
 * React to suggest address form submission action and call submit form actions
 */
function* watchFetchRequest() {
  yield takeLatest(LocatorSuggestionsActionType.FETCH_REQUESTED, findSuggestionResults);
}

export default function* suggestionsSaga() {
  yield all([fork(watchFetchRequest)])
}