import {all, call, fork, put, select, takeEvery, takeLatest} from "redux-saga/effects";
import {push} from "connected-react-router";

import {LoadSelectedAction, locatorResultsActions, LocatorResultsActionType, QuickSearchAction, SearchAction} from "../../actions/postshop-locator/locatorResultsActions";
import {GeolocationSearchAction, locatorMapActions, LocatorMapActionType, MapExtentChangedAction} from "../../actions/postshop-locator/locatorMapActions";
import {PostshopLocation} from "../../../models/PostshopLocation";
import {postshopLocatorService} from "../../../services/postshopLocatorService";
import {locatorSuggestionActions} from "../../actions/postshop-locator/locatorSuggestionActions";
import {ApplicationState} from "../../AppStore";
import {
  locationsActions,
  paths,
  pklStateMachineActions,
  postshopLocatorUIActions,
  STATE_TYPE_DEFAULT,
  STATE_TYPE_EXACT_LOCATION,
  STATE_TYPE_EXTENT_LOCATIONS,
  STATE_TYPE_NEARBY_LOCATIONS,
} from "./external";


/**
 * Main search function invoked from selection or key press
 * @param searchAction
 */
function* search(searchAction: SearchAction) {
  // do not search if the query is unchanged
  const previousSearchQuery = yield select((state: ApplicationState) => state.postshopLocator.resultsState.searchQuery);
  if (searchAction.payload.value === "" || (previousSearchQuery === searchAction.payload.name)) {
    return;
  }
  let keyword = "";
  const selectedSuggestion = yield select((state: ApplicationState) => state.postshopLocator.suggestionState.selectedSuggestion);
  if (selectedSuggestion.address == searchAction.payload.value) {
    keyword = searchAction.payload.value;
  } else if (selectedSuggestion.name == searchAction.payload.value && selectedSuggestion.address != null) {
    keyword = selectedSuggestion.address;
  } else {
    keyword = searchAction.payload.value;
  }

  // Stop loading suggestions if in progress
  yield put(locatorSuggestionActions.searchCancelledAction());

  // Clear current results
  yield put(locatorResultsActions.clearResultsRequested());

  // Start loading
  yield put(locatorResultsActions.resultsLoadingAction());
  let locations: PostshopLocation[];
  try {
    // Fetch location data
    locations = yield call(
      postshopLocatorService.getLocationsByKeyword,
      {value: keyword}
    );


    if (locations.length < 1) {
      yield put(postshopLocatorUIActions.setUIStateSearchLocationsNotFound());
      // TODO: Do we need the next line ?
      yield put(locatorResultsActions.resultsNotFoundAction());

      // Replace all locations
      yield put(locationsActions.replaceLocations({locations: []}));
      return;
    }

    // Clear location suggestions
    yield put(locatorSuggestionActions.clearSuggestionsRequested());
    let selectedSuggestionMatched = locations.find((location: PostshopLocation) => location.address === selectedSuggestion.address && location.name === selectedSuggestion.name)
    // Set locations
    if (selectedSuggestionMatched) {
      // get exact location if required
      const exactLocations = yield new Array(selectedSuggestionMatched);

      // Change PostshopLocator UI state to EXACT_LOCATION
      yield put(postshopLocatorUIActions.setUIStateExactLocation());

      yield put(locatorResultsActions.setResultsAction(exactLocations));
      // Replace all locations
      yield put(locationsActions.replaceLocations({locations: exactLocations}));

    } else {
      // Change PostshopLocator UI state to NEARBY_LOCATIONS
      yield put(postshopLocatorUIActions.setUIStateNearbyLocations());

      yield put(locatorResultsActions.setResultsAction(locations));
      // Replace all locations
      yield put(locationsActions.replaceLocations({locations}));
    }

    // Set query
    yield put(locatorResultsActions.setSearchQuery(searchAction.payload.value));

  } catch (e) {
    yield put(push(paths.SERVER_ERROR));
  }
  // Set loading to finished
  yield put(locatorResultsActions.resultsLoadedAction());
}

/**
 * Load a specific suggestion as result
 * @param loadSelectedAction
 */
function* loadSelected(loadSelectedAction: LoadSelectedAction) {
  const suggestion = loadSelectedAction.payload.suggestion;
  // Save selected suggestion
  yield put(locatorSuggestionActions.setSelectedSuggestionAction(suggestion));
  if (suggestion.address) {
    yield put(locatorResultsActions.searchRequested(suggestion.address, suggestion.name, true));
  } else {
    yield put(locatorResultsActions.searchRequested(suggestion.name, false));
  }
}

/**
 * Quick search conducts a standard search and sets the searchfield value to the quick search value
 * @param quickSearchAction
 */
function* quickSearch(quickSearchAction: QuickSearchAction) {
  yield put(locatorSuggestionActions.setSearchFieldValueAction(quickSearchAction.payload.value));
  yield put(locatorResultsActions.searchRequested(quickSearchAction.payload.value));
}

function* searchRequested() {
  yield takeEvery(LocatorResultsActionType.SEARCH, search)
}

function* quickSearchRequested() {
  yield takeEvery(LocatorResultsActionType.QUICK_SEARCH, quickSearch)
}

function* loadSelectedRequested() {
  yield takeEvery(LocatorResultsActionType.LOAD_SELECTED, loadSelected)
}

function* loadOnMapExtentChange(mapExtentChangedAction: MapExtentChangedAction) {
  /**
   * @idea Actions to add:
   *       FETCH_MAP_EXTENT_LOCATIONS_REQUESTED
   *       FETCH_MAP_EXTENT_LOCATIONS_SUCCEEDED
   *       FETCH_MAP_EXTENT_LOCATIONS_FAILED
   */
  const {lat1, lat2, lng1, lng2} = mapExtentChangedAction.payload;
  const value = {
    latitude1: lat1,
    latitude2: lat2,
    longitude1: lng1,
    longitude2: lng2,
  };

  try {
    // Fetch location data
    const locations: PostshopLocation[] = yield call(
      postshopLocatorService.getLocationsByMapExtent,
      {value},
    );

    if (locations.length < 1) {
      yield put(postshopLocatorUIActions.setUIStateExtentLocationNotFound());
      // TODO: Check if actions below are required indeed
      yield put(locatorResultsActions.resultsNotFoundAction());
      yield put(locatorMapActions.loadLocatorMarkers([]));
      yield put(locationsActions.replaceLocations({locations: []}));
    } else {
      yield put(postshopLocatorUIActions.setUIStateExtentLocation());
      // TODO: Check if action below is required
      yield put(locatorResultsActions.setResultsAction(locations));
      // Replace all locations
      yield put(locationsActions.replaceLocations({locations}));
    }

  } catch
    (e) {
    yield put(push(paths.SERVER_ERROR));
  }

  // Set loading to finished
  yield put(locatorResultsActions.resultsLoadedAction());
}


/**
 * This method is responsible for switching postshop locator ui states
 * and passing control further when appropriate.
 *
 * @idea This logic should be separated to a distinct saga with the same responsibility.
 *       That saga should communicate with the existing sagas by means of actions
 *
 *       Critique: Is this a good idea ?
 *
 *       TODO: Find more info on the subject:
 *             "sagas messaging each other by publishing actions"
 *
 * @param mapExtentChangedAction
 */
function* handleMapExtentChange(mapExtentChangedAction: MapExtentChangedAction) {
  // TODO: Refactor to get this value from a selector
  const stateType = yield select((state: ApplicationState) => state.postshopLocator.uiState.stateType);
  const {zoom} = mapExtentChangedAction.payload;
  const maxZoom = 5; // TODO: Get maxZoom from the single source of truth

  switch (stateType) {
    case STATE_TYPE_DEFAULT:
      // When max zoom level of the map is reached do nothing
      if (zoom === maxZoom) {
        break;
      }
      // Change PostshopLocator UI state to EXTENT_LOCATIONS
      yield put(postshopLocatorUIActions.setUIStateExtentLocation());
      // TODO: Think about moving loadOnMapExtentChange logic to setUIStateExtentLocation
      yield call(loadOnMapExtentChange, mapExtentChangedAction);
      break;

    case STATE_TYPE_EXACT_LOCATION:
      // Do nothing
      break;

    case STATE_TYPE_EXTENT_LOCATIONS:
      // When max zoom level of the map is reached
      // change PostshopLocator UI state to DEFAULT
      if (zoom === maxZoom) {
        yield put(pklStateMachineActions.setStateDefault());
        // TODO: Think about moving replaceLocations logic to setUIStateDefault
        yield put(locationsActions.replaceLocations({locations: []}));
        break;
      }
      // Fetch locations for all zoom levels except the maximum one
      yield call(loadOnMapExtentChange, mapExtentChangedAction);
      break;

    case STATE_TYPE_NEARBY_LOCATIONS:
      // Change PostshopLocator UI state to EXTENT_LOCATIONS
      yield put(postshopLocatorUIActions.setUIStateExtentLocation());
      // TODO: Think about moving loadOnMapExtentChange logic to setUIStateExtentLocation
      yield call(loadOnMapExtentChange, mapExtentChangedAction);
      break;

    default:
      yield call(loadOnMapExtentChange, mapExtentChangedAction);
  }
}

function* watchMapExtentChanged() {
  yield takeLatest(LocatorMapActionType.MAP_EXTENT_CHANGED, handleMapExtentChange)
}

function* loadGeolocationResult(geolocationSearchAction: GeolocationSearchAction) {
  const {latitude, longitude} = geolocationSearchAction.payload;
  const value = {
    latitude: latitude,
    longitude: longitude
  };

  let query = "type=NEARBY";
  query += "&value=" + encodeURIComponent(JSON.stringify(value));
  query += "&max=5"
  try {
    // Fetch location data
    const locations: PostshopLocation[] = yield call(postshopLocatorService.getLocationsByGeolocation, query);

    if (locations.length < 1) {
      yield put(locatorResultsActions.resultsNotFoundAction());
      yield put(locatorMapActions.loadLocatorMarkers([]));
      return;
    }

    yield put(postshopLocatorUIActions.setUIStateNearbyLocations());
    // Replace all locations
    yield put(locationsActions.replaceLocations({locations}));
    yield put(locatorResultsActions.setResultsAction(locations));

  } catch (e) {
    yield put(push(paths.SERVER_ERROR));
  }
  // Set loading to finished
  yield put(locatorResultsActions.resultsLoadedAction());
}

function* loadGeolocationSearch() {
  yield takeLatest(LocatorMapActionType.GEOLOCATION_SEARCH, loadGeolocationResult)
}

export default function* resultsSaga() {
  yield all([
    fork(searchRequested),
    fork(loadSelectedRequested),
    fork(quickSearchRequested),
    fork(watchMapExtentChanged),
    fork(loadGeolocationSearch),
  ]);
}