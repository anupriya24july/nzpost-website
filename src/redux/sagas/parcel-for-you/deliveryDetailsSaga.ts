import {all, call, fork, put, select, takeEvery, takeLatest} from "redux-saga/effects";
import {
  AddressSuggestionsFetchRequested,
  ApplicationState,
  deliveryDetailsActions,
  DeliveryDetailsActionType,
  parcelForYouService,
  paths,
  selectDeliveryAddressSearchSuggestions,
  selectDeliveryAddress,
  selectDeliveryAddressId,
  SubmitRequested,
  toastNotificationActions,
} from "./external";
import {push} from "connected-react-router";
import {AddressChanged} from "src/redux/actions/parcel-for-you/deliveryDetailsActions";

export const mapAddressSuggestions = (suggestions: Array<any>): Array<any> =>
  suggestions.map(({address_id, full_address}) =>
    ({fullAddress: full_address, id: address_id}));

// This is a helper function reducing boilerplate code
export const generateJSendError = (err: Error) => ({
  status: "error",
  message: err.message,
  data: {
    name: err.name,
  }
});

export type JSend = {
  status: string,
  data?: any,
  code?: string,
  message?: string
}

// Helper handler function
export const handleJSendByType = (
  jsend: JSend,
  successHandler: Function,
  failHandler: Function,
  errorHandler: Function,
) => {
  switch (jsend.status) {
    case "success":
      return successHandler(jsend);

    case "fail":
      return failHandler(jsend);

    case "error":
      return errorHandler(jsend);
  }
}

// This function is implemented to handle the error JSend status the same way
export const handleJSendByTypePatched = (
  jsend: JSend,
  successHandler: Function,
  failHandler: Function,
) =>
  handleJSendByType(
    jsend,
    successHandler,
    failHandler,
    function* () {
      yield put(push(paths.SERVER_ERROR))
    }
  );

export function* fetchAddressSuggestions(addressSuggestionFetchRequestedAction: AddressSuggestionsFetchRequested) {
  try {
    const addressQuery = addressSuggestionFetchRequestedAction.payload.value;
    const response = yield call(parcelForYouService.fetchAddressSuggestions, {value: addressQuery});
    return response;
  } catch (err) {
    return generateJSendError(err);
  }
}

export function* validateAddress(action: any) {
  try {
    const address = yield select((state: ApplicationState) => selectDeliveryAddress(state));
    const response = yield call(parcelForYouService.validateAddress, {value: address});
    return response;
  } catch (err) {
    return generateJSendError(err);
  }
}

export function* handleAddressChange(action: AddressChanged) {
  yield put(toastNotificationActions.dismissAll());
}

export function* handleAddressSuggestionsFetchRequest(action: AddressSuggestionsFetchRequested) {
  const jsendResponse = yield call(fetchAddressSuggestions, action);

  yield call(
    handleJSendByTypePatched,
    jsendResponse,
    // Success path
    function* ({data}: { data: { addresses: Array<any> } }) {
      const suggestions = mapAddressSuggestions(data.addresses);
      yield put(deliveryDetailsActions.addressSuggestionsFetchSucceeded(
        suggestions,
      ));
    },
    () => {/* do nothing */
    }
  );

  // Handle case when a user changed address input to one that matches valid one
  // and hit enter or next without selected an andress from a list
  const address = yield select((state: ApplicationState) =>
    selectDeliveryAddress(state));
  const addressSearchSuggestions = yield select((state: ApplicationState) =>
    selectDeliveryAddressSearchSuggestions(state));

  let matchingAddressId = "";

  addressSearchSuggestions.forEach((addressSuggestion: any) => {
    if (addressSuggestion.fullAddress === address) {
      matchingAddressId = addressSuggestion.id;
    }
  });

  if (matchingAddressId) {
    yield put(deliveryDetailsActions.addressSelected({id: matchingAddressId}));
  }
}

export function* handleSubmitRequested(action: SubmitRequested) {
  const jsendResponse = yield call(validateAddress, action);

  yield call(
    handleJSendByTypePatched,
    jsendResponse,
    // Success path
    function* () {
      // !!! BREACH OF SEPARATION OF CONCERNS between DUMB FE and SMART BE !!!
      // Workaround for handling the case when we don`t have a matching address 
      const addressId = yield select((state: ApplicationState) => selectDeliveryAddressId(state));
      if (!addressId) {
        return yield put(toastNotificationActions.toastError({
          message: "Please enter the original delivery address for this parcel and select an option from the provided list."
        }));
      }
      // Workaround END

      yield put(push(paths.PARCEL_REDELIVERY_OPTIONS));
    },
    // Fail path
    function* ({data}: { data: { address: string } }) {
      yield put(toastNotificationActions.toastError({message: data.address}));
    }
  );
}

export function* watchAddressSuggestionsFetchRequest() {
  yield takeLatest(
    DeliveryDetailsActionType.ADDRESS_SUGGESTIONS_FETCH_REQUESTED,
    handleAddressSuggestionsFetchRequest,
  );
}

export function* watchSubmitRequest() {
  yield takeLatest(
    DeliveryDetailsActionType.SUBMIT_REQUESTED,
    handleSubmitRequested,
  )
}

export function* watchAddressChange() {
  yield takeEvery(
    DeliveryDetailsActionType.ADDRESS_CHANGED,
    handleAddressChange,
  );
}

export default function* deliveryDetailsSaga() {
  yield all([
    fork(watchAddressChange),
    fork(watchAddressSuggestionsFetchRequest),
    fork(watchSubmitRequest),
  ]);
}