import {all, call, fork, put, select, takeLatest} from "redux-saga/effects";
import {ApplicationState, parcelForYouService, paths, selectDeliveryAddressId, SubmitRequested, toastNotificationActions,} from "./external";
import {push} from "connected-react-router";
import {generateJSendError, JSend} from "./deliveryDetailsSaga";
import {getSelectedRedeliveryOption, getTrackingNumber} from "../../selectors/parcelForYouSelectors";
import {redeliveryOptionsActions, RedeliveryOptionsActionType} from "../../actions/parcel-for-you/redeliveryOptionsActions";


// Helper handler function
export const handleJSendByType = (
  jsend: JSend,
  successHandler: Function,
  failHandler: Function,
  errorHandler: Function,
) => {
  switch (jsend.status) {
    case "success":
      return successHandler(jsend);

    case "fail":
      return failHandler(jsend);

    case "error":
      return errorHandler(jsend);
  }
}
// This function is implemented to handle the error JSend status the same way
export const handleJSendByTypePatched = (
  jsend: JSend,
  successHandler: Function,
  failHandler: Function,
) =>
  handleJSendByType(
    jsend,
    successHandler,
    failHandler,
    function* () {
      yield put(push(paths.SERVER_ERROR))
    }
  );

export function* getRedeliveryDetails(action: any) {
  try {
    const trackingNumber = yield select((state: ApplicationState) => getTrackingNumber(state));
    const selectedAddressId = yield select((state: ApplicationState) => selectDeliveryAddressId(state));
    const response = yield call(parcelForYouService.getRedeliveryDetails, {trackingNumber: trackingNumber, originalAddressId: selectedAddressId, newAddressId: selectedAddressId});
    return response;
  } catch (err) {
    return generateJSendError(err);
  }
}

export function* handleSubmitRequested(action: SubmitRequested) {

  const selectedOption = yield select((state: ApplicationState) => getSelectedRedeliveryOption(state));

  switch (selectedOption) {
    case 'sameAddress':
      const jsendResponse = yield call(getRedeliveryDetails, action);
      yield call(
        handleJSendByTypePatched,
        jsendResponse,
        // Success path
        function* ({data}: { data: { redeliveryDetails: Array<any> } }) {
          yield put(redeliveryOptionsActions.setRedeliveryDetails(
            data.redeliveryDetails,
          ));
        },
        // Fail path
        function* ({data}: { data: { redeliveryDetails: string } }) {
          yield put(toastNotificationActions.toastError({message: data.redeliveryDetails}));
        }
      );
      //
      yield put(push(paths.PARCEL_REDELIVERY_CONTACT_DETAILS));
      break;

    case 'differentAddress':
      yield put(push(paths.PARCEL_REDELIVERY_ADDRESS));
      break;
  }
}

export function* watchSubmitRequest() {
  yield takeLatest(
    RedeliveryOptionsActionType.SUBMIT_OPTION,
    handleSubmitRequested,
  )
}

export default function* redeliveryOptionsSaga() {
  yield all([
    fork(watchSubmitRequest),
  ]);
}