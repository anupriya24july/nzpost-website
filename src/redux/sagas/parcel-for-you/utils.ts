import {put} from "redux-saga/effects";
import {push} from "connected-react-router";
import {handleJSendByType, JSend, paths} from "./external";

// This function is implemented to handle the error JSend status the same way
export const handleJSendByTypePatched = (
  jsend: JSend,
  successHandler: Function,
  failHandler: Function,
) =>
  handleJSendByType(
    jsend,
    successHandler,
    failHandler,
    function* () {
      yield put(push(paths.SERVER_ERROR))
    }
  );