import {all, fork, put, takeEvery} from "redux-saga/effects";
import {toastNotificationActions} from "./external";
import deliveryDetailsSaga from "./deliveryDetailsSaga";
import redeliveryOptionsSaga from "./redeliveryOptionsSaga";
import redeliveryAddressSaga from "./redeliveryAddressSaga";
import contactDetailsSaga from "./contactDetailsSaga";

export function* handleRouterLocationChange() {
  yield put(toastNotificationActions.dismissAll());
}

export function* watchForRouterLocationChange() {
  // This implemented to prevent toast notification retaining after browsing to another P4U page
  // TODO: Critique this workaround
  yield takeEvery("@@router/LOCATION_CHANGE", handleRouterLocationChange);
}

/**
 * Export all postshop locator related sagas as a bundle
 */
export default function* postshopLocatorSaga() {
  yield all([
    fork(deliveryDetailsSaga),
    fork(redeliveryAddressSaga),
    fork(redeliveryOptionsSaga),
    fork(contactDetailsSaga),
    fork(watchForRouterLocationChange),
  ]);
}