import {all, call, fork, put, select, takeEvery, takeLatest} from "redux-saga/effects";
import {push} from "connected-react-router";

import {parcelForYouService, paths, toastNotificationActions, trackingNumberCheckActions, TrackingNumberCheckActionType, TrackingNumberCheckRequestedAction,} from "./external";
import {IValidation} from "../../reducers/parcel-for-you/trackingNumberCheckReducer";
import {ApplicationState} from "../../AppStore";
import {redeliveryOptionsActions} from "../../actions/parcel-for-you/redeliveryOptionsActions";

export function* checkTrackingNumber(trackingNumberCheckRequestedAction: TrackingNumberCheckRequestedAction) {
  try {
    const trackingNumber = yield select((state: ApplicationState) => state.parcelForYou.trackingNumberCheck.trackingNumber);
    const response = yield call(
      parcelForYouService.checkTrackingNumber,
      {value: trackingNumber}
    );

    switch (response.status) {
      case "success":
        yield put(trackingNumberCheckActions.succeedTrackingNumberCheck({
          number: trackingNumber
        }));
        yield put(redeliveryOptionsActions.setRedeliveryOptionsRequested({
          redeliveryOptions: response.data
        }));

        yield put(push(paths.PARCEL_DELIVERY_DETAILS));
        break;

      case "fail":
        yield call(handleTrackingNumberCheckFail, {
          message: response.data.message,
          code: response.code,
        });
        break;

      case "error":
        throw new Error(response.message);

      default:
        // Navigate to new page
        yield put(push(paths.PARCEL_DELIVERY_DETAILS));
        throw new Error(`Unknown response status : ${response.status}`);
    }
  } catch (err) {
    console.error(err);
    yield put(push(paths.SERVER_ERROR));
  }
}

export function* handleTrackingNumberCheckFail({message, code}: IValidation) {
  const ERR_VALID = "ERR_VALID";
  const ERR_ELIGIBLE = "ERR_ELIGIBLE";

  switch (code) {
    case ERR_VALID:
      yield put(trackingNumberCheckActions.failTrackingNumberCheck({message, code}));
      break;

    case ERR_ELIGIBLE:
      yield put(toastNotificationActions.toastError({message}));
      break;

    default:
      yield put(trackingNumberCheckActions.failTrackingNumberCheck({message, code}));
  }
}

export function* handleTrackingNumberChange() {
  yield put(toastNotificationActions.dismissAll());
}

export function* handleTrackingNumberCheckRequest(action: TrackingNumberCheckRequestedAction) {
  yield put(toastNotificationActions.dismissAll());
  yield call(checkTrackingNumber, action);
}

export function* watchTrackingNumberChange() {
  yield takeEvery(
    TrackingNumberCheckActionType.TRACKING_NUMBER_CHANGED,
    handleTrackingNumberChange,
  );
}

export function* watchTrackingNumberCheckRequest() {
  yield takeLatest(
    TrackingNumberCheckActionType.TRACKING_NUMBER_CHECK_REQUESTED,
    handleTrackingNumberCheckRequest,
  );
}

export default function* trackingNumberCheckSaga() {
  yield all([
    fork(watchTrackingNumberCheckRequest),
    fork(watchTrackingNumberChange),
  ]);
}