import {all, call, fork, put, select, takeLatest} from "redux-saga/effects";
import {push} from "connected-react-router";

import {
  ApplicationState,
  generateJSendError,
  parcelForYouService,
  paths,
  RedeliveryAddressActionType,
  selectDeliveryAddressId,
  selectRedeliveryAddressId,
  selectTrackingNumber,
  toastNotificationActions,
} from "./external";
import {handleJSendByTypePatched} from "./utils";

export function* handleSubmitRequested(action: any) {
  const jsendResponse = yield call(validateAddress, action);

  yield call(
    handleJSendByTypePatched,
    jsendResponse,
    // Success path
    function* () {
      // !!! BREACH OF SEPARATION OF CONCERNS between DUMB FE and SMART BE !!!
      // Workaround for handling the case when we don`t have a matching address 
      const addressId = yield select((state: ApplicationState) => selectRedeliveryAddressId(state));
      if (!addressId) {
        return yield put(toastNotificationActions.toastError({
          message: "Please enter the new delivery address for this parcel and select an option from the provided list."
        }));
      } else {
        const originalAddressId = yield select((state: ApplicationState) => selectDeliveryAddressId(state));
        if (originalAddressId === addressId) {
          return yield put(toastNotificationActions.toastError({
            message: `When redelivering to another address, the redelivery address needs to be different. 
            Please select “Redeliver to the same address” if you would like the parcel delivered to the same address.`
          }));
        }
      }
      // Workaround END

      yield put(push(paths.PARCEL_REDELIVERY_CONTACT_DETAILS));
    },
    // Fail path
    function* ({data}: { data: { redeliveryDetails: string } }) {
      yield put(toastNotificationActions.toastError({message: data.redeliveryDetails}));
    }
  );
}

export function* validateAddress(action: any) {
  try {
    const trackingNumber = yield select((state: ApplicationState) => selectTrackingNumber(state));
    const deliveryAddressId = yield select((state: ApplicationState) => selectDeliveryAddressId(state));
    const redeliveryAddressId = yield select((state: ApplicationState) => selectRedeliveryAddressId(state));
    const response = yield call(parcelForYouService.getRedeliveryDetails,
      {
        originalAddressId: deliveryAddressId,
        newAddressId: redeliveryAddressId,
        trackingNumber,
      });
    return response;
  } catch (err) {
    return generateJSendError(err);
  }
}

export function* watchSubmitRequest() {
  yield takeLatest(
    RedeliveryAddressActionType.SUBMIT_REQUESTED,
    handleSubmitRequested,
    );
}

export default function* redeliveryAddressSaga() {
  yield all([
    fork(watchSubmitRequest),
  ]);
}