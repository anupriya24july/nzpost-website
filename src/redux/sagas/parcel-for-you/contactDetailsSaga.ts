import {all, fork, put, takeLatest} from "redux-saga/effects";
import {push} from "connected-react-router";
import {ContactDetailsActionType} from "../../actions/parcel-for-you/contactDetailsActions";
import {paths} from "../../../routes/routes";

export function* handleSubmitRequested(action: any) {
  yield put(push(paths.PARCEL_REDELIVERY_SUCCESS));
}

export function* watchSubmitRequest() {
  yield takeLatest(
    ContactDetailsActionType.SUBMIT,
    handleSubmitRequested,
  )
}

export default function* redeliveryOptionsSaga() {
  yield all([
    fork(watchSubmitRequest),
  ]);
}