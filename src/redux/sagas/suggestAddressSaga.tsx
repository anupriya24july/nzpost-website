import {all, call, fork, put, takeEvery} from "redux-saga/effects";
import {ISuggestAddressFormSubmission, SuggestAddressActionType} from "../actions/suggestAddressActions";
import {suggestAddressService} from "../../services/suggestAddressService";
import {push} from "connected-react-router";
import {paths} from "../../routes/routes";

/**
 * Suggest address form saga middleware layer
 *
 * @author MaxT
 * @author NickT
 */
function* submitForm(submitAction: ISuggestAddressFormSubmission) {
  try {
    // Send submission data to API
    yield call(suggestAddressService.submitForm, submitAction.payload.formValues);

    // Navigate to submitted page
    yield put(push(paths.SUGGEST_ADDRESS_SUBMITTED));
  } catch (e) {
    console.log("Something went wrong!", e);
    yield put(push(paths.SERVER_ERROR));
  }
}

/**
 * React to suggest address form submission action and call submit form actions
 */
function* watchFormSubmit() {
  yield takeEvery(SuggestAddressActionType.SUBMIT, submitForm);
}

export default function* suggestAddressSaga() {
  yield all([fork(watchFormSubmit)])
}