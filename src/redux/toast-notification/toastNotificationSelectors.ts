import {IToastNotificationState} from "./toastNotificationReducer";

export const selectMessage
  = (state: IToastNotificationState): string => state.message;

export const selectType
  = (state: IToastNotificationState): string => state.type;

export default {
  selectMessage,
  selectType,
};
