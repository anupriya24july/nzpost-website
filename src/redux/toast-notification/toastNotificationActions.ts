import {Action, ActionCreator} from "redux";

export const enum ToastNotificationActionType {
  TOAST_ERROR = "@@toast-notification/TOAST_ERROR",
  DISMISS_All = "@@toast-notification/DISMISS_ALL",
}

export interface ToastErrorAction extends Action {
  type: ToastNotificationActionType.TOAST_ERROR,
  payload: {
    message: string,
  }
}

export interface DismissAllAction extends Action {
  type: ToastNotificationActionType.DISMISS_All,
}

export namespace toastNotificationActions {
  export const toastError: ActionCreator<ToastErrorAction>
    = ({ message }) => ({
      type: ToastNotificationActionType.TOAST_ERROR,
      payload: {message},
    });
  
  export const dismissAll: ActionCreator<DismissAllAction>
    = () => ({ type: ToastNotificationActionType.DISMISS_All });
};

export type ToastNotificationActions
  = ToastErrorAction
  | DismissAllAction
  ;

export default toastNotificationActions;
