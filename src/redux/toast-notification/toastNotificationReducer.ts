import {Reducer} from "redux";

import {
  ToastNotificationActions,
  ToastNotificationActionType,
} from "./toastNotificationActions";

export interface IToastNotificationState {
  readonly message: string;
  readonly type: string,
};

export const initialState: IToastNotificationState = {
  message: "",
  type: "error",
};

export const reducer: Reducer<IToastNotificationState>
  = (state = initialState, action: ToastNotificationActions) => {
    switch (action.type) {
      case ToastNotificationActionType.DISMISS_All:
        return {
          ...state,
          message: "",
          type: "error",
        }

      case ToastNotificationActionType.TOAST_ERROR:
        return {
          ...state,
          message: action.payload.message,
          type: "error",
        }

      default:
        return state;
    }
  }

export default reducer;
