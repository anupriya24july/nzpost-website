/**
 * @file Contains action creators for switching PKL various components states
 * @author Eugene Poletski 
 */
import {Action, ActionCreator} from "redux";

/**
 * @description Action types for managing PKL components states
 */
export const enum PKLStateMachineActionType {
  SET_STATE_DEFAULT = "@@postshop-locator/SET_STATE_DEFAULT",
  SET_STATE_EXACT_LOCATION = "@@postshop-locator/SET_STATE_EXACT_LOCATION",
  SET_STATE_EXTENT_LOCATIONS = "@@postshop-locator/SET_STATE_EXTENT_LOCATIONS",
  SET_STATE_EXTENT_LOCATIONS_NOT_FOUND = "@@postshop-locator/SET_STATE_EXTENT_LOCATIONS_NOT_FOUND",
  SET_STATE_NEARBY_LOCATIONS = "@@postshop-locator/SET_STATE_NEARBY_LOCATIONS",
  SET_STATE_SEARCH_LOCATIONS_NOT_FOUND = "@@postshop-locator/SET_STATE_SEARCH_LOCATIONS_NOT_FOUND",
}

export interface ISetStateDefaultAction extends Action {
  type: PKLStateMachineActionType.SET_STATE_DEFAULT,
}

export namespace pklStateMachineActions {
  export const setStateDefault: ActionCreator<ISetStateDefaultAction> = () => ({
    type: PKLStateMachineActionType.SET_STATE_DEFAULT,
  });
}

export type PKLStateMachineActions = ISetStateDefaultAction;