import {Action, ActionCreator} from "redux";

/**
 * @Description Action types for postshop locator ui
 */
export const enum PostshopLocatorUIActionType {
  SET_UI_STATE_DEFAULT = "@@postshop-locator/SET_UI_STATE_DEFAULT",
  SET_UI_STATE_EXACT_LOCATION = "@@postshop-locator/SET_UI_STATE_EXACT_LOCATION",
  SET_UI_STATE_EXTENT_LOCATIONS = "@@postshop-locator/SET_UI_STATE_EXTENT_LOCATIONS",
  SET_UI_STATE_EXTENT_LOCATIONS_NOT_FOUND = "@@postshop-locator/SET_UI_STATE_EXTENT_LOCATIONS_NOT_FOUND",
  SET_UI_STATE_NEARBY_LOCATIONS = "@@postshop-locator/SET_UI_STATE_NEARBY_LOCATIONS",
  SET_UI_STATE_SEARCH_LOCATIONS_NOT_FOUND = "@@postshop-locator/SET_UI_STATE_SEARCH_LOCATIONS_NOT_FOUND",
}

export interface SetUIStateDefaultAction extends Action {
  type: PostshopLocatorUIActionType.SET_UI_STATE_DEFAULT,
}

export interface SetUIStateExactLocation extends Action {
  type: PostshopLocatorUIActionType.SET_UI_STATE_EXACT_LOCATION,
}

export interface SetUIStateExtentLocations extends Action {
  type: PostshopLocatorUIActionType.SET_UI_STATE_EXTENT_LOCATIONS,
}

export interface SetUIStateExtentLocationsNotFound extends Action {
  type: PostshopLocatorUIActionType.SET_UI_STATE_EXTENT_LOCATIONS_NOT_FOUND,
}

export interface SetUIStateNearbyLocations extends Action {
  type: PostshopLocatorUIActionType.SET_UI_STATE_NEARBY_LOCATIONS,
}

export interface SetUIStateSearchLocationsNotFound extends Action {
  type: PostshopLocatorUIActionType.SET_UI_STATE_SEARCH_LOCATIONS_NOT_FOUND,
}

export namespace postshopLocatorUIActions {
  export const setUIStateDefault: ActionCreator<SetUIStateDefaultAction> = () => ({
    type: PostshopLocatorUIActionType.SET_UI_STATE_DEFAULT,
  });

  export const setUIStateExactLocation: ActionCreator<SetUIStateExactLocation> = () => ({
    type: PostshopLocatorUIActionType.SET_UI_STATE_EXACT_LOCATION,
  });

  export const setUIStateExtentLocation: ActionCreator<SetUIStateExtentLocations> = () => ({
    type: PostshopLocatorUIActionType.SET_UI_STATE_EXTENT_LOCATIONS,
  });

  export const setUIStateExtentLocationNotFound: ActionCreator<SetUIStateExtentLocationsNotFound> = () => ({
    type: PostshopLocatorUIActionType.SET_UI_STATE_EXTENT_LOCATIONS_NOT_FOUND,
  });

  export const setUIStateNearbyLocations: ActionCreator<SetUIStateNearbyLocations> = () => ({
    type: PostshopLocatorUIActionType.SET_UI_STATE_NEARBY_LOCATIONS,
  });

  export const setUIStateSearchLocationsNotFound: ActionCreator<SetUIStateSearchLocationsNotFound> = () => ({
    type: PostshopLocatorUIActionType.SET_UI_STATE_SEARCH_LOCATIONS_NOT_FOUND,
  });
}

export type PostshopLocatorUIActions = SetUIStateDefaultAction
  | SetUIStateExactLocation
  | SetUIStateExtentLocations
  | SetUIStateExtentLocationsNotFound
  | SetUIStateNearbyLocations
  | SetUIStateSearchLocationsNotFound
  ;
