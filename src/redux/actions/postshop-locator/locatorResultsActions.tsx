import {Action, ActionCreator} from "redux";
import {PostshopLocation} from "../../../models/PostshopLocation";
import {Suggestion} from "../../../models/AddressResultCollection";

/**
 * All action types and physical actions for the results section of postshop locator
 *
 * @author MaxT
 */

export const enum LocatorResultsActionType {
  SET_SEARCH_QUERY = "@@postshop-locator-results/SET_SEARCH_QUERY",
  RESULTS_NOT_FOUND = "@@postshop-locator-results/RESULTS_NOT_FOUND",
  RESULTS_LOADING = "@@postshop-locator-results/RESULTS_LOADING",
  RESULTS_LOADED = "@@postshop-locator-results/RESULTS_LOADED",
  CLEAR_RESULTS = "@@postshop-locator-results/CLEAR_RESULTS",
  SET_RESULTS = "@@postshop-locator-results/SET_RESULTS",
  SEARCH = "@@postshop-locator-results/SEARCH",
  QUICK_SEARCH = "@@postshop-locator-results/QUICK_SEARCH",
  LOAD_SELECTED = "@@postshop-locator-results/LOAD_SELECTED",
}

// Action types
export interface ResultsNotFoundAction extends Action {
  type: LocatorResultsActionType.RESULTS_NOT_FOUND;
}

export interface ResultsLoadingAction extends Action {
  type: LocatorResultsActionType.RESULTS_LOADING;
}

export interface ResultsLoadedAction extends Action {
  type: LocatorResultsActionType.RESULTS_LOADED;
}

export interface ClearResultsAction extends Action {
  type: LocatorResultsActionType.CLEAR_RESULTS;
}

export interface SetResultsAction extends Action {
  type: LocatorResultsActionType.SET_RESULTS;
  payload: {
    results: PostshopLocation[]
  }
}

export interface SetSearchQueryAction extends Action {
  type: LocatorResultsActionType.SET_SEARCH_QUERY;
  payload: {
    value: string;
  }
}

export interface SearchAction extends Action {
  type: LocatorResultsActionType.SEARCH;
  payload: {
    value: string,
    name: string
  }
}

export interface QuickSearchAction extends Action {
  type: LocatorResultsActionType.QUICK_SEARCH;
  payload: {
    value: string
  }
}

export interface LoadSelectedAction extends Action {
  type: LocatorResultsActionType.LOAD_SELECTED;
  payload: {
    suggestion: Suggestion
  }
}

export namespace locatorResultsActions {
  export const resultsNotFoundAction: ActionCreator<ResultsNotFoundAction> = () => ({
    type: LocatorResultsActionType.RESULTS_NOT_FOUND
  });

  /**
   * Action for setting results loading param. Used for displaying loading spinner mapIcons etc.
   */
  export const resultsLoadingAction: ActionCreator<ResultsLoadingAction> = () => ({
    type: LocatorResultsActionType.RESULTS_LOADING
  });

  /**
   * Action for setting results to loaded
   */
  export const resultsLoadedAction: ActionCreator<ResultsLoadedAction> = () => ({
    type: LocatorResultsActionType.RESULTS_LOADED
  });

  /**
   * Define action to clear results list
   */
  export const clearResultsRequested: ActionCreator<ClearResultsAction> = () => ({
    type: LocatorResultsActionType.CLEAR_RESULTS
  });

  /**
   * On keydown, or value selection. Kick off a search request.
   * @param value
   */
  export const searchRequested: ActionCreator<SearchAction> = (value: string, name: string) => ({
    type: LocatorResultsActionType.SEARCH,
    payload: {
      value,
      name
    }
  });

  /**
   * Quick search action used to fork two events
   * @param value
   */
  export const quickSearchRequested: ActionCreator<QuickSearchAction> = (value: string) => ({
    type: LocatorResultsActionType.QUICK_SEARCH,
    payload: {
      value
    }
  });

  /**
   * Action defined for refresh of results section items
   * @param results
   */
  export const setResultsAction: ActionCreator<SetResultsAction> = (results: PostshopLocation[]) => ({
    type: LocatorResultsActionType.SET_RESULTS,
    payload: {
      results
    }
  });

  /**
   * Action defined for loading a selected suggestion as a result
   * @param postshop
   */
  export const loadSelectedRequested: ActionCreator<LoadSelectedAction> = (suggestion: Suggestion) => ({
    type: LocatorResultsActionType.LOAD_SELECTED,
    payload: {
      suggestion
    }
  });

  /**
   * Action defined for loading a selected suggestion as a result
   * @param postshop
   */
  export const setSearchQuery: ActionCreator<SetSearchQueryAction> = (value: string) => ({
    type: LocatorResultsActionType.SET_SEARCH_QUERY,
    payload: {
      value
    }
  });

}

export type LocatorResultsActions =
  SetSearchQueryAction
  | ResultsNotFoundAction
  | ResultsLoadedAction
  | ResultsLoadingAction
  | SetResultsAction
  | ClearResultsAction
  | QuickSearchAction
  | SearchAction;
