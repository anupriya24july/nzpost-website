import {Action, ActionCreator} from "redux";
import {PostshopLocation} from "../../../models/PostshopLocation";
import {MapCoordinate} from "../../../models/LocatorMap";

/**
 * All action types and physical actions for the results section of postshop locator
 *
 * @author MaxT
 */

export const enum LocatorMapActionType {
  LOAD_POSTSHOPS_MARKERS = "@@postshop-locator-map/LOAD_POSTSHOP_MARKERS",
  MAP_EXTENT_CHANGED = "@@postshop-locator-map/MAP_EXTENT_CHANGED",
  GEOLOCATION_SEARCH = "@@postshop-locator-map/GEOLOCATION_SEARCH"
}

// Action types
export interface LoadPostshopMarkersAction extends Action {
  type: LocatorMapActionType.LOAD_POSTSHOPS_MARKERS
  payload: {
    postshops: PostshopLocation[]
  }
}

export interface MapExtentChangedAction extends Action {
  type: LocatorMapActionType.MAP_EXTENT_CHANGED
  // FIXME: Change to interface
  payload: {
    lat1: number,
    lat2: number,
    lng1: number,
    lng2: number,
    zoom: number,
  }
}

export interface GeolocationSearchAction extends Action {
  type: LocatorMapActionType.GEOLOCATION_SEARCH
  payload: {
    latitude: number,
    longitude: number
  }
}

export namespace locatorMapActions {
  export const loadLocatorMarkers: ActionCreator<LoadPostshopMarkersAction> = (postshops: PostshopLocation[]) => ({
    type: LocatorMapActionType.LOAD_POSTSHOPS_MARKERS,
    payload: {
      postshops
    }
  });

  export const mapExtentChanged: ActionCreator<MapExtentChangedAction> = (mapExtentInfo: any) => ({
    type: LocatorMapActionType.MAP_EXTENT_CHANGED,
    // FIXME: Change to interface
    payload: {
      lat1: mapExtentInfo.lat1,
      lat2: mapExtentInfo.lat2,
      lng1: mapExtentInfo.lng1,
      lng2: mapExtentInfo.lng2,
      zoom: mapExtentInfo.zoom,
    }
  });

  export const geolocationSearch: ActionCreator<GeolocationSearchAction> = (geolocationCoords: MapCoordinate) => ({
    type: LocatorMapActionType.GEOLOCATION_SEARCH,
    payload: {
      latitude: geolocationCoords.latitude,
      longitude: geolocationCoords.longitude
    }
  });
}

export type LocatorMapActions = LoadPostshopMarkersAction | GeolocationSearchAction;
