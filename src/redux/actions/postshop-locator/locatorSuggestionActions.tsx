import {Action, ActionCreator} from "redux";
import {PostshopLocation} from "../../../models/PostshopLocation";

export const enum LocatorSuggestionsActionType {
  ON_FIELD_CHANGE = "@@postshop-locator-suggestions/ON_FIELD_CHANGE",
  FETCH_REQUESTED = "@@postshop-locator-suggestions/FETCH_SUGGESTIONS",
  SET_SUGGESTIONS = "@@postshop-locator-suggestions/SET_SUGGESTIONS",
  SET_SEARCH_FIELD_VALUE = "@@postshop-locator-suggestions/SET_SEARCHFIELD_VALUE",
  CLEAR_SUGGESTIONS = "@@postshop-locator-suggestions/CLEAR_SUGGESTIONS",
  CLEAR_SEARCH = "@@postshop-locator-suggestions/CLEAR_ALL",
  SUGGESTIONS_LOADING = "@@postshop-locator-suggestions/SUGGESTIONS-LOADING",
  SUGGESTIONS_LOADED = "@@postshop-locator-suggestions/SUGGESTIONS-LOADED",
  SEARCH_CANCELLED = "@@postshop-locator-suggestions/SEARCH-CANCELLED",
  SET_SELECTED_SUGGESTIONS = "@@postshop-locator-suggestions/SET_SELECTED_SUGGESTIONS",
}

// Action types
export interface OnFieldChangeAction extends Action {
  type: LocatorSuggestionsActionType.ON_FIELD_CHANGE;
  payload: {
    value: string
  }
}

export interface FetchRequestedAction extends Action {
  type: LocatorSuggestionsActionType.FETCH_REQUESTED;
  payload: {
    value: string
  }
}

export interface SetSuggestionsAction extends Action {
  type: LocatorSuggestionsActionType.SET_SUGGESTIONS;
  payload: {
    suggestions: PostshopLocation[]
  }
}

export interface SetSearchFieldValue extends Action {
  type: LocatorSuggestionsActionType.SET_SEARCH_FIELD_VALUE;
  payload: {
    value: string
  }
}

export interface ClearSuggestionsAction extends Action {
  type: LocatorSuggestionsActionType.CLEAR_SUGGESTIONS;
}

export interface ClearSearchAction extends Action {
  type: LocatorSuggestionsActionType.CLEAR_SEARCH;
}

export interface SuggestionsLoadingAction extends Action {
  type: LocatorSuggestionsActionType.SUGGESTIONS_LOADING;
}

export interface SuggestionsLoadedAction extends Action {
  type: LocatorSuggestionsActionType.SUGGESTIONS_LOADED;
}

export interface SearchCancelledAction extends Action {
  type: LocatorSuggestionsActionType.SEARCH_CANCELLED;
}

export interface SetSelectedSuggestionAction extends Action {
  type: LocatorSuggestionsActionType.SET_SELECTED_SUGGESTIONS;
}

export namespace locatorSuggestionActions {
  /**
   * When a user types in information, set state field 'value' on the fly
   * @param value
   */
  export const fieldChangedRequested: ActionCreator<OnFieldChangeAction> = (value: string) => ({
    type: LocatorSuggestionsActionType.ON_FIELD_CHANGE,
    payload: {
      value
    }
  });

  /**
   * Kick start async process for fetching search results
   * @param value
   */
  export const fetchRequested: ActionCreator<FetchRequestedAction> = (value: string) => ({
    type: LocatorSuggestionsActionType.FETCH_REQUESTED,
    payload: {
      value
    }
  });

  /**
   * Action for clearing all state
   */
  export const clearSearchRequested: ActionCreator<ClearSearchAction> = () => ({
    type: LocatorSuggestionsActionType.CLEAR_SEARCH
  });

  /**
   * Send clear requested event to reducer
   */
  export const clearSuggestionsRequested: ActionCreator<ClearSuggestionsAction> = () => ({
    type: LocatorSuggestionsActionType.CLEAR_SUGGESTIONS,
  });

  /**
   * Action for setting results loading param
   */
  export const suggestionsLoadingAction: ActionCreator<SuggestionsLoadingAction> = () => ({
    type: LocatorSuggestionsActionType.SUGGESTIONS_LOADING,
  });

  /**
   * Action for setting results loading param to false once completed
   */
  export const suggestionsLoadedAction: ActionCreator<SuggestionsLoadedAction> = () => ({
    type: LocatorSuggestionsActionType.SUGGESTIONS_LOADED,
  });

  /**
   * Action for setting results in results list
   * @param suggestions
   */
  export const setSuggestionsAction: ActionCreator<SetSuggestionsAction> = (suggestions: PostshopLocation[]) => ({
    type: LocatorSuggestionsActionType.SET_SUGGESTIONS,
    payload: {
      suggestions
    }
  });

  /**
   * Action for setting the searchfield value. Useful for triggering searches from other components.
   * @param value to set
   */
  export const setSearchFieldValueAction: ActionCreator<SetSearchFieldValue> = (value: string) => ({
    type: LocatorSuggestionsActionType.SET_SEARCH_FIELD_VALUE,
    payload: {
      value
    }
  });

  /**
   * Action for cancelling a search in progress
   */
  export const searchCancelledAction: ActionCreator<SearchCancelledAction> = () => ({
    type: LocatorSuggestionsActionType.SEARCH_CANCELLED,
  });

  /**
   * Action for setting results in results list
   * @param suggestions
   */
  export const setSelectedSuggestionAction: ActionCreator<SetSelectedSuggestionAction> = (suggestion: PostshopLocation[]) => ({
    type: LocatorSuggestionsActionType.SET_SELECTED_SUGGESTIONS,
    payload: {
      suggestion
    }
  });
}


export type LocatorSuggestionActions =
  OnFieldChangeAction
  | SearchCancelledAction
  | FetchRequestedAction
  | SetSuggestionsAction
  | SetSearchFieldValue
  | ClearSuggestionsAction
  | ClearSearchAction
  | SuggestionsLoadingAction
  | SuggestionsLoadedAction
  | SetSelectedSuggestionAction;