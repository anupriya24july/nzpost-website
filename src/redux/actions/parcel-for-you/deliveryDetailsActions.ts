import {Action, ActionCreator} from "redux";

export const enum DeliveryDetailsActionType {
  ADDRESS_SUGGESTIONS_CLEAR_REQUESTED = "@@parcel-for-you/delivery-details/ADDRESS_SUGGESTIONS_CLEAR_REQUESTED",
  ADDRESS_SUGGESTIONS_FETCH_REQUESTED = "@@parcel-for-you/delivery-details/ADDRESS_SUGGESTIONS_FETCH_REQUESTED",
  ADDRESS_SUGGESTIONS_FETCH_SUCCEEDED = "@@parcel-for-you/delivery-details/ADDRESS_SUGGESTIONS_FETCH_SUCCEEDED",
  ADDRESS_CHANGED = "@@parcel-for-you/delivery-details/ADDRESS_CHANGED",
  ADDRESS_SELECTED = "@@parcel-for-you/delivery-details/ADDRESS_SELECTED",
  SUBMIT_REQUESTED = "@@parcel-for-you/delivery-details/SUBMIT_REQUESTED",
  RESET_REQUESTED = "@@parcel-for-you/delivery-details/RESET_REQUESTED",
}

export interface AddressSuggestionsClearRequested extends Action {
  type: DeliveryDetailsActionType.ADDRESS_SUGGESTIONS_CLEAR_REQUESTED;
  payload: {},
}

export interface AddressSuggestionsFetchRequested extends Action {
  type: DeliveryDetailsActionType.ADDRESS_SUGGESTIONS_FETCH_REQUESTED;
  payload: {
    value: string
  }
}

export interface AddressSuggestionsFetchSucceeded extends Action {
  type: DeliveryDetailsActionType.ADDRESS_SUGGESTIONS_FETCH_SUCCEEDED;
  payload: {
    suggestions: Array<any>,
  }
}

export interface AddressChanged extends Action {
  type: DeliveryDetailsActionType.ADDRESS_CHANGED
  payload: {
    address: string,
  }
}

export interface AddressSelected extends Action {
  type: DeliveryDetailsActionType.ADDRESS_SELECTED;
  payload: {
    id: string,
  }
}

export interface SubmitRequested extends Action {
  type: DeliveryDetailsActionType.SUBMIT_REQUESTED;
  payload: {},
}

export interface ResetActionRequested extends Action {
  type: DeliveryDetailsActionType.RESET_REQUESTED;
}

export namespace deliveryDetailsActions {
  export const addressSuggestionsClearRequested: ActionCreator<AddressSuggestionsClearRequested>
    = () => ({
    type: DeliveryDetailsActionType.ADDRESS_SUGGESTIONS_CLEAR_REQUESTED,
    payload: {},
  });

  /**
   * When a user types in information, set state field 'value' on the fly
   * @param value
   */
  export const addressSuggestionsFetchRequested: ActionCreator<AddressSuggestionsFetchRequested> = (value: string) => ({
    type: DeliveryDetailsActionType.ADDRESS_SUGGESTIONS_FETCH_REQUESTED,
    payload: {
      value
    }
  });

  export const addressSuggestionsFetchSucceeded: ActionCreator<AddressSuggestionsFetchSucceeded>
    = (suggestions: Array<any>) => ({
    type: DeliveryDetailsActionType.ADDRESS_SUGGESTIONS_FETCH_SUCCEEDED,
    payload: {suggestions},
  });

  export const addressChanged: ActionCreator<AddressChanged>
    = ({address}: { address: string }) => ({
    type: DeliveryDetailsActionType.ADDRESS_CHANGED,
    payload: {address},
  });

  export const addressSelected: ActionCreator<AddressSelected>
    = ({id}: { id: string }) => ({
    type: DeliveryDetailsActionType.ADDRESS_SELECTED,
    payload: {id},
  });

  export const submitRequested: ActionCreator<SubmitRequested>
    = () => ({
    type: DeliveryDetailsActionType.SUBMIT_REQUESTED,
    payload: {},
  })

  export const resetDeliveryDetails: ActionCreator<ResetActionRequested> = () => ({
    type: DeliveryDetailsActionType.RESET_REQUESTED,
  });
}

export type DeliveryDetailsActions
  = AddressSuggestionsClearRequested
  | AddressSuggestionsFetchRequested
  | AddressSuggestionsFetchSucceeded
  | AddressChanged
  | AddressSelected
  | SubmitRequested
  | ResetActionRequested
  ;
