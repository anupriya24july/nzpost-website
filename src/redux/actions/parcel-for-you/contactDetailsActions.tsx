import {Action} from "redux";

export interface IContactDetailsFormValues {
  firstName: string,
  lastName: string,
  company: string,
  email: string,
  phone: string
}

/**
 * Suggest address actions
 */
export const enum ContactDetailsActionType {
  SUBMIT = "@@parcel-for-you/contact-details/SUBMIT_REQUESTED",
  RESET_REQUESTED = "@@parcel-for-you/contact-details/RESET_REQUESTED"
}

export interface IContactDetailsFormSubmission extends Action {
  type: ContactDetailsActionType.SUBMIT;
  payload: {
    formValues: IContactDetailsFormValues
  }
}

export interface IContactDetailsResetRequested extends Action {
  type: ContactDetailsActionType.RESET_REQUESTED;
}

export namespace contactDetailsActions {

  export function resetContactDetails(): IContactDetailsResetRequested {
    return {
      type: ContactDetailsActionType.RESET_REQUESTED
    };
  }

  export function submit(formValues: IContactDetailsFormValues): IContactDetailsFormSubmission {
    return {
      type: ContactDetailsActionType.SUBMIT,
      payload: {
        formValues
      }
    }
  }

}

export type ContactDetailsActions = IContactDetailsFormSubmission;
