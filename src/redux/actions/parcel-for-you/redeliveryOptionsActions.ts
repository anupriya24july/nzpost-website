import {Action, ActionCreator} from "redux";

export const enum RedeliveryOptionsActionType {
  SET_OPTIONS = "@@parcel-for-you/redelivery-options/SET_OPTIONS",
  CHANGE_OPTION = "@@parcel-for-you/redelivery-options/CHANGE_OPTION",
  SUBMIT_OPTION = "@@parcel-for-you/redelivery-options/SUBMIT_OPTION",
  SET_REDELIVERY_DETAILS = "@@parcel-for-you/redelivery-options/SET_REDELIVERY_DETAILS",
  RESET_REDELIVERY_DETAILS = "@@parcel-for-you/redelivery-options/RESET_REDELIVERY_DETAILS",
}

export interface SetRedeliveryOptionsRequested extends Action {
  type: RedeliveryOptionsActionType.SET_OPTIONS;
  payload: { redeliveryOptions: any };
}

export interface ChangeRedeliveryOptionRequested extends Action {
  type: RedeliveryOptionsActionType.CHANGE_OPTION;
  payload: { option: string };
}

export interface SubmitRedeliveryOptionRequested extends Action {
  type: RedeliveryOptionsActionType.SUBMIT_OPTION;
}

export interface SetRedeliveryDetailsRequested extends Action {
  type: RedeliveryOptionsActionType.SET_REDELIVERY_DETAILS;
  payload: { redeliveryDetails: any };
}

export interface RedeliveryDetailsResetAction extends Action {
  type: RedeliveryOptionsActionType.RESET_REDELIVERY_DETAILS;
}


export namespace redeliveryOptionsActions {
  export const setRedeliveryOptionsRequested: ActionCreator<SetRedeliveryOptionsRequested>
    = (redeliveryOptions: any) => ({
    type: RedeliveryOptionsActionType.SET_OPTIONS,
    payload: {redeliveryOptions}
  });

  export const changeRedeliveryOption: ActionCreator<ChangeRedeliveryOptionRequested>
    = (option: string) => ({
    type: RedeliveryOptionsActionType.CHANGE_OPTION,
    payload: {option}
  });

  export const submitRedeliveryOptionRequested: ActionCreator<SubmitRedeliveryOptionRequested>
    = () => ({
    type: RedeliveryOptionsActionType.SUBMIT_OPTION,
  });

  export const setRedeliveryDetails: ActionCreator<SetRedeliveryDetailsRequested>
    = (redeliveryDetails: string) => ({
    type: RedeliveryOptionsActionType.SET_REDELIVERY_DETAILS,
    payload: {redeliveryDetails}
  });

  export const resetRedeliveryDetails: ActionCreator<RedeliveryDetailsResetAction> = () => ({
    type: RedeliveryOptionsActionType.RESET_REDELIVERY_DETAILS,
  });
}


export type RedeliveryOptionsActions =
  SetRedeliveryOptionsRequested
  | ChangeRedeliveryOptionRequested
  | SubmitRedeliveryOptionRequested
  | SetRedeliveryDetailsRequested
  | RedeliveryDetailsResetAction;
