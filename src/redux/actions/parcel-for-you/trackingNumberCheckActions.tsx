import {Action, ActionCreator} from "redux";


export const enum TrackingNumberCheckActionType {
  TRACKING_NUMBER_CHANGED = "@@tracking-number-check/TRACKING_NUMBER_CHANGED",
  TRACKING_NUMBER_CHECK_REQUESTED = "@@tracking-number-check/TRACKING_NUMBER_CHECK_REQUESTED",
  TRACKING_NUMBER_CHECK_FAILED = "@@tracking-number-check/TRACKING_NUMBER_CHECK_FAILED",
  TRACKING_NUMBER_CHECK_SUCCEEDED = "@@tracking-number-check/TRACKING_NUMBER_CHECK_SUCCEEDED",
  TRACKING_NUMBER_CHECK_RESET = "@@tracking-number-check/TRACKING_NUMBER_CHECK_RESET",
}

export interface TrackingNumberChangedAction extends Action {
  type: TrackingNumberCheckActionType.TRACKING_NUMBER_CHANGED,
  payload: {
    trackingNumber: string,
  }
}

export interface TrackingNumberCheckRequestedAction extends Action {
  type: TrackingNumberCheckActionType.TRACKING_NUMBER_CHECK_REQUESTED;
}

export interface TrackingNumberCheckFailedAction extends Action {
  type: TrackingNumberCheckActionType.TRACKING_NUMBER_CHECK_FAILED,
  payload: {
    message: any,
    code: string,
  }
}

export interface TrackingNumberCheckSucceededAction extends Action {
  type: TrackingNumberCheckActionType.TRACKING_NUMBER_CHECK_SUCCEEDED,
  payload: {
    number: string,
    redeliveryOptions: any
  }
}

export interface TrackingNumberCheckResetAction extends Action {
  type: TrackingNumberCheckActionType.TRACKING_NUMBER_CHECK_RESET;
}

export namespace trackingNumberCheckActions {

  export const changeTrackingNumber: ActionCreator<TrackingNumberChangedAction>
    = ({trackingNumber}) => ({
    type: TrackingNumberCheckActionType.TRACKING_NUMBER_CHANGED,
    payload: {trackingNumber},
  });

  export const requestTrackingNumberCheck: ActionCreator<TrackingNumberCheckRequestedAction> = () => ({
    type: TrackingNumberCheckActionType.TRACKING_NUMBER_CHECK_REQUESTED,
  });

  export const failTrackingNumberCheck: ActionCreator<TrackingNumberCheckFailedAction> = (message: string, code: string) => ({
    type: TrackingNumberCheckActionType.TRACKING_NUMBER_CHECK_FAILED,
    payload: {message, code}
  });

  export const succeedTrackingNumberCheck: ActionCreator<TrackingNumberCheckSucceededAction>
    = ({number, redeliveryOptions}) => ({
    type: TrackingNumberCheckActionType.TRACKING_NUMBER_CHECK_SUCCEEDED,
    payload: {number, redeliveryOptions}
  });


  export const resetTrackingNumberCheck: ActionCreator<TrackingNumberCheckResetAction> = () => ({
    type: TrackingNumberCheckActionType.TRACKING_NUMBER_CHECK_RESET,
  });
}

export type TrackingNumberCheckActions
  = TrackingNumberChangedAction
  | TrackingNumberCheckRequestedAction
  | TrackingNumberCheckFailedAction
  | TrackingNumberCheckSucceededAction
  | TrackingNumberCheckResetAction;