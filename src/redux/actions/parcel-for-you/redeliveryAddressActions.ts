import {Action, ActionCreator} from "redux";

export const enum RedeliveryAddressActionType {
  ADDRESS_CHANGED = "@@parcel-for-you/redelivery-address/ADDRESS_CHANGED",
  ADDRESS_SELECTED = "@@parcel-for-you/redelivery-address/ADDRESS_SELECTED",
  SUBMIT_REQUESTED = "@@parcel-for-you/redelivery-address/SUBMIT_REQUESTED",
};

export interface RedeliveryAddressSubmitRequested extends Action {
  type: RedeliveryAddressActionType.SUBMIT_REQUESTED;
  payload: {},
};

export interface RedeliveryAddressSelected extends Action {
  type: RedeliveryAddressActionType.ADDRESS_SELECTED;
  payload: {
    id: string,
  }
};

export interface RedeliveryAddressChanged extends Action {
  type: RedeliveryAddressActionType.ADDRESS_CHANGED
  payload: {
    address: string,
  }
}

export namespace redeliveryAddressActions {
  export const addressChanged: ActionCreator<RedeliveryAddressChanged>
  = ({address}: {address: string}) => ({
    type: RedeliveryAddressActionType.ADDRESS_CHANGED,
    payload: {address},
  });

  export const addressSelected: ActionCreator<RedeliveryAddressSelected>
  = ({id}: {id: string}) => ({
    type: RedeliveryAddressActionType.ADDRESS_SELECTED,
    payload: {id},
  });

  export const submitRequested: ActionCreator<RedeliveryAddressSubmitRequested>
  = () => ({
    type: RedeliveryAddressActionType.SUBMIT_REQUESTED,
    payload: {},
  })
};

export type RedeliveryAddressActions
  = RedeliveryAddressChanged
  | RedeliveryAddressSelected
  | RedeliveryAddressSubmitRequested
  ;