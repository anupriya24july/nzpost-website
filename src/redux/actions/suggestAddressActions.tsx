import {Action} from "redux";

export interface ISuggestAddressFormValues {
  fullName: string,
  email: string,
  streetAddress: string,
  suburb: string,
  cityOrTown: string,
  addressConfirmed: string,
  comments?: string
}

/**
 * Suggest address actions
 */
export const enum SuggestAddressActionType {
  SUBMIT = "@@suggest-address/SUBMIT"
}

export interface ISuggestAddressFormSubmission extends Action {
  type: SuggestAddressActionType.SUBMIT;
  payload: {
    formValues: ISuggestAddressFormValues
  }
}

export namespace suggestAddressActions {

  export function submit(formValues: ISuggestAddressFormValues): ISuggestAddressFormSubmission {
    return {
      type: SuggestAddressActionType.SUBMIT,
      payload: {
        formValues
      }
    }
  }

}
