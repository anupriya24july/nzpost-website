import { Location } from "./externals";

export const selectLocationsDomain = (state: any) => state.postshopLocator.locationsState;

export const selectLocationList = (state: any): Array<Location> => {
  return selectLocationsDomain(state).locationList;
}
