import { Reducer } from "redux";
import { Location } from "./externals";
import { LocationsActions, LocationsActionType } from "./actions";

export interface ILocationsState {
  locationList: Location[],
};

export const initialState = {
  locationList: [],
};

const locationsReducer: Reducer<ILocationsState> = (state = initialState, action) => {
  switch ((action as LocationsActions).type) {
    case LocationsActionType.REPLACE_LOCATIONS:
      return {...state, locationList: action.payload.locations }
    default:
      return state;
  }
};

export default locationsReducer;
