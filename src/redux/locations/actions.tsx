import { Action, ActionCreator } from "redux";
import { Location } from "./externals";

export const enum LocationsActionType {
  REPLACE_LOCATIONS = "@@locations/REPLACE_LOCATIONS",
};

export interface ReplaceLocationsAction extends Action {
  type: LocationsActionType.REPLACE_LOCATIONS,
  payload: {
    locations: Location[],
  },
};

export namespace locationsActions {
  export const replaceLocations: ActionCreator<ReplaceLocationsAction>
    = ({ locations }) => ({
      type: LocationsActionType.REPLACE_LOCATIONS,
      payload: {
        locations,
      }
    }) 
};

export type LocationsActions = ReplaceLocationsAction;