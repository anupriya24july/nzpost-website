import postshopLocatorReducer, {PostshopLocatorState} from "./reducers/postshop-locator/postshopLocatorReducer";

import toastNotificationReducer, {IToastNotificationState} from "./toast-notification/toastNotificationReducer";

import trackingNumberCheckReducer,
  {ITrackingNumberCheckState}
from "./reducers/parcel-for-you/trackingNumberCheckReducer";

export {default as parcelForYouReducer, IParcelForYouState} from "src/redux/reducers/parcel-for-you/parcelForYouReducer";

export {
  IToastNotificationState,
  postshopLocatorReducer,
  PostshopLocatorState,
  toastNotificationReducer,
  trackingNumberCheckReducer,
  ITrackingNumberCheckState,
}

export {paths} from "../routes/routes";