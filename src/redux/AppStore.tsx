import {applyMiddleware, combineReducers, createStore, Reducer} from 'redux';
import {FormStateMap, reducer as form} from 'redux-form';
import createSagaMiddleware from "redux-saga";
import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly';
import {createBrowserHistory} from 'history';
import {connectRouter, routerMiddleware} from 'connected-react-router';
import rootSaga from './sagas/rootSaga';
import {
  IParcelForYouState,
  IToastNotificationState,
  parcelForYouReducer,
  postshopLocatorReducer,
  PostshopLocatorState,
  toastNotificationReducer,
} from "./external";

/**
 * Setup and apply all middleware
 */
export const history = createBrowserHistory();
const appRouterMiddleware = routerMiddleware(history);

const sagaMiddleware = createSagaMiddleware();

const getMiddleware = () => {
  return applyMiddleware(appRouterMiddleware, sagaMiddleware);
};

// The top-level state object
export interface ApplicationState {
  form: FormStateMap;
  parcelForYou: IParcelForYouState,
  postshopLocator: PostshopLocatorState;
  toastNotification: IToastNotificationState,
}

/**
 * Root reducer combines all used reducers
 */
const rootReducer: Reducer<ApplicationState> = combineReducers<ApplicationState>({
  form,
  parcelForYou: parcelForYouReducer,
  postshopLocator: postshopLocatorReducer,
  toastNotification: toastNotificationReducer,
});

/**
 * Default AppStore
 */
export const AppStore = createStore(
  connectRouter(history)(rootReducer), composeWithDevTools(getMiddleware())
);

sagaMiddleware.run(rootSaga);
