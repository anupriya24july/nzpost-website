export const getLocationsDomain
  = (state: any) => state.locationsState;

export const selectPostshopLocatorDomain
  = (state: any) => state.postshopLocator;

export const selectParcelForYouDomain
  = (state: any) => state.parcelForYou;
