export function getFullName(state: any) {
  return state.form.suggestAddress.values.fullName;
}

export function getEmail(state: any) {
  return state.form.suggestAddress.values.email;
}

export function getStreetAddress(state: any) {
  return state.form.suggestAddress.values.streetAddress;
}

export function getSuburb(state: any) {
  return state.form.suggestAddress.values.suburb;
}

export function getCityOrTown(state: any) {
  return state.form.suggestAddress.values.cityOrTown;
}

export function getAddressConfirmed(state: any) {
  return state.form.suggestAddress.values.addressConfirmed;
}

export function getComments(state: any) {
  return state.form.suggestAddress.values.comments;
}