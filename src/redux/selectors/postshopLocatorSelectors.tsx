import { selectPostshopLocatorDomain } from "./domainSelectors";

export function selectShouldAdjustMapExtentToLocations(state: any): boolean {
  return selectPostshopLocatorDomain(state).uiState.shouldAdjustMapExtentToLocations;
}

export function getSearchQuery(state: any): any {
  return selectPostshopLocatorDomain(state).resultsState.searchQuery;
}

export function selectResultsMessageTitle(state: any): boolean {
  return selectPostshopLocatorDomain(state).resultsState.messageTitle;
}

export function selectResultsMessageText(state: any): boolean {
  return selectPostshopLocatorDomain(state).resultsState.messageText;
}