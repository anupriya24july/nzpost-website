import {selectParcelForYouDomain} from "./domainSelectors";

export function selectAddressSearchSuggestions(state: any): any {
  return selectParcelForYouDomain(state).deliveryDetails.addressSearchSuggestions;
}

export function getTrackingNumber(state: any): string {
  return state.parcelForYou.trackingNumberCheck.trackingNumber;
}

export function selectTrackingNumber(state: any): string {
  return selectParcelForYouDomain(state).trackingNumberCheck.trackingNumber;
}

export function selectDeliveryAddressId(state: any): string {
  return selectParcelForYouDomain(state).deliveryDetails.addressId;
}

export function selectDeliveryAddress(state: any): string {
  return selectParcelForYouDomain(state).deliveryDetails.address;
}

export function selectDeliveryAddressSearchSuggestions(state: any): Array<any> {
  return selectParcelForYouDomain(state).deliveryDetails.addressSearchSuggestions;
}

export function getSelectedRedeliveryOption(state: any): string {
  return state.parcelForYou.redeliveryOptions.selectedOption;
}

export function selectRedeliveryAddressId(state: any): string {
  return selectParcelForYouDomain(state).redeliveryAddress.addressId;
}

export function selectRedeliveryAddress(state: any): string {
  return selectParcelForYouDomain(state).redeliveryAddress.address;
}

export function selectContactDetails(state: any): any {
  return selectParcelForYouDomain(state).contactDetails;
}