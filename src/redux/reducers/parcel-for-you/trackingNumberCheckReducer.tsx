import {Reducer} from "redux";
import {TrackingNumberCheckActions, TrackingNumberCheckActionType} from "./external";

export interface ITrackingNumberFormValues {
  readonly trackingNumber: string
};

export interface IValidation {
  readonly message: string,
  readonly code: string,
};

export const initialValidationState: IValidation = {
  message: "", code: ""
};

export interface ITrackingNumberCheckState {
  readonly trackingNumber: string,
  readonly isCheckingTrackingNumber: boolean,
  readonly validation: IValidation
};

export const initialState: ITrackingNumberCheckState = {
  trackingNumber: "",
  isCheckingTrackingNumber: false,
  validation: {message: "", code: ""}
};

export const trackingNumberCheckReducer: Reducer<ITrackingNumberCheckState>
  = (state = initialState, action: TrackingNumberCheckActions) => {
  switch (action.type) {
    case TrackingNumberCheckActionType.TRACKING_NUMBER_CHANGED:
      return {
        ...state,
        trackingNumber: action.payload.trackingNumber,
        validation: {message: "", code: ""}
      };

    case TrackingNumberCheckActionType.TRACKING_NUMBER_CHECK_REQUESTED:
      return {
        ...state,
        isCheckingTrackingNumber: true,
      };

    case TrackingNumberCheckActionType.TRACKING_NUMBER_CHECK_FAILED:
      return {
        ...state,
        validation: {message: action.payload.message, code: action.payload.code},
        isCheckingTrackingNumber: false,
      };

    case TrackingNumberCheckActionType.TRACKING_NUMBER_CHECK_SUCCEEDED:
      return {
        ...state,
        isCheckingTrackingNumber: false,

      };

    case TrackingNumberCheckActionType.TRACKING_NUMBER_CHECK_RESET:
      return initialState;

    default:
      return state;
  }
};

export default trackingNumberCheckReducer;
