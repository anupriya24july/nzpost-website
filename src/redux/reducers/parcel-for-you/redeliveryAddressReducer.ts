import {Reducer} from "redux";
import {
  RedeliveryAddressActions,
  RedeliveryAddressActionType,
} from "./external";

export interface IRedeliveryAddressState {
  readonly addressId: string;
  readonly address: string;
}

export const initialState: IRedeliveryAddressState = {
  addressId: "",
  address: "",
};

export const deliveryDetailsReducer: Reducer<IRedeliveryAddressState>
  = (state: IRedeliveryAddressState = initialState, action) => {
    switch ((action as RedeliveryAddressActions).type) {
      case RedeliveryAddressActionType.ADDRESS_CHANGED:
        return {
          ...state,
          address: action.payload.address,
          addressId: "",
        };

      case RedeliveryAddressActionType.ADDRESS_SELECTED:
        return {...state, addressId: action.payload.id};

      default:
        return state;
    }
};

export default deliveryDetailsReducer;