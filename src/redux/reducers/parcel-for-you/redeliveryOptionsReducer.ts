import {Reducer} from "redux";
import {RedeliveryOptionsActions, RedeliveryOptionsActionType} from "src/redux/actions/parcel-for-you/redeliveryOptionsActions";

export interface IRedeliveryOptionsState {
  readonly sameAddress: boolean;
  readonly differentAddress: boolean;
  readonly parcelCollect: boolean;
  readonly selectedOption: string;
  readonly deliveryDetails: any;

}

export const initialState: IRedeliveryOptionsState = {
  sameAddress: true,
  differentAddress: true,
  parcelCollect: false,
  selectedOption: "sameAddress",
  deliveryDetails: []
};

export const redeliveryOptionsReducer: Reducer<IRedeliveryOptionsState>
  = (state: IRedeliveryOptionsState = initialState, action) => {
  switch ((action as RedeliveryOptionsActions).type) {
    case RedeliveryOptionsActionType.SET_OPTIONS:
      return {
        ...state,
        sameAddress: action.payload.redeliveryOptions.redeliveryOptions.sameAddress,
        differentAddress: action.payload.redeliveryOptions.redeliveryOptions.differentAddress,
        parcelCollect: action.payload.redeliveryOptions.redeliveryOptions.parcelCollect,
      };
    case RedeliveryOptionsActionType.CHANGE_OPTION:
      return {
        ...state,
        selectedOption: action.payload.option
      };
    case RedeliveryOptionsActionType.SET_REDELIVERY_DETAILS:
      return {
        ...state,
        deliveryDetails: action.payload.redeliveryDetails
      };

    case RedeliveryOptionsActionType.RESET_REDELIVERY_DETAILS:
      return initialState;

    default:
      return state;
  }
};

export default redeliveryOptionsReducer;
