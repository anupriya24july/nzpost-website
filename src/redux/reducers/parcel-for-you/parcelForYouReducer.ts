import {combineReducers, Reducer} from "redux";
import deliveryDetailsReducer, {IDeliveryDetailsState} from "./deliveryDetailsReducer";
import trackingNumberCheckReducer, {ITrackingNumberCheckState} from "./trackingNumberCheckReducer";
import redeliveryOptionsReducer, {IRedeliveryOptionsState} from "./redeliveryOptionsReducer";
import redeliveryAddressReducer, {IRedeliveryAddressState} from "./redeliveryAddressReducer";
import contactDetailsReducer, {IContactDetailsState} from "./contactDetailsReducer";

export interface IParcelForYouState {
  trackingNumberCheck: ITrackingNumberCheckState;
  deliveryDetails: IDeliveryDetailsState;
  redeliveryOptions: IRedeliveryOptionsState;
  redeliveryAddress: IRedeliveryAddressState;
  contactDetails: IContactDetailsState;
}

export const parcelForYouReducer: Reducer<IParcelForYouState> = combineReducers<IParcelForYouState>(
  {
    trackingNumberCheck: trackingNumberCheckReducer,
    deliveryDetails: deliveryDetailsReducer,
    redeliveryOptions: redeliveryOptionsReducer,
    redeliveryAddress: redeliveryAddressReducer,
    contactDetails: contactDetailsReducer
  }
);

export default parcelForYouReducer;