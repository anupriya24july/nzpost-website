import {Reducer} from "redux";
import {DeliveryDetailsActions} from "./external";
import {DeliveryDetailsActionType} from "src/redux/actions/parcel-for-you/deliveryDetailsActions";

export interface IDeliveryDetailsState {
  readonly addressId: string;
  readonly address: string;
  readonly addressSearchSuggestions: Array<any>;
}

export const initialState: IDeliveryDetailsState = {
  addressId: "",
  address: "",
  addressSearchSuggestions: [],
};

export const deliveryDetailsReducer: Reducer<IDeliveryDetailsState>
  = (state: IDeliveryDetailsState = initialState, action) => {
  switch ((action as DeliveryDetailsActions).type) {
    case DeliveryDetailsActionType.ADDRESS_SUGGESTIONS_CLEAR_REQUESTED:
      return {...state, addressSearchSuggestions: []};

    case DeliveryDetailsActionType.ADDRESS_SUGGESTIONS_FETCH_SUCCEEDED:
      return {...state, addressSearchSuggestions: action.payload.suggestions};

    case DeliveryDetailsActionType.ADDRESS_CHANGED:
      return {
        ...state,
        address: action.payload.address,
        addressId: "",
      };

    case DeliveryDetailsActionType.ADDRESS_SELECTED:
      return {...state, addressId: action.payload.id};

    case DeliveryDetailsActionType.RESET_REQUESTED:
      return initialState;

    default:
      return state;
  }
};

export default deliveryDetailsReducer;
