import {Reducer} from "redux";
import {ContactDetailsActionType} from "src/redux/actions/parcel-for-you/contactDetailsActions";

export interface IContactDetailsState {
    readonly firstName: string,
    readonly lastName: string,
    readonly company: string,
    readonly email: string,
    readonly phone: string
}

export const initialState: IContactDetailsState = {
    firstName: '',
    lastName: '',
    company: '',
    email: '',
    phone: ''
};

export const contactDetailsReducer: Reducer<IContactDetailsState>
    = (state: IContactDetailsState = initialState, action) => {
    switch (action.type) {

        case ContactDetailsActionType.SUBMIT:
            return {...state, ...action.payload.formValues};

        case ContactDetailsActionType.RESET_REQUESTED:
            return initialState;
        default:
            return state;
    }
};

export default contactDetailsReducer;
