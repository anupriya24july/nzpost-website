import {PostshopLocation} from "../../../models/PostshopLocation";
import {Reducer} from "redux";
import {LocatorMapActions, LocatorMapActionType} from "../../actions/postshop-locator/locatorMapActions";

export interface ILocatorMapState {
  readonly latitude: number,
  readonly longitude: number,
  readonly geo_latitude?: number,
  readonly geo_longitude?: number,
  readonly zoom: number,
  readonly postshops: PostshopLocation[],
}

// Initial state is a zoomed out version of New Zealand
export const initialState: ILocatorMapState = {
  latitude: -41.038875,
  longitude: 171.7799,
  zoom: 5,
  postshops: []
};

/**
 * Reduction control for the postshop locator map
 * @param state
 * @param action
 */
const locatorMapReducer: Reducer<ILocatorMapState> = (state: ILocatorMapState = initialState, action) => {
  switch ((action as LocatorMapActions).type) {
    case LocatorMapActionType.LOAD_POSTSHOPS_MARKERS:
      return {...state, postshops: action.payload.postshops};
    case LocatorMapActionType.GEOLOCATION_SEARCH:
      return {...state, geo_latitude: action.payload.latitude, geo_longitude: action.payload.longitude};
    default:
      return state;
  }
};

export default locatorMapReducer;