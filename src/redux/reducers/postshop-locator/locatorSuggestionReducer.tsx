import {Reducer} from "redux";
import {LocatorSuggestionActions, LocatorSuggestionsActionType} from "../../actions/postshop-locator/locatorSuggestionActions";
import {Suggestion} from "../../../models/AddressResultCollection";

export interface ILocatorSuggestionsState {
  readonly value: string;
  readonly suggestions: Suggestion[];
  readonly selectedSuggestion: Suggestion[];
  readonly loadingSuggestions: boolean;
}

export const initialState: ILocatorSuggestionsState = {
  value: '',
  suggestions: [],
  selectedSuggestion: [],
  loadingSuggestions: false
};

/**
 * Postshop Locator suggestionState reduction logic
 * @param state
 * @param action
 */
const locatorSuggestionReducer: Reducer<ILocatorSuggestionsState> = (state: ILocatorSuggestionsState = initialState, action) => {
  switch ((action as LocatorSuggestionActions).type) {
    case LocatorSuggestionsActionType.ON_FIELD_CHANGE:
      return {...state, value: action.payload.value};
    case LocatorSuggestionsActionType.SET_SUGGESTIONS:
      return {...state, suggestions: action.payload.suggestions};
    case LocatorSuggestionsActionType.SET_SEARCH_FIELD_VALUE:
      return {...state, value: action.payload.value};
    case LocatorSuggestionsActionType.SUGGESTIONS_LOADING:
      return {...state, loadingSuggestions: true};
    case LocatorSuggestionsActionType.SUGGESTIONS_LOADED:
      return {...state, loadingSuggestions: false};
    case LocatorSuggestionsActionType.CLEAR_SUGGESTIONS:
      return {...state, suggestions: [], loadingSuggestions: false};
    case LocatorSuggestionsActionType.SEARCH_CANCELLED:
      return {...state, loadingSuggestions: false};
    case LocatorSuggestionsActionType.CLEAR_SEARCH:
      return initialState;
    case LocatorSuggestionsActionType.SET_SELECTED_SUGGESTIONS:
      return {...state, selectedSuggestion: action.payload.suggestion};
    default:
      return state;
  }
};

export default locatorSuggestionReducer;