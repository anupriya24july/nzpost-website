import {Reducer} from "redux";
import {LocatorResultsActions, LocatorResultsActionType} from "../../actions/postshop-locator/locatorResultsActions";
import {PostshopLocatorUIActions, PostshopLocatorUIActionType} from "./external";

export const STATE_TYPE_DEFAULT = "@@postshop-locator-results-list/STATE_TYPE_DEFAULT";
export const STATE_TYPE_EXTENT_LOCATIONS_NOT_FOUND = "@@postshop-locator-results-list/STATE_TYPE_EXTENT_LOCATIONS_NOT_FOUND";
export const STATE_TYPE_SEARCH_LOCATIONS_NOT_FOUND = "@@postshop-locator-results-list/STATE_TYPE_SEARCH_LOCATIONS_NOT_FOUND";

export interface ILocatorResultsState {
  readonly loadingResults: boolean;
  readonly resultsNotFound: boolean;
  readonly searchQuery: string;
  readonly stateType: string;
  readonly messageTitle: string,
  readonly messageText: string,
}

export const initialState: ILocatorResultsState = {
  loadingResults: false,
  resultsNotFound: false,
  searchQuery: "",
  stateType: STATE_TYPE_DEFAULT,
  messageTitle: "",
  messageText: "",
};

/**
 * Postshop Locator results reduction logic
 * @param state
 * @param action
 */
const locatorResultsListReducer: Reducer<ILocatorResultsState> = (state: ILocatorResultsState = initialState, action) => {
  switch ((action as LocatorResultsActions | PostshopLocatorUIActions).type) {
    case PostshopLocatorUIActionType.SET_UI_STATE_DEFAULT:
    case PostshopLocatorUIActionType.SET_UI_STATE_EXACT_LOCATION:
    case PostshopLocatorUIActionType.SET_UI_STATE_EXTENT_LOCATIONS:
    case PostshopLocatorUIActionType.SET_UI_STATE_NEARBY_LOCATIONS:
      return {
        ...state,
        // TODO: Do we need to reflect other than DEFAULT state types ?
        stateType: STATE_TYPE_DEFAULT,
        messageTitle: "",
        messageText: "",
      }

    case PostshopLocatorUIActionType.SET_UI_STATE_EXTENT_LOCATIONS_NOT_FOUND:
      return {
        ...state,
        stateType: STATE_TYPE_EXTENT_LOCATIONS_NOT_FOUND,
        messageTitle: "No location found.",
        messageText: "Please try zooming or panning the map or entering a new search term.",
      }

    case PostshopLocatorUIActionType.SET_UI_STATE_SEARCH_LOCATIONS_NOT_FOUND:
      return {
        ...state,
        stateType: STATE_TYPE_SEARCH_LOCATIONS_NOT_FOUND,
        messageTitle: "We can't find a match for your search.",
        messageText: "Please check the address you’ve entered and try again - avoiding characters such as !#$%^&@.",
      }

    case LocatorResultsActionType.SET_RESULTS:
      return {...state, resultsNotFound: false};
    case LocatorResultsActionType.RESULTS_LOADING:
      return {...state, loadingResults: true};
    case LocatorResultsActionType.RESULTS_LOADED:
      return {...state, loadingResults: false};
    case LocatorResultsActionType.CLEAR_RESULTS:
      return initialState;
    case LocatorResultsActionType.RESULTS_NOT_FOUND:
      return {...state, resultsNotFound: true};
    case LocatorResultsActionType.SET_SEARCH_QUERY:
      return {...state, searchQuery: action.payload.value};
    default:
      return state;
  }
};

export default locatorResultsListReducer;