import locatorSuggestionReducer, {ILocatorSuggestionsState} from "./locatorSuggestionReducer";
import locatorResultsListReducer, {ILocatorResultsState} from "./locatorResultsListReducer";
import {combineReducers, Reducer} from "redux";
import locatorMapReducer, {ILocatorMapState} from "./locatorMapReducer";
import postshopLocatorUIReducer from "./postshopLocatorUIReducer";

import {
  locationsReducer,
  ILocationsState,
} from "./external";

/**
 * State defined for postshop locator tool
 */
export interface PostshopLocatorState {
  suggestionState: ILocatorSuggestionsState;
  resultsState: ILocatorResultsState;
  mapState: ILocatorMapState; // TODO: Change this to map reducer
  locationsState: ILocationsState;
  uiState: any;
}

/**
 * Combine and export both postshop locator reducers
 */
const postshopLocatorReducer: Reducer<PostshopLocatorState> = combineReducers<PostshopLocatorState>({
  suggestionState: locatorSuggestionReducer,
  resultsState: locatorResultsListReducer,
  mapState: locatorMapReducer,
  locationsState: locationsReducer,
  uiState: postshopLocatorUIReducer,
});

export default postshopLocatorReducer;