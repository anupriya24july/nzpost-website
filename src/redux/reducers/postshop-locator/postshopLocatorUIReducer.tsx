import {Reducer} from "redux";
import {PKLStateMachineActions, PKLStateMachineActionType, PostshopLocatorUIActions, PostshopLocatorUIActionType,} from "./external";

export const STATE_TYPE_DEFAULT = "@@postshop-locator/STATE_TYPE_DEFAULT";
export const STATE_TYPE_EXACT_LOCATION = "@@postshop-locator/STATE_TYPE_EXACT_LOCATION";
export const STATE_TYPE_EXTENT_LOCATIONS = "@@postshop-locator/STATE_TYPE_EXTENT_LOCATIONS";
export const STATE_TYPE_NEARBY_LOCATIONS = "@@postshop-locator/STATE_TYPE_NEARBY_LOCATIONS";

export interface IPostshopLocatorUIState {
  readonly stateType: string;
  readonly shouldAdjustMapExtentToLocations: boolean;
}

// Initial ui domain state
export const initialState: IPostshopLocatorUIState = {
  stateType: STATE_TYPE_DEFAULT,
  shouldAdjustMapExtentToLocations: false,
};

const postshopLocatorUIReducer: Reducer<IPostshopLocatorUIState> = (state = initialState, action) => {
  switch ((action as PostshopLocatorUIActions | PKLStateMachineActions).type) {
    case PKLStateMachineActionType.SET_STATE_DEFAULT:
      return {
        ...state,
        stateType: STATE_TYPE_DEFAULT,
        shouldAdjustMapExtentToLocations: false,
      };

    case PostshopLocatorUIActionType.SET_UI_STATE_EXACT_LOCATION:
      return {
        ...state,
        stateType: STATE_TYPE_EXACT_LOCATION,
        shouldAdjustMapExtentToLocations: true,
      };

    case PostshopLocatorUIActionType.SET_UI_STATE_EXTENT_LOCATIONS:
      return {
        ...state,
        stateType: STATE_TYPE_EXTENT_LOCATIONS,
        shouldAdjustMapExtentToLocations: false,
      };

    case PostshopLocatorUIActionType.SET_UI_STATE_NEARBY_LOCATIONS:
      return {
        ...state,
        stateType: STATE_TYPE_NEARBY_LOCATIONS,
        shouldAdjustMapExtentToLocations: true,
      };

    default:
      return state;
  }
};

export default postshopLocatorUIReducer;
