import React from 'react';
import "./HomePage.scss";
import {Button} from "@material-ui/core";
import {paths} from "../../routes/routes";

/**
 * HomePage placeholder which for the time being acts as a quick navigator
 * @constructor
 */
const HomePage = () =>
  <div className="home-container">
    <h1>Developer Secret Quick Menu</h1>

    <Button className="home-item" variant="contained" href={paths.SUGGEST_ADDRESS}>Suggest an Address</Button>
    <Button className="home-item" variant="contained" href={paths.POSTSHOP_LOCATOR}>Postshop Locator</Button>
    <Button className="home-item" variant="contained" href={paths.PARCEL_FOR_YOU}>Parcel for You</Button>
  </div>
;

export default HomePage;