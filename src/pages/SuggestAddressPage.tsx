import * as React from "react";
import SuggestAddressFormContainer from "../containers/suggest-address/SuggestAddressFormContainer";
import MailboxPhoto from "../components/MailboxPhoto/MailboxPhoto";
import SingleColumnPage from "../components/lib/Pages/SingleColumnPage/SingleColumnPage";
import PageHeader from "../components/lib/Pages/PageHeader/PageHeader";
import TwoColumnPage from "../components/lib/Pages/TwoColumnPage/TwoColumnPage";

class SuggestAddressPage extends React.Component {
  componentDidMount() {
    document.title = "Suggest an Address";
  }

  render() {
    return <TwoColumnPage
      left={
        <SingleColumnPage
          header={<PageHeader title="Suggest an address"/>}
          body={<SuggestAddressFormContainer/>}
        />
      }
      right={<MailboxPhoto/>}
    />;
  }
}

export default SuggestAddressPage;
