import React from 'react';
import './ErrorPage.scss';
import logo_error from './nzpost-logo-error.svg';

/**
 * Stateless component for rendering an error page in the case where something goes wrong
 * @constructor
 */

document.title = "New Zealand Post";
const ErrorPage: React.SFC = () =>
  <div className="error-page-container">
    <img src={logo_error} className="error-page-logo"/>

    <div className="error-page-message">
      <h1>Looks like something's gone wrong.</h1>
      <h1>Please try again later.</h1>
    </div>
  </div>
;

export default ErrorPage;