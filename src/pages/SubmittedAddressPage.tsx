import * as React from "react";
import {StatelessComponent} from "react";
import MailboxPhoto from "../components/MailboxPhoto/MailboxPhoto";
import SingleColumnPage from "../components/lib/Pages/SingleColumnPage/SingleColumnPage";
import SubmittedAddressContainer from "../containers/suggest-address/SubmittedAddressContainer";
import TwoColumnPage from "../components/lib/Pages/TwoColumnPage/TwoColumnPage";

const SubmittedAddressPage: React.SFC<StatelessComponent> = () =>
  <TwoColumnPage
    left={
      <SingleColumnPage
        body={<SubmittedAddressContainer/>}
      />
    }
    right={<MailboxPhoto/>}
  />
;

export default SubmittedAddressPage;
