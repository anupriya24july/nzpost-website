import * as React from "react";

import {
  SingleColumnPage,
  SinglePageContainer,
  RedeliverySuccess,
  ToastNotificationContainer,
} from "./external";

export const TITLE = "Parcel Redelivery Success";

export class ParcelRedeliverySuccessPage extends React.Component {
  componentDidMount() {
    document.title = TITLE;
  }

  render() {
    return (
      <SingleColumnPage
        header={(
          <SinglePageContainer>
            <div className="confetti-background" />
            <ToastNotificationContainer />
          </SinglePageContainer>
        )}
        body={<RedeliverySuccess/>}
      />
    );
  }
}

export default ParcelRedeliverySuccessPage;