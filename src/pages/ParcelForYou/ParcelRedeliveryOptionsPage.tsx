import * as React from "react";

import {
  SingleColumnPage,
  SinglePageContainer,
  ToastNotificationContainer,
} from "./external";
import ParcelRedeliveryOptions from "../../components/ParcelForYou/RedeliveryOptions/RedeliveryOptions";

export const TITLE = "Parcel Redelivery Options";

export class ParcelRedeliveryOptionsPage extends React.Component {
  componentDidMount() {
    document.title = TITLE;
  }

  render() {
    return <SingleColumnPage
      header={(
        <SinglePageContainer>
          <ToastNotificationContainer />
        </SinglePageContainer>
      )}
      body={<ParcelRedeliveryOptions />}
    />
  }
};


export default ParcelRedeliveryOptionsPage;