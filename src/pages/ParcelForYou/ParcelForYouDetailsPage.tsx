import * as React from "react";

import {
  SingleColumnPage,
  SinglePageContainer,
  ToastNotificationContainer,
} from "./external";
import ParcelForYouDetails from "../../components/ParcelForYou/ParcelForYouDetails";

export const TITLE = "Parcel Details";

export class ParcelForYouDetailsPage extends React.Component {
  componentDidMount() {
    document.title = TITLE;
  }

  render() {
    return <SingleColumnPage
      header={(
        <SinglePageContainer>
          <ToastNotificationContainer />
        </SinglePageContainer>
      )}
      body={<ParcelForYouDetails />}
    />
  }
};

export default ParcelForYouDetailsPage;
