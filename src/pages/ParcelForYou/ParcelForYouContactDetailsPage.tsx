import * as React from "react";

import {
    SingleColumnPage,
    SinglePageContainer,
    ToastNotificationContainer,
    ContactDetails,
} from "./external";

export const TITLE = "Parcel for You";

export class ParcelForYouContactDetailsPage extends React.Component {
    componentDidMount() {
        document.title = TITLE;
    }

    render() {
        return <SingleColumnPage
            header={(
                <SinglePageContainer>
                    <ToastNotificationContainer />
                </SinglePageContainer>
            )}
            body={<ContactDetails />}
        />
    }
};


export default ParcelForYouContactDetailsPage;
