import * as React from "react";

import {
  SingleColumnPage,
  SinglePageContainer,
  ToastNotificationContainer,
  TrackingNumberCheckContainer,
} from "./external";

export const TITLE = "Parcel for You";

export class ParcelForYouPage extends React.Component {
  componentDidMount() {
    document.title = TITLE;
  }

  render() {
    return <SingleColumnPage
      header={(
        <SinglePageContainer>
          <ToastNotificationContainer />
        </SinglePageContainer>
      )}
      body={<TrackingNumberCheckContainer/>}
    />
  }
};


export default ParcelForYouPage;
