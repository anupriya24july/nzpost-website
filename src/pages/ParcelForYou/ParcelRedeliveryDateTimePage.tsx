import * as React from "react";

import {
  SingleColumnPage,
  SinglePageContainer,
  ToastNotificationContainer,
} from "./external";
import RedeliveryDateTime from "../../components/ParcelForYou/RedeliveryDateTime/RedeliveryDateTime";

export const TITLE = "Parcel Redelivery Date Time";

export class ParcelRedeliveryDateTimePage extends React.Component {

  componentDidMount() {
    document.title = TITLE;
  }

  render() {
    return <SingleColumnPage
      header={(
        <SinglePageContainer>
          <ToastNotificationContainer />
        </SinglePageContainer>
      )}
      body={<RedeliveryDateTime />}
    />
  }
};

export default ParcelRedeliveryDateTimePage;