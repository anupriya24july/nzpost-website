import * as React from "react";

import {
  RedeliveryAddress,
  SingleColumnPage,
  SinglePageContainer,
  ToastNotificationContainer,
} from "./external";

export const TITLE = "Parcel Redelivery Address";

export class ParcelRedeliveryAddressPage extends React.Component {
  componentDidMount() {
    document.title = TITLE;
  }

  render() {
    return <SingleColumnPage
      header={(
        <SinglePageContainer>
          <ToastNotificationContainer />
        </SinglePageContainer>
      )}
      body={<RedeliveryAddress />}
    />
  }
};


export default ParcelRedeliveryAddressPage;