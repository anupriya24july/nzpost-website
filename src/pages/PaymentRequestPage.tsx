import * as React from "react";
import SingleColumnPage from "../components/lib/Pages/SingleColumnPage/SingleColumnPage";
import PageHeader from "../components/lib/Pages/PageHeader/PageHeader";

class PaymentRequestPage extends React.Component {
  componentDidMount() {
    document.title = "Payment Request Page";
  }

  render() {
    return  <SingleColumnPage
      header={<PageHeader title="Payment"/>}
      body={<div></div>}
    />;
  }
}

export default PaymentRequestPage;