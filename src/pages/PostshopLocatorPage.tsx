import * as React from "react";
import SingleColumnPage from "../components/lib/Pages/SingleColumnPage/SingleColumnPage";
import PageHeader from "../components/lib/Pages/PageHeader/PageHeader";
import LocatorMapContainer from "../containers/postshop-locator/LocatorMapContainer";
import TwoColumnPage from "../components/lib/Pages/TwoColumnPage/TwoColumnPage";
import LocatorContainer from "../containers/postshop-locator/PostshopLocatorContainer";

class PostshopLocatorPage extends React.Component {
  componentDidMount() {
    document.title = "Postshop Locator";
  }

  render() {
    return <TwoColumnPage
      left={
        <SingleColumnPage
          header={<PageHeader title="Find a store"/>}
          body={<LocatorContainer/>}
        />
      }
      right={<LocatorMapContainer/>}
    />;
  }
}

export default PostshopLocatorPage;