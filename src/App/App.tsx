import * as React from "react";
import {history} from "../redux/AppStore";
import {ConnectedRouter} from "connected-react-router";
import routes from "../routes/routes";
import "./App.scss";
import logo from "./nzpost-logo.svg";

/**
 * Root App component
 * @constructor
 */
const App = () =>
  <main className="grid-container">
    <section className="header">
      <img className="logo" alt="NZ Post logo" src={logo}/>
    </section>
    <section className="page">
      <ConnectedRouter history={history}>
        {routes}
      </ConnectedRouter>
    </section>
  </main>
;

export default App;
