import {connect} from "react-redux";
import SubmittedAddress from "../../components/suggest-address/SubmittedAddress/SubmittedAddress";
import {
  getAddressConfirmed,
  getCityOrTown,
  getComments,
  getEmail,
  getFullName,
  getStreetAddress,
  getSuburb
} from "../../redux/selectors/suggestAddressSelectors";
import {Dispatch} from "redux";
import {reset} from "redux-form";

/**
 * Map properties from state to submitted address component
 * @param state
 */
const mapStateToProps = (state: any) => ({
  fullName: getFullName(state),
  email: getEmail(state),
  streetAddress: getStreetAddress(state),
  suburb: getSuburb(state),
  cityOrTown: getCityOrTown(state),
  addressConfirmed: getAddressConfirmed(state),
  comments: getComments(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  resetSuggestAddressForm: () => dispatch(reset("suggestAddress"))
});

const SubmittedAddressContainer = connect(mapStateToProps, mapDispatchToProps)(SubmittedAddress);

export default SubmittedAddressContainer;
