import {ConfigProps, reduxForm} from "redux-form";
import validation from "../../components/suggest-address/SuggestAddressForm/validation";
import SuggestAddressForm from "../../components/suggest-address/SuggestAddressForm/SuggestAddressForm";
import {ISuggestAddressFormValues, suggestAddressActions} from "../../redux/actions/suggestAddressActions";
import {Dispatch} from "redux";

/**
 * Configuration properties for suggest address formName
 *
 * onSubmit: Delegates method on formName submission to
 */
const config: ConfigProps = {
  form: "suggestAddress",
  destroyOnUnmount: false,
  onSubmit: (values: ISuggestAddressFormValues, dispatch: Dispatch) => {
    return new Promise(() => dispatch(suggestAddressActions.submit(values)));
  },
  validate: validation,
  initialValues: {
    addressConfirmed: "true"
  }
};

/**
 * Suggest an address container wires suggest an address form to reduxForm + config
 */
const SuggestAddressFormContainer = reduxForm(config)(SuggestAddressForm);

export default SuggestAddressFormContainer;