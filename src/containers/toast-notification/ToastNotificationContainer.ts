import {Dispatch} from "redux";
import {connect} from "react-redux";

import {

  IToastNotificationState,
  ToastNotification,
  toastNotificationActions,
  toastNotificationSelectors,
} from "./external";

const mapStateToProps = (state: any) => {
  const domainState: IToastNotificationState = state.toastNotification;
  return {
    message: toastNotificationSelectors.selectMessage(domainState),
    type: toastNotificationSelectors.selectType(domainState),
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onClickClose: () => dispatch(toastNotificationActions.dismissAll()),
});

const ToastNotificationContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ToastNotification);

export default ToastNotificationContainer;
