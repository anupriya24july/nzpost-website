import {ApplicationState} from "../../redux/AppStore";
import {connect} from "react-redux";
import QuickSearch from "../../components/PostshopLocator/QuickSearch/QuickSearch";
import {locatorResultsActions} from "../../redux/actions/postshop-locator/locatorResultsActions";
import {Dispatch} from "redux";

import { selectLocationList } from "./external";

/**
 * Map properties from state to locator search component
 * @param state
 */
const mapStateToProps = (state: ApplicationState) => ({
  ...state.postshopLocator.resultsState, // FIXME: refactor to conform updated state
  results: selectLocationList(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  quickSearchRequested: (city: string) => dispatch(locatorResultsActions.quickSearchRequested(city))
});

const QuickSearchContainer = connect(mapStateToProps, mapDispatchToProps)(QuickSearch);

export default QuickSearchContainer;
