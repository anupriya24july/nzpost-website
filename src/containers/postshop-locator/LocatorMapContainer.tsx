import {ApplicationState} from "../../redux/AppStore";
import {connect} from "react-redux";
import StandardMap, {IStandardMapIcon} from "../../components/lib/StandardMap/StandardMap";
import React from 'react';
import {Dispatch} from "redux";
import {ILocatorMapState} from "../../redux/reducers/postshop-locator/locatorMapReducer";
import postshopServices from "../../services/postshopServices";
import {locatorMapActions} from "../../redux/actions/postshop-locator/locatorMapActions";
import GeolocationIcon from "../../components/lib/Icons/GeolocationIcon/GeolocationIcon";
import {selectLocationList, selectShouldAdjustMapExtentToLocations} from "./external";

/**
 * Map properties from state to locator search component
 * @param state
 */
const mapStateToProps = (state: ApplicationState) => ({
  ...state.postshopLocator.mapState,
  locations: selectLocationList(state),
  shouldAdjustMapExtentToLocations: selectShouldAdjustMapExtentToLocations(state),
});


/**
 * Map all relevant functions to properties with dispatch events
 *
 * @param dispatch
 */
const mapDispatchToProps = (dispatch: Dispatch) => ({
  // FIXME: change value type to shareable Interface
  onMapExtentChange: (value: object) => dispatch(locatorMapActions.mapExtentChanged(value)),
});

export interface ILocatorMapProps extends ILocatorMapState {
  locations: Array<any>;
  onMapExtentChange: Function;
  shouldAdjustMapExtentToLocations: boolean;
}

/**
 * Stateless higher order component for rendering a standard map for postshop locator purposes
 * @param props
 * @constructor
 */
const LocatorMap = (props: ILocatorMapProps) => {
  const zoom = props.locations && props.locations.length === 1 ? 14 : props.zoom;
  let mapIcons = postshopServices.buildPostshopLocationMapIcons(props.locations);

  if (props.geo_latitude && props.geo_longitude) {
    const currentLocationIcon: IStandardMapIcon = {
      id: "geolocation",
      point: {latitude: props.geo_latitude, longitude: props.geo_longitude},
      createIconComponent: () => <GeolocationIcon/>,
      iconWidth: "40px",
      iconHeight: "40px"
    }
    mapIcons.push(currentLocationIcon);
  }

  return <StandardMap
    center={{
      "latitude": props.latitude,
      "longitude": props.longitude
    }}
    zoom={zoom}
    mapIcons={mapIcons}
    onMapExtentChange={props.onMapExtentChange}
    /**
     * @description Used to declare when we want map visible area
     *              to be panned and zoomed to passed map icons.
     */
    shouldAdjustExtentToIcons={props.shouldAdjustMapExtentToLocations}
  />
}

const LocatorMapContainer = connect(mapStateToProps, mapDispatchToProps)(LocatorMap);

export default LocatorMapContainer;


