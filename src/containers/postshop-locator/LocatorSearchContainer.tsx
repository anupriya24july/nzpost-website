import {connect} from "react-redux";
import {ApplicationState} from "../../redux/AppStore";
import {Dispatch} from "redux";
import {locatorSuggestionActions} from "../../redux/actions/postshop-locator/locatorSuggestionActions";
import LocatorSearch from "../../components/PostshopLocator/LocatorSearch/LocatorSearch";
import {locatorResultsActions} from "../../redux/actions/postshop-locator/locatorResultsActions";
import {PostshopLocation} from "../../models/PostshopLocation";

/**
 * Map properties from state to locator search component
 * @param state
 */
const mapStateToProps = (state: ApplicationState) => state.postshopLocator.suggestionState;

/**
 * Map all relevant functions to properties with dispatch events
 * @param dispatch
 */
const mapDispatchToProps = (dispatch: Dispatch) => ({
  fieldChangedRequested: (value: string) => dispatch(locatorSuggestionActions.fieldChangedRequested(value)),
  fetchRequested: (value: string) => dispatch(locatorSuggestionActions.fetchRequested(value)),
  clearSuggestionsRequested: () => dispatch(locatorSuggestionActions.clearSuggestionsRequested()),
  clearAllRequested: () => dispatch(locatorSuggestionActions.clearSearchRequested()),
  searchRequested: (value: string) => dispatch(locatorResultsActions.searchRequested(value, false)),
  loadSelectedRequested: (postshop: PostshopLocation) => dispatch(locatorResultsActions.loadSelectedRequested(postshop))
});

const LocatorSearchContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LocatorSearch);

export default LocatorSearchContainer;
