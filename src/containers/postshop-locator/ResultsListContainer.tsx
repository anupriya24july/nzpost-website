import {ApplicationState} from "../../redux/AppStore";
import {connect} from "react-redux";

import {ResultsList, selectLocationList, selectResultsMessageText, selectResultsMessageTitle} from "./external";


/**
 * Map properties from state to locator search component
 * @param state
 */
const mapStateToProps = (state: ApplicationState) => ({
  loadingResults: state.postshopLocator.resultsState.loadingResults,
  // FIXME: Reimplement state structure
  resultsNotFound: state.postshopLocator.resultsState.resultsNotFound,
  searchQuery: state.postshopLocator.resultsState.searchQuery,
  results: selectLocationList(state),
  messageTitle: selectResultsMessageTitle(state),
  messageText: selectResultsMessageText(state),
});

const ResultsListContainer = connect(mapStateToProps)(ResultsList);

export default ResultsListContainer;

