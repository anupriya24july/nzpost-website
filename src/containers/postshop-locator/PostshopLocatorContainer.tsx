import {ApplicationState} from "../../redux/AppStore";
import {connect} from "react-redux";
import React from 'react';
import {Dispatch} from "redux";
import {ILocatorMapState} from "../../redux/reducers/postshop-locator/locatorMapReducer";
import {locatorMapActions} from "../../redux/actions/postshop-locator/locatorMapActions";
import PostshopLocator from "../../components/PostshopLocator/PostshopLocator";
import {MapCoordinate} from "../../models/LocatorMap";

/**
 * Map properties from state to geolocation locator search component
 * @param state
 */
const mapStateToProps = (state: ApplicationState) => state.postshopLocator.mapState;


/**
 * Map all relevant functions to properties with dispatch events
 *
 * @param dispatch
 */
const mapDispatchToProps = (dispatch: Dispatch) => ({
  geolocationSearch: (geolocationCoords: MapCoordinate) => dispatch(locatorMapActions.geolocationSearch(geolocationCoords)),
});

export interface ILocatorMapProps extends ILocatorMapState {
  geolocationSearch: typeof locatorMapActions.geolocationSearch
}

/**
 * Stateless higher order component to enable geolocation
 * @param props
 * @constructor
 */
const LocatorContainer = (props: ILocatorMapProps) => {
  return <PostshopLocator
    geolocationSearch={props.geolocationSearch}
  />
}

const PostshopLocatorContainer = connect(mapStateToProps, mapDispatchToProps)(LocatorContainer);

export default PostshopLocatorContainer;


