import {ApplicationState} from "./external";
import {connect} from "react-redux";
import ParcelForYouDetails from "../../components/ParcelForYou/ParcelForYouDetails";

const mapStateToProps = (state: ApplicationState) => ({
  ...state.parcelForYou,
});

const ParcelForYouDetailsContainer = connect(
  mapStateToProps,
)(ParcelForYouDetails);

export default ParcelForYouDetailsContainer;
