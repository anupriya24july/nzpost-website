import {ApplicationState, TrackingNumberCheck} from "./external";
import {connect} from "react-redux";

const mapStateToProps = (state: ApplicationState) => ({
  validationErrorMessage: state.parcelForYou,
});

const TrackingNumberCheckContainer = connect(
  mapStateToProps,
)(TrackingNumberCheck);

export default TrackingNumberCheckContainer;
