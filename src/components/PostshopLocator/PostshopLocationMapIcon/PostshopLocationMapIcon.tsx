import * as React from "react";
import {PostshopLocation} from "../../../models/PostshopLocation";
import postshopServices, {IPostshopLocationIcon} from "../../../services/postshopServices";
import "./PostshopLocationMapIcon.scss";
import {Popover, withStyles} from "@material-ui/core";
import {ScreenHelper} from "../../../utilities/ScreenHelper";
import {DomHelper} from "../../../utilities/DomHelper";

/**
 * For better performance opted to handle the selected list by adding and removing the classname
 */
const addClassUnique = (element: HTMLElement, className: string) => {
  // remove all previously selected items
  const removeSelected = document.querySelector(`.${className}`);
  if (removeSelected) {
    removeSelected.classList.remove(className);
  }

  // select
  element.classList.add(className);
}

interface IPostshopLocationIconProps {
  id: string;
  postshop: PostshopLocation;
  classes: any,
  postOnClick: Function;
}

interface IPostshopLocationIconState {
  anchorEl: null | HTMLElement | ((element: HTMLElement) => HTMLElement);
}

const styles = (theme: any): any => ({
  popover: {
    pointerEvents: 'none',
  },
  paper: {
    padding: theme.spacing.unit,
  },
});

class PostshopLocationMapIcon extends React.Component<IPostshopLocationIconProps, IPostshopLocationIconState> {
  private postshopLocationIcon: IPostshopLocationIcon;

  constructor(props: IPostshopLocationIconProps) {
    super(props);

    // internal state, this.setState does not work in constructor
    this.state = {anchorEl: null};

    this.postshopLocationIcon = postshopServices.getIcon(this.props.postshop);

    this.handlePopoverOpen = this.handlePopoverOpen.bind(this);
    this.handlePopoverClose = this.handlePopoverClose.bind(this);
    this.onClickHandler = this.onClickHandler.bind(this);
  }

  handlePopoverOpen(event: any) {
    event.preventDefault();
    event.stopPropagation();

    this.setState({anchorEl: event.target});
  };

  handlePopoverClose(event: any) {
    event.preventDefault();
    event.stopPropagation();

    this.setState({anchorEl: null});
  };

  onClickHandler(event: any, postshop: PostshopLocation) {
    event.preventDefault();
    event.stopPropagation();

    const postshopId = postshopServices.buildPostshopId(postshop);
    ScreenHelper.scrollIntoScreenById(postshopId);
    addClassUnique(DomHelper.getElementByIdNotNull(postshopId), "selected-postshop");
    this.handlePopoverOpen(event);

    this.props.postOnClick(event);
  }

  render() {
    const {classes, postshop, id} = this.props;
    const {anchorEl} = this.state;
    const open = Boolean(anchorEl);

    return (
      <div>
        <img
          id={id}
          className="postshop-location-icon"
          src={this.postshopLocationIcon.url}
          width={this.postshopLocationIcon.width}
          height={this.postshopLocationIcon.height}
          onClick={(event) => this.onClickHandler(event, postshop)}
          aria-owns={open ? 'mouse-over-popover' : undefined}
          aria-haspopup="true"
          onMouseEnter={this.handlePopoverOpen}
          onMouseLeave={this.handlePopoverClose}
        />
        <Popover
          id="mouse-over-popover"
          PaperProps={{classes: {root: "postshop-location-popover"}}}
          className={classes.popover}
          open={open}
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}
          onClose={this.handlePopoverClose}
          disableRestoreFocus
          container={document.getElementsByClassName("gis-map-container")[0]}
        >
          <div className="postshop-location-tooltip-content-container">
            <p className="postshop-location-tooltip-title">{postshop.name}</p>
            <p className="postshop-location-tooltip-address">{postshop.address}</p>
          </div>
        </Popover>
      </div>
    );
  }
}

export default withStyles(styles)(PostshopLocationMapIcon);
