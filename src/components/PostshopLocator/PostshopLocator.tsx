import React from 'react';
import LocatorSearchContainer from "../../containers/postshop-locator/LocatorSearchContainer";
import ResultsListContainer from "../../containers/postshop-locator/ResultsListContainer";
import "./PostshopLocator.scss";
import QuickSearchContainer from "../../containers/postshop-locator/QuickSearchContainer";

interface IPostshopLocatorProps {
  geolocationSearch: Function;
}

class PostshopLocator extends React.Component<IPostshopLocatorProps> {

  constructor(props: IPostshopLocatorProps) {
    super(props);

    this.getGeoLocation = this.getGeoLocation.bind(this);
  }

  componentDidMount() {
    this.getGeoLocation();
  }

  private getGeoLocation(): void {
    const geolocation = navigator.geolocation;
    new Promise((resolve, reject) => {

      if (!geolocation) {
        reject(new Error('Not Supported'));
      }

      geolocation.getCurrentPosition((position) => {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;
        this.props.geolocationSearch({latitude, longitude});
      }, () => {
      });
    });
  };


  render() {
    return (
      <div className="postshop-locator-container">
        <LocatorSearchContainer/>
        <ResultsListContainer/>
        <QuickSearchContainer/>
      </div>
    );
  }
}

export default PostshopLocator;