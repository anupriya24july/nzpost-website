import React from 'react';
import {HoursEntity, PostshopLocation} from "../../../../models/PostshopLocation";
import "./PostshopCard.scss"
import moment from "moment";
import {Button} from "@material-ui/core";
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import postshopServices, {PostshopServiceType} from "../../../../services/postshopServices";
import directionIcon from "./direction-icon.svg";
import phoneIcon from "./phone-icon.svg";
import arrowDownIcon from "./arrow-down-icon.svg";
import greenTickIcon from "./green-tick-icon.svg";
import StandardMap from "../../../lib/StandardMap/StandardMap";
import {ScreenSize} from "../../../lib/ScreenSize/ScreenSize";
import MediaQuery from "react-responsive";
import ClockIcon from "../../../lib/Icons/ClockIcon/ClockIcon";
import {ScreenHelper} from "../../../../utilities/ScreenHelper";

interface IPostshopCardProps {
  postshop: PostshopLocation;
}

/**
 * Converts a postshop opening/closing time to a readable format
 * @param time
 */
const mapToDisplayTime = (time: string) => {
  // Convert 24 hour time to 12 hour time
  let formattedTime = moment(time, "HH:mm").format("h:ma");

  // Filter out times without minutes
  if (formattedTime.includes(":0")) {
    formattedTime = moment(formattedTime, "h:ma").format("ha");
  }

  return formattedTime;
};

/**
 * Maps day number to their explicit string counter parts
 * @param day
 */
const mapToDay = (day: number) => {
  switch (day) {
    case 0:
      return "Monday";
    case 1:
      return "Tuesday";
    case 2:
      return "Wednesday";
    case 3:
      return "Thursday";
    case 4:
      return "Friday";
    case 5:
      return "Saturday";
    default:
      return "Sunday";
  }
};

/**
 * Returns closed if it's closed on that specific day; otherwise return the formatted time string
 * @param hour
 */
const getTimeString = (hour: HoursEntity): string => {
  if (hour.closed) {
    return "Closed"
  }

  return mapToDisplayTime(hour.open) + " - " + mapToDisplayTime(hour.close);
};

/**
 * Returns current day variable based of browser date
 * @param currentDate
 */
const getCurrentDay = (currentDate: Date) => currentDate.getDay() - 1;

/**
 * Checks if a postshop is currently open
 * @param hours
 */
const postshopIsOpen = (hours: HoursEntity[] | undefined): boolean => {
  if (!hours) {
    return false;
  }

  const format = "hh:mm";

  const currentDate = new Date();

  const hour = getTodayHourEntity(hours);

  if (!hour) {
    return false;
  }

  const formattedCurrent = moment(currentDate, format);
  const formattedOpenTime = moment(hour.open, format);
  const formattedCloseTime = moment(hour.close, format);

  return formattedCurrent.isBetween(formattedOpenTime, formattedCloseTime);
};

/**
 * Returns the hour entity that matches the current browser browser day
 * @param hours
 */
const getTodayHourEntity = (hours: HoursEntity[]): HoursEntity | null => {
  const currentDay = getCurrentDay(new Date());
  const hour = hours.find(hour => hour.day == currentDay);

  if (!hour || hour.day != currentDay) {
    return null;
  }

  return hour;
};

/**
 * Get formatted opening hours for today
 * @param hours
 */
const getTodayOpeningHours = (hours: HoursEntity[]): string => {
  // CurrentDay is guaranteed to be one of the days within api payload
  const hour = getTodayHourEntity(hours);

  if (!hour) {
    return "";
  }

  return getTimeString(hour);
};

const handleServiceExpansion = (expanded: boolean, element: any) => {
  if (expanded) {
    ScreenHelper.scrollIntoScreenByElement(element);
  }
}

const PostshopCard: React.SFC<IPostshopCardProps> = ({postshop}: IPostshopCardProps) =>
  <div className="postshop-card" id={postshopServices.buildPostshopId(postshop)}>
    <div className="postshop-card-details">
      <span className="postshop-card-title">{postshop.name}</span>

      {/* location details */}
      <div className="postshop-location-details">
        <span className="postshop-card-address">{postshop.address}</span>
        {/* placeholder for distance */}
        <span className="postshop-distance-text">{}</span>
      </div>

      {/* get direction and phone number */}
      <div className="postshop-card-contact-toolbar">
        <Button className="postshop-card-directions-button"
                href={`https://www.google.com/maps/dir/?api=1&destination=${postshop.address}`} disableRipple>
          <img className="direction-icon-image" alt="Direction Icon" src={directionIcon}/>
          <span className="postshop-card-directions-text">Get directions</span>
        </Button>

        <Button className="postshop-card-phone-button" href={`tel:${postshop.phone}`} disableRipple>
          <img className="phone-icon-image" alt="Phone Icon" src={phoneIcon}/>
          <span className="postshop-card-phone-text">{postshop.phone}</span>
        </Button>
      </div>

      {/* opening hours */}
      {postshop.hours && postshop.hours.length > 0 &&
      <ExpansionPanel className="postshop-card-hours-container" onChange={(event, expanded) => handleServiceExpansion(expanded, event.target)}>
        <ExpansionPanelSummary className="postshop-card-hours-title"
                               expandIcon={<img className="arrow-down-icon-image" alt="Arrow Down Icon"
                                                src={arrowDownIcon}/>}
                               IconButtonProps={{"disableRipple": true}}>
          {/* clock icon */}
          <ClockIcon pathClass={`postshop-hours-icon ${!postshopIsOpen(postshop.hours) ? "postshop-hours-icon--closed" : ""}`} size={22}/>

          {/* current day opening hours */}
          <span className={`postshop-card-hours-status ${!postshopIsOpen(postshop.hours) ? "postshop-card-hours-status--closed" : ""}`}>
            {postshopIsOpen(postshop.hours) ? "Open now" : "Closed"}
            </span>
          {postshopIsOpen(postshop.hours) && <span className="postshop-card-hours-today">{getTodayOpeningHours(postshop.hours)}</span>}
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className="postshop-card-hours-details">
          {postshop.hours.map((hour, index) =>
            <div key={index} className={`postshop-card-hours-full ${getCurrentDay(new Date()) == hour.day ? "postshop-card-hours-full--current-day" : ""}`}>
              <span className="postshop-opening-hour-day" key={hour.day}>{mapToDay(hour.day)}</span>
              <span className="postshop-card-hours-text">{getTimeString(hour)}</span>
            </div>
          )}
        </ExpansionPanelDetails>

      </ExpansionPanel>}
    </div>

    {/* services */}
    {postshop.services && postshop.services.length > 0 &&
    <ExpansionPanel className="postshop-card-services-container" onChange={(event, expanded) => handleServiceExpansion(expanded, event.target)}>
      <ExpansionPanelSummary className="postshop-card-services-title"
                             expandIcon={<img className="arrow-down-icon-image" alt="Arrow Down Icon"
                                              src={arrowDownIcon}/>}
                             IconButtonProps={{"disableRipple": true}}>

        <span>Services</span>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails className="postshop-card-services-details">
        <ul className="postshop-card-services">
          {postshop.services && postshop.services.map((service, index) =>
            <li className="postshop-card-services-list-item" key={index}>
              <img className="green-tick-icon-image" alt="Green Tick Icon" src={greenTickIcon}/>
              <span className="autotest-service">{PostshopServiceType[service]}</span>
            </li>
          )}
        </ul>
      </ExpansionPanelDetails>
      <MediaQuery minWidth={ScreenSize.Mobile} maxWidth={ScreenSize.Desktop - 1}>
        <a href={`https://www.google.com/maps/dir/?api=1&destination=${postshop.address}`}>
          <div className="map-container">
            <StandardMap
              center={{
                "latitude": postshop.lat,
                "longitude": postshop.lng
              }}
              zoom={14}
              showMapControl={false}
              mapIcons={postshopServices.buildPostshopLocationMapIcons([postshop])}
            />
            <div className="map-overlay"></div>
          </div>
        </a>
      </MediaQuery>
    </ExpansionPanel>
    }
  </div>
;

export default PostshopCard;