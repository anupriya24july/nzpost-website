import React from 'react';
import {PostshopLocation} from "../../../models/PostshopLocation";
import {ConnectedReduxProps} from "../../../redux/ConnectedReduxProps";
import PostshopCard from "./PostshopCard/PostshopCard";
import "./ResultsList.scss"
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import {List, ListItem} from "@material-ui/core";
import NotFoundCard from "../../lib/NotFoundCard/NotFoundCard";

interface PropsFromState {
  results: PostshopLocation[];
  loadingResults: boolean;
  resultsNotFound: boolean;
  messageTitle: string;
  messageText: string;
}

type ResultsProps = PropsFromState & ConnectedReduxProps;

/**
 * Stateless component for rendering a list of location results
 * @constructor
 */
const ResultsList: React.SFC = ({
  results,
  loadingResults,
  resultsNotFound,
  messageTitle,
  messageText,
}: ResultsProps) => {
    const showLoading = loadingResults && !resultsNotFound;

    if (messageTitle || messageText) {
      return <NotFoundCard
        title={messageTitle}
        children={messageText}
      />
    }

    if (results.length < 1) {
      return null;
    }

    return <div className="results-list-container">
      {showLoading
        ? <CircularProgress size={35}/>
        : <List className="result-list-items">
          {results.map(postshop =>
            <ListItem key={`postshop-${postshop.id}`} className="result-list-item">
              <PostshopCard postshop={postshop} key={`postshop-${postshop.id}`}/>
            </ListItem>
          )}
        </List>}
    </div>;
  }
;

ResultsList.defaultProps = {
  results: [],
}

export default ResultsList;