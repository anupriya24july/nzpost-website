import {Button} from "@material-ui/core";
import React from "react";
import './QuickSearch.scss'
import {locatorResultsActions} from "../../../redux/actions/postshop-locator/locatorResultsActions";
import {PostshopLocation} from "../../../models/PostshopLocation";

interface IQuickSearchProps {
  quickSearchRequested: typeof locatorResultsActions.quickSearchRequested;
  results: PostshopLocation[];
  loadingResults: boolean;
  resultsNotFound: boolean;
}

/**
 * Provides postshop locator with a quick search functionality
 * @param quickSearchRequested function for requesting a specific city to be searched
 * @constructor
 */
const QuickSearch = ({quickSearchRequested, results, loadingResults, resultsNotFound}: IQuickSearchProps) => {
  const cities: string[] = ["Auckland", "Wellington", "Christchurch", "Dunedin"];
  const showLoading = loadingResults && !resultsNotFound;
  const showQuickSearch = results.length < 1 && !showLoading;

  if (!showQuickSearch) {
    return null;
  }

  return <div className="locator-quick-search-main">
    <div className="locator-quick-search-container">
      <div className="locator-quick-search-title">Quick search</div>
      <div className="locator-quick-search-button-container">
        {cities.map(city => <Button key={city} className="locator-quick-search-button" disableRipple
                                    onClick={() => quickSearchRequested(city)}>{city}</Button>)}
      </div>
    </div>
  </div>;
};

export default QuickSearch;
