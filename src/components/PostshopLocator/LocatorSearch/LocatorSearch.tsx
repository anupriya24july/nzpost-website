import React from 'react';
import * as Autosuggest from "react-autosuggest";
import './LocatorSearch.scss';
import {locatorSuggestionActions} from "../../../redux/actions/postshop-locator/locatorSuggestionActions";
import {ConnectedReduxProps} from "../../../redux/ConnectedReduxProps";
import DropdownResultWindow from "../../lib/Search/DropdownResultWindow/DropdownResultWindow";
import SearchBar from "../../lib/Search/SearchBar/SearchBar";
import SuggestedLocationResult from "./SuggestedLocationResult/SuggestedLocationResult";
import {locatorResultsActions} from "../../../redux/actions/postshop-locator/locatorResultsActions";
import {Suggestion} from "../../../models/AddressResultCollection";


/**
 * When suggestion is clicked, Autosuggest needs to populate the input
 * based on the clicked suggestion. Teach Autosuggest how to calculate the
 * input value for every given suggestion.
 * @param suggestion
 */
const getLocationSuggestionValue = (suggestion: Suggestion) => suggestion.name;

// Separate state props + dispatch props to their own interfaces.
interface PropsFromState {
  value: string;
  suggestions: Suggestion[];
  loadingSuggestions: boolean;
}

interface PropsFromDispatch {
  fieldChangedRequested: typeof locatorSuggestionActions.fieldChangedRequested;
  clearSuggestionsRequested: typeof locatorSuggestionActions.clearSuggestionsRequested;
  fetchRequested: typeof locatorSuggestionActions.fetchRequested;
  clearAllRequested: typeof locatorSuggestionActions.clearSearchRequested;
  searchRequested: typeof locatorResultsActions.searchRequested;
  loadSelectedRequested: typeof locatorResultsActions.loadSelectedRequested;
}

// Todo: May not need connected redux props since we don't need to access dispatch
type AllProps = PropsFromState & PropsFromDispatch & ConnectedReduxProps;

/**
 * Main postshop locator page
 * Important: need any otherwise typescript will complain
 */
const LocatorSearch: React.SFC<any> = ({
                                         value,
                                         suggestions,
                                         loadingSuggestions,
                                         clearAllRequested,
                                         clearSuggestionsRequested,
                                         fetchRequested,
                                         fieldChangedRequested,
                                         searchRequested,
                                         loadSelectedRequested
                                       }: AllProps) => {

  const onChange = (event: any, {newValue}: any) => fieldChangedRequested(newValue);
  const onSuggestionsFetchRequested = ({value}: any) => fetchRequested(value);

  const inputProps = {
    placeholder: 'Type an address, suburb, or city',
    value,
    onChange,
    loadingSuggestions,
    clearAllRequested,
    searchRequested,
    autoFocus: true
  };

  return (
    <div className="locator-search-container">
      <Autosuggest
        renderInputComponent={SearchBar}
        inputProps={inputProps}

        renderSuggestionsContainer={({containerProps, children}: any) =>
          <DropdownResultWindow children={children} containerProps={containerProps}/>}
        renderSuggestion={(suggestion: Suggestion) =>
          <SuggestedLocationResult suggestion={suggestion} searchValue={value}/>}

        onSuggestionsFetchRequested={onSuggestionsFetchRequested}
        onSuggestionsClearRequested={clearSuggestionsRequested}
        suggestions={suggestions}
        getSuggestionValue={getLocationSuggestionValue}
        onSuggestionSelected={(event: any, {suggestion}: any) => loadSelectedRequested(suggestion)}
      />
    </div>
  );
};

export default LocatorSearch;