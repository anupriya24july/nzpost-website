import React from "react";
import ListItem from "@material-ui/core/ListItem/ListItem";
import "./SuggestedLocationResult.scss"
import PostshopIcon from "../../../lib/Icons/PostshopIcon/PostshopIcon";
import locationIcon from "./location-icon.svg";
import {Suggestion} from "../../../../models/AddressResultCollection";

interface ISuggestedLocationProps {
  suggestion: Suggestion;
  searchValue: string;
}

/**
 * Reusable component for defining a location search result
 * @param suggestion
 * @param searchValue
 * @constructor
 */
const SuggestedLocationResult: React.SFC<ISuggestedLocationProps> = ({suggestion, searchValue}: ISuggestedLocationProps) => {
  const icon: JSX.Element = suggestion.address ? <PostshopIcon/> : <img className="location-icon-image" src={locationIcon}/>;

  return (
    <ListItem key={suggestion.name} dense button selected={searchValue === suggestion.name} className="location-suggestion-item">
      <div key={suggestion.name} className="location-suggestion-container">
        <div className="location-information-container">
          <div className="location-icon">
            {icon}
          </div>
          <div className="location-content">
            <span className="location-name-text">{suggestion.name}</span>
            {suggestion.address && <span className="location-address-text">{suggestion.address}</span>}
          </div>
        </div>
      </div>
    </ListItem>
  );
};

export default SuggestedLocationResult;