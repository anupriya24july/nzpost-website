import React, {SFC} from "react";

import SystemIcon from "src/components/lib/Icons/SystemIcon/SystemIcon";
import "./AddressSuggestion.scss";

export type Props = {
  fullAddress: string,
};

export const AddressSuggestion: SFC<Props> = ({
  fullAddress,
}) =>
  <div className="address-suggestion autotest-address-suggestion">
    <SystemIcon
      className="address-suggestion-icon autotest-address-suggestion-icon"
      name="marker-map-s"
    />
    {fullAddress}
  </div>

export default AddressSuggestion;
