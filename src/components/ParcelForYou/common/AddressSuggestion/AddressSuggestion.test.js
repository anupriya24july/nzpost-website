import React from "react";
import {shallow} from "enzyme";

import AddressSuggestion from "./AddressSuggestion";

describe("<AddressSuggestion />", () => {
  it("renders without crashing", () => {
    shallow(<AddressSuggestion />);
  });

  it("renders full address", () => {
    const fullAddress = "1 Queen street, Auckland, 1234";
    const wrapper = shallow(
      <AddressSuggestion fullAddress={fullAddress} />
    );
    expect(wrapper.text()).toContain(fullAddress);
  });
});