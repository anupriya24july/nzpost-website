import React from "react";
import "./PreviousButton.scss";
import {Link} from 'react-router-dom'
import {SystemIcon} from "../../RedeliveryOptions/external";

export type Props = {
  path: string,
}

export const PreviousButton = ({path}: Props) => (
  <Link to={path} className="previous-button"><SystemIcon className="left-arrow-button-icon" name={"arrow-left-o"}/>Previous</Link>
);

export default PreviousButton;
