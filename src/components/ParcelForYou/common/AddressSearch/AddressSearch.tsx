import React, {SFC} from "react";

import SearchWithSuggestions from "../../../lib/SearchWithSuggestions/SearchWithSuggestions";
import AddressSuggestion from "../AddressSuggestion/AddressSuggestion";
import "./AddressSearch.scss";

export type Props = {
  addressSearchSuggestions: Array<any>,
  onAddressChange: Function,
  onAddressSelected: Function,
  onAddressSubmitRequested?: Function,
  onAddressSuggestionsClearRequested: any,
  onAddressSuggestionsFetchRequested: Function,
  selectedAddress?: string
};

export type AddressSuggestion = {
  id: string,
  fullAddress: string,
};

export const AddressSearch: SFC<Props> = ({
  onAddressSelected,
  onAddressChange,
  onAddressSubmitRequested,
  onAddressSuggestionsClearRequested,
  onAddressSuggestionsFetchRequested,
  addressSearchSuggestions,
                                            selectedAddress
}) => {
  return (
    <SearchWithSuggestions
      className="address-search"
      placeholder="Enter a delivery address"
      value={selectedAddress}
      getSuggestionValue={({fullAddress}: AddressSuggestion) => fullAddress}
      onInputChange={(evt, {newValue}) => {
        onAddressChange({address: newValue})
      }}
      onInputPressEnter={onAddressSubmitRequested}
      onSuggestionSelected={(evt: any, {suggestion}: { suggestion: AddressSuggestion }) =>
        onAddressSelected({id: suggestion.id})}
      onSuggestionsClearRequested={onAddressSuggestionsClearRequested}
      onSuggestionsFetchRequested={({value}) => onAddressSuggestionsFetchRequested(value)}
      suggestions={addressSearchSuggestions}
      renderSuggestion={AddressSuggestion}
    />
  );
}

export default AddressSearch;