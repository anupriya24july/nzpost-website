import React from "react";
import {StandardButton} from "../../external";
import "./NextButton.scss";

export type Props = {
  className?: string,
  onClick: (evt: any) => void,
}

export const NextButton = ({className, onClick}: Props) => (
  <StandardButton
    className={`next-button parcel-next-button ${className ? className : ""}`}
    label="Next"
    buttonType="button"
    onClick={onClick}
    icon="arrow-right-o"
  />
);

export default NextButton;
