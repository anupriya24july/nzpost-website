import React from 'react';
import DeliveryDetails from "./DeliveryDetails/DeliveryDetails";

/**
 * The main form layout for parcel check form.
 * This form is rendered within a container for state management and isn't used directly.
 * @param handleSubmit
 * @constructor
 */
const ParcelForYouDetails = () =>
  <React.Fragment>
    <DeliveryDetails />
  </React.Fragment>
;

export default ParcelForYouDetails;
