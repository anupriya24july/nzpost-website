export const domainNames = {
  CHECK_PARCEL_NUMBER: "CHECK_PARCEL_NUMBER",
  DELIVERY_DETAILS: "DELIVERY_DETAILS",
  REDELIVERY_ADDRESS: "REDELIVERY_ADDRESS",
  REDELIVERY_OPTIONS: "REDELIVERY_OPTIONS",
  CONTACT_DETAILS: 'CONTACT_DETAILS',
  DATE_TIME: 'DATE_TIME',
  REDELIVERY_SUCCESS: "REDELIVERY_SUCCESS",
}

const {
  CHECK_PARCEL_NUMBER,
  DELIVERY_DETAILS,
  REDELIVERY_ADDRESS,
  REDELIVERY_OPTIONS,
  CONTACT_DETAILS,
  DATE_TIME,
  REDELIVERY_SUCCESS,
} = domainNames;

export const content = {
  [CHECK_PARCEL_NUMBER]: {
    title: "We missed you",
    subtitle: "We tried to deliver a parcel to you, but no one was home. Enter your tracking number below to choose from our redelivery options.",
  },
  [DELIVERY_DETAILS]: {
    title: "Original delivery address",
    subtitle: "Please enter the original delivery address so that we can help you with delivery options. Start typing and select an address from the list of suggestions.",
  },
  [REDELIVERY_ADDRESS]: {
    title: "Redeliver to a different address",
    subtitle: "Enter the address you would like your parcel sent to below.",
  },
  [REDELIVERY_OPTIONS]: {
    title: "Redelivery Options",
    subtitle: "What would you like to do? Choose a delivery option below.",
  },
  [CONTACT_DETAILS]: {
    title: 'Contact details',
    subtitle: 'Please provide your contact details, we will let you know about your parcel.'
  },
  [DATE_TIME]: {
    title: 'Choose a date and time',
    subtitle: 'What would you like to do?'
  },
  [REDELIVERY_SUCCESS]: {
    title: "Great! Your redelivery is confirmed",
    subtitle: "We've sent you an email with all the details."
  },
};

export const getContentDomain = (domainName: any) => content[domainName];
