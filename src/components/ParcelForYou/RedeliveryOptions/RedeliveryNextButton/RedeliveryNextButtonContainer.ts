import {Dispatch} from "redux";
import {connect} from "react-redux";

import RedeliveryNextButton from "./RedeliveryNextButton";
import {redeliveryOptionsActions} from "../external";

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onClickNext: () => dispatch(redeliveryOptionsActions.submitRedeliveryOptionRequested()),
});

export default connect(
  null,
  mapDispatchToProps
)(RedeliveryNextButton);
