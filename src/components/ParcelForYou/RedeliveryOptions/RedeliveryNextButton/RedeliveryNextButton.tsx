import React from "react";
import {NextButton, redeliveryOptionsActions} from "../external";

export type Props = {
  onClickNext: typeof redeliveryOptionsActions.submitRedeliveryOptionRequested,
}

export const RedeliveryNextButton = ({onClickNext}: Props) => (
  <NextButton
    className="autotest-redelivery-options-next-button"
    onClick={onClickNext}
  />
);

export default RedeliveryNextButton;
