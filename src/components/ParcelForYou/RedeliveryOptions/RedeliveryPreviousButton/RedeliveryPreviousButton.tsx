import React from "react";
import {paths, PreviousButton} from "../external";

export const RedeliveryPreviousButton = () => (
  <PreviousButton path={paths.PARCEL_DELIVERY_DETAILS}/>
);

export default RedeliveryPreviousButton;
