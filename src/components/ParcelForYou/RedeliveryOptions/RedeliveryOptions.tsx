import React from "react";

import {SinglePageContainer, Subtitle, Title} from "../external";
import {domainNames, getContentDomain, RedeliveryNextButtonContainer, RedeliveryPreviousButton, RedeliveryRadioButtonsContainer} from "./external";

const ParcelRedeliveryOptions = () => {
  const content = getContentDomain(domainNames.REDELIVERY_OPTIONS);

  return (
    <SinglePageContainer>
      {content.title && (
        <Title className="autotest-delivery-details-header" color="redNzPost" size="lg">
          {content.title}
        </Title>
      )}

      {content.subtitle && (
        <Subtitle className="PageSubtitle" size="md">
          {content.subtitle}
        </Subtitle>
      )}

      <RedeliveryRadioButtonsContainer/>
      <RedeliveryPreviousButton/>
      <RedeliveryNextButtonContainer/>
    </SinglePageContainer>
  );
}

export default ParcelRedeliveryOptions;
