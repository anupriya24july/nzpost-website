import {Dispatch} from "redux";
import {connect} from "react-redux";

import RedeliveryRadioButtons from "./RedeliveryRadioButtons";
import {ApplicationState, redeliveryOptionsActions} from "../external";

const mapStateToProps = (state: ApplicationState) => ({
  options: state.parcelForYou.redeliveryOptions
});
const mapDispatchToProps = (dispatch: Dispatch) => ({
  onClick: (option: string) => dispatch(redeliveryOptionsActions.changeRedeliveryOption(option)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RedeliveryRadioButtons);
