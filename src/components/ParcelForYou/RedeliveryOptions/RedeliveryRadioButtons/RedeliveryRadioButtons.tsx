import React from "react";
import {IRedeliveryOptionsState, RadioButtonGroup} from "../external";

export type Props = {
  onClick: Function,
  options: IRedeliveryOptionsState
}

export const RedeliveryRadioButtons = ({onClick, options}: Props) => (
  <RadioButtonGroup
    value={options.selectedOption}
    onValueChange={onClick}
    radios={[
      {
        "label": "Redeliver to the same address",
        "value": options.sameAddress ? "sameAddress" : "disabled",
        "className": "redelivery-option autotest-same-address",
        "disabled": !options.sameAddress
      },
      {
        "label": "Redeliver to a different address",
        "value": "differentAddress",
        "className": "redelivery-option autotest-different-address",
        "disabled": !options.differentAddress
      },
      {
        "label": "Redeliver to a collection point",
        "value": "parcelCollect",
        "className": "redelivery-option autotest-parcel-collect",
        // "disabled": !options.parcelCollect
        // disabled temporarily
        "disabled": true
      }
    ]}
    className="autotest-redelivery-option-confirmed"
  />
);

export default RedeliveryRadioButtons;
