import {Dispatch} from "redux";
import {connect} from "react-redux";

import {deliveryDetailsActions} from "../../external";
import DeliveryDetailsNextButton from "./DeliveryDetailsNextButton";

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onClick: () => dispatch(deliveryDetailsActions.submitRequested()),
});

export default connect(
  null,
  mapDispatchToProps
)(DeliveryDetailsNextButton);
