import React from "react";
import NextButton from "../../common/NextButton/NextButton";

export type Props = {
  onClick: () => void,
}

export const DeliveryDetailsNextButton = ({onClick}: Props) => (
  <NextButton
    className="autotest-delivery-details-next-button"
    onClick={onClick}
  />
);

export default DeliveryDetailsNextButton;
