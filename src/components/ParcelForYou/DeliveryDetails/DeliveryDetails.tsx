import React from "react";

import {domainNames, getContentDomain} from ".././content";
import {
  SinglePageContainer, Subtitle, Title,
} from "../external";
import DeliveryAddressSearchContainer from "./DeliveryAddressSearch/DeliveryAddressSearchContainer";
import DeliveryDetailsNextButtonContainer from "./DeliveryDetailsNextButton/DeliveryDetailsNextButtonContainer";

const DeliveryDetails = () => {
  const content = getContentDomain(domainNames.DELIVERY_DETAILS);

  return (
    <SinglePageContainer>
      {content.title && (
        <Title className="autotest-delivery-details-header" color="redNzPost" size="lg">
          {content.title}
        </Title>
      )}

      {content.subtitle && (
        <Subtitle className="PageSubtitle" size="md">
          {content.subtitle}
        </Subtitle>
      )}

      <DeliveryAddressSearchContainer/>
      <DeliveryDetailsNextButtonContainer />
    </SinglePageContainer>
  );
}

export default DeliveryDetails;
