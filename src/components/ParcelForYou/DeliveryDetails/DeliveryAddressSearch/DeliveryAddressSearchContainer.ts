import {connect} from "react-redux";
import {Dispatch} from "redux";
import {ApplicationState, deliveryDetailsActions, selectAddressSearchSuggestions,} from "../../external";

import AddressSearch from "../../common/AddressSearch/AddressSearch";

const mapStateToProps = (state: ApplicationState) => ({
  addressSearchSuggestions: selectAddressSearchSuggestions(state),
  selectedAddress: state.parcelForYou.deliveryDetails.address
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onAddressChange: ({address}: { address: string }) =>
    dispatch(deliveryDetailsActions.addressChanged({address})),
  onAddressSelected: ({id}: { id: string }) =>
    dispatch(deliveryDetailsActions.addressSelected({id})),
  onAddressSubmitRequested: () => dispatch(deliveryDetailsActions.submitRequested()),
  onAddressSuggestionsClearRequested: () =>
    dispatch(deliveryDetailsActions.addressSuggestionsClearRequested()),
  onAddressSuggestionsFetchRequested: (value: string) =>
    dispatch(deliveryDetailsActions.addressSuggestionsFetchRequested(value)),
});

const AddressSearchContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddressSearch);

export default AddressSearchContainer;