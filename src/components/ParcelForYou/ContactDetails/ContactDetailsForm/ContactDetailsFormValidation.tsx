/**
 * Client side validation requirements for the contact details form
 * @param values
 */
import { IContactDetailsFormValues } from "../../../../redux/actions/parcel-for-you/contactDetailsActions";
import { isNil, isEmpty, trim } from "lodash";

const validation = (values: IContactDetailsFormValues) => {
  const errors: any = {};
  const isNilorEmpty = (value: string) => isNil(value) || isEmpty(value);

  const firstName = trim(values.firstName);
  const lastName = trim(values.lastName);
  const email = trim(values.email);
  const phone = trim(values.phone);

  // First name validation
  if (isNilorEmpty(firstName)) {
    errors.firstName = "Please enter your first name."
  } else if (!/^[a-zA-Z\s-']*$/.test(firstName)) {
    errors.firstName = "Please enter a valid name, avoiding characters such as #$%.";
  }

  // Last name validation
  if (isNilorEmpty(lastName)) {
    errors.lastName = "Please enter your last name."
  } else if (!/^[a-zA-Z\s-']*$/.test(lastName)) {
    errors.lastName = "Please enter a valid name, avoiding characters such as #$%.";
  }

  // Email validation
  if (isNilorEmpty(email)) {
    errors.email = "Please enter your email."
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
    errors.email = "Please enter your email address in this format: name@example.com and avoid characters such as #$%."
  }

  // Phone validation
  if(isNilorEmpty(phone)) {
    errors.phone = "Please enter your phone number."
  } else if (!/^[0-9]*$/.test(phone)) {
    errors.phone = "Please enter a valid phone number, using characters such as 123.";
  }

  return errors;
};

export default validation;