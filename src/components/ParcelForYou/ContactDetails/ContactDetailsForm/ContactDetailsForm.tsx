import React from "react";
import {Field, InjectedFormProps} from "redux-form";

import FormText from "../../../lib/Form/Text/FormText";
import {StandardButton} from "../../TrackingNumberCheck/external";
import PreviousButton from "../../common/PreviousButton/PreviousButton";
import {paths} from "../../../../routes/routes";
import './ContactDetailsForm.scss';

export type PropsFromState = {
    contactDetails: any,
}

type AllProps = PropsFromState & InjectedFormProps;

export const ContactDetailsForm = ({handleSubmit, contactDetails} : AllProps) => (
    <div>
        <form>
            <div className="contact-details-form-grid">
                <div className="contact-details-first-name">
                    <Field name="firstName" className="contact-details-first-name-input" component={FormText}
                           label="First name" autoFocus={true} value={contactDetails.firstName} />
                </div>
                <div className="contact-details-last-name">
                    <Field name="lastName" className="contact-details-last-name-input" component={FormText}
                           label="Last name" value={contactDetails.lastName} />
                </div>
                <div className="contact-details-company">
                    <Field name="company" className="contact-details-company-input" component={FormText}
                           label="Company (optional)" value={contactDetails.company} />
                </div>
                <div className="contact-details-email">
                    <Field name="email" className="contact-details-email-input" component={FormText}
                           label="Email address" value={contactDetails.email} />
                </div>
                <div className="contact-details-phone">
                    <Field name="phone" className="contact-details-phone-input" component={FormText}
                           label="Phone number" value={contactDetails.phone} />
                </div>
            </div>
            <div className="contact-details-actions-container">
                <PreviousButton path={paths.PARCEL_REDELIVERY_OPTIONS} />
                <StandardButton
                    className="next-button contact-details-submit"
                    label="Next"
                    buttonType={"button"}
                    onClick={handleSubmit}
                    icon="arrow-right-o"
                />
            </div>
        </form>
    </div>
);