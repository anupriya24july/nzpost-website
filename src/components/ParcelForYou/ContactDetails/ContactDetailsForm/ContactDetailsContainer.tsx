import {ConfigProps, reduxForm} from "redux-form";
import {ContactDetailsForm} from "./ContactDetailsForm";
import validation from "./ContactDetailsFormValidation";
import {contactDetailsActions} from "../../../../redux/actions/parcel-for-you/contactDetailsActions";
import {ApplicationState} from "../../../../redux/AppStore";
import {
    selectContactDetails,
} from "../../../../redux/selectors/parcelForYouSelectors";
import {Dispatch} from "redux";
import {connect} from "react-redux";

const config: ConfigProps = {
    form: "contactDetailsForm",
    destroyOnUnmount: false,
    onSubmit: (values: any, dispatch: any) => {
       return new Promise(() => dispatch(contactDetailsActions.submit(values)));
    },
    validate: validation
};

const mapStateToProps = (state: ApplicationState) => ({
    contactDetails: selectContactDetails(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({});

/**
 * Suggest an address container wires suggest an address form to reduxForm + config
 */
const ContactDetailsContainer = connect(mapStateToProps, mapDispatchToProps)(reduxForm(config)(ContactDetailsForm));

export default ContactDetailsContainer;