import React from "react";
import {domainNames, getContentDomain} from ".././content";
import SinglePageContainer from "../../lib/SinglePageContainer/SinglePageContainer";
import {Subtitle, Title} from "../TrackingNumberCheck/external";
import ContactDetailsContainer from "./ContactDetailsForm/ContactDetailsContainer";


const ContactDetails = () => {
    const {CONTACT_DETAILS} = domainNames;
    const content = getContentDomain(CONTACT_DETAILS);
    return (
        <SinglePageContainer>
            {content.title && <Title color="redNzPost" size="lg">{content.title}</Title>}
            {content.subtitle && <Subtitle className="PageSubtitle" size="md">{content.subtitle}</Subtitle>}

            <ContactDetailsContainer />
        </SinglePageContainer>
    );
};


export default ContactDetails;
