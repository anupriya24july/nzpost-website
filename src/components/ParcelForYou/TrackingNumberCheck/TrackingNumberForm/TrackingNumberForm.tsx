import React from 'react';
import {Field, InjectedFormProps} from "redux-form";
import {FormText, FormTextError, StandardButton, trackingNumberCheckActions} from "../external";
import "./TrackingNumberForm.scss";

/**
 * The main form layout for tracking number check form.
 * This form is rendered within a container for state management and isn't used directly.
 * @param handleSubmit
 * @constructor
 */
export type PropsFromState = {
  errorMessageFromApi: any,
  requestTrackingNumberCheck: typeof trackingNumberCheckActions.requestTrackingNumberCheck
}

type AllProps = PropsFromState & InjectedFormProps;

export const TrackingNumberForm = ({handleSubmit, errorMessageFromApi, requestTrackingNumberCheck}: AllProps) => {
    const className = errorMessageFromApi.message ? "autotest-parcel-tracking-number form-text-field-input-error" : "autotest-parcel-tracking-number";
    const invalidErrorId = errorMessageFromApi.message ? "form-text-field-input-error" : "";
    const isError = !!errorMessageFromApi.message;

    return (
      <div className="form-container tracking-number-form-container form-container--ugly-fix">
        <form className="tracking-number-form" onSubmit={(e) => {
          e.preventDefault();
          requestTrackingNumberCheck()
        }}>
          <div>
            <Field
              name="trackingNumber"
              component={FormText}
              label="Tracking Number"
              className={className}
              autoFocus={true}
              error={isError}
              id={invalidErrorId}
            />
            <FormTextError
              className="autotest-tracking-number-form-text-error-message"
              name="trackingNumber"
              touched={true}
              error={errorMessageFromApi.message}
              warning=""
            />
          </div>
          <div>
            <StandardButton
              className="next-button tracking-number-submit"
              label="Next"
              buttonType={"button"}
              onClick={handleSubmit}
              icon="arrow-right-o"
            />
          </div>
        </form>
      </div>
    )
  }
;

export default TrackingNumberForm;
