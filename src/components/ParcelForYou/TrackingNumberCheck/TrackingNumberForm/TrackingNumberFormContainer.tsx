import {connect} from "react-redux";
import {ConfigProps, reduxForm} from "redux-form";
import {Dispatch} from "redux";
import {ApplicationState, ITrackingNumberFormValues, trackingNumberCheckActions, TrackingNumberForm} from "../external";

/**
 * Configuration properties for tracking number form
 *
 * onSubmit: Delegates method on formName submission to
 */
const config: ConfigProps = {
  form: "trackingNumberForm",
  destroyOnUnmount: false,
  onSubmit: (values: ITrackingNumberFormValues, dispatch: Dispatch) => {
    dispatch(trackingNumberCheckActions.requestTrackingNumberCheck(values));
  },
  onChange: (trackingNumber, dispatch) => {
    dispatch(trackingNumberCheckActions.changeTrackingNumber(trackingNumber));
  },
};

export const mapStateToProps = (state: ApplicationState) => ({
  errorMessageFromApi: state.parcelForYou.trackingNumberCheck.validation.message
})


const mapDispatchToProps = (dispatch: Dispatch) => ({
  requestTrackingNumberCheck: () => dispatch(trackingNumberCheckActions.requestTrackingNumberCheck())
});
/**
 * Parcel check form container wires parcel check form to reduxForm + config
 */

const TrackingNumberFormContainer = connect(mapStateToProps, mapDispatchToProps)(reduxForm(config)(TrackingNumberForm));

export default TrackingNumberFormContainer;