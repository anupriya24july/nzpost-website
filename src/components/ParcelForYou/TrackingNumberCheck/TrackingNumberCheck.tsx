import React from "react";
import {domainNames, getContentDomain} from ".././content";
import SinglePageContainer from "../../lib/SinglePageContainer/SinglePageContainer";
import {Subtitle, Title} from "./external";
import TrackingNumberFormContainer from "./TrackingNumberForm/TrackingNumberFormContainer";


export const TrackingNumberCheck = () => {
  const {CHECK_PARCEL_NUMBER} = domainNames;
  const content = getContentDomain(CHECK_PARCEL_NUMBER)
  return (
    <SinglePageContainer>
      {content.title && <Title color="redNzPost" size="lg">{content.title}</Title>}
      {content.subtitle && <Subtitle className="PageSubtitle" size="md">{content.subtitle}</Subtitle>}
      <TrackingNumberFormContainer/>
    </SinglePageContainer>
  );
}


export default TrackingNumberCheck;
