import React from "react";
import {domainNames, getContentDomain} from "../content";
import SinglePageContainer from "../../lib/SinglePageContainer/SinglePageContainer";
import {Subtitle, Title} from "../external";
import SystemIcon from "../../lib/Icons/SystemIcon/SystemIcon";
import "./RedeliverySuccess.scss";

export type Props = {
  trackingNumber: string;
  deliveryAddress: string;
}

const RedeliverySuccess = ({trackingNumber, deliveryAddress}: Props) => {
  const {REDELIVERY_SUCCESS} = domainNames;
  const content = getContentDomain(REDELIVERY_SUCCESS);
  return (
    <SinglePageContainer>
      {content.title && <Title color="redNzPost" size="lg">{content.title}</Title>}
      {content.subtitle && <Subtitle className="PageSubtitle" size="md">{content.subtitle}</Subtitle>}

      <div className="card-success">
        <div className="row-container">
          <div className="row">
            <div className="icon parcel-icon" >
              <SystemIcon name="parcel-s" height="13px" width="13px"/>
            </div>
            <div className="label">Redelivery reference number</div>
            <div className="success-info-ref-number value">{trackingNumber}</div>
          </div>
        </div>
        <div className="row-container">
          <div className="row">
            <div className="icon pin-icon">
              <SystemIcon name="locator-s" height="13px" width="13px"/>
            </div>
            <div className="label">Redeliver to </div>
            <div className="success-info-redelivery-address value">{deliveryAddress}</div>
          </div>
        </div>
        <div className="row-container">
          <div className="row">
            <div className="icon calendar-icon">
              <SystemIcon name="calendar-s" height="13px" width="13px"/>
            </div>
            <div className="label">Redeliver on</div>
            <div className="success-info-redelivery-date value">Thursday 9th MAY</div>
          </div>
        </div>
      </div>

    </SinglePageContainer>
  )
};

export default RedeliverySuccess;