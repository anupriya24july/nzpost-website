import {connect} from "react-redux";
import {Dispatch} from "redux";

import RedeliverySuccess from "./RedeliverySuccess";
import {ApplicationState, selectTrackingNumber, selectDeliveryAddress, selectRedeliveryAddress} from "../external";
import {getSelectedRedeliveryOption} from "../../../redux/selectors/parcelForYouSelectors";

const mapStateToProps = (state: ApplicationState) => {

  const selectedOption = getSelectedRedeliveryOption(state);
  let address = null;

  if (selectedOption === 'sameAddress') {
      address = selectDeliveryAddress(state);
  } else if (selectedOption === 'differentAddress') {
      address = selectRedeliveryAddress(state);
  }

  return ({
      trackingNumber: selectTrackingNumber(state),
      deliveryAddress: address
  });
};
const mapDispatchToProps = (dispatch: Dispatch) => ({});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RedeliverySuccess);
