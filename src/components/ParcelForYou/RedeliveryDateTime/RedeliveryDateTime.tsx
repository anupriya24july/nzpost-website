import React from "react";
import {domainNames, getContentDomain} from ".././content";
import SinglePageContainer from "../../lib/SinglePageContainer/SinglePageContainer";
import {Subtitle, Title} from "../TrackingNumberCheck/external";
import PreviousButton from "../common/PreviousButton/PreviousButton";
import {paths} from "../../../routes/routes";


const RedeliveryDateTime = () => {
    const {DATE_TIME} = domainNames;
    const content = getContentDomain(DATE_TIME);
    return (
        <SinglePageContainer>
            {content.title && <Title color="redNzPost" size="lg">{content.title}</Title>}
            {content.subtitle && <Subtitle className="PageSubtitle" size="md">{content.subtitle}</Subtitle>}

            <PreviousButton path={paths.PARCEL_REDELIVERY_CONTACT_DETAILS} />

        </SinglePageContainer>
    );
};


export default RedeliveryDateTime;
