import React from "react";
import NextButton from "../../common/NextButton/NextButton";

export type Props = {
  onClick: () => void,
}

export const RedeliveryAddressNextButton = ({onClick}: Props) => (
  <NextButton
    className="autotest-redelivery-address-next-button"
    onClick={onClick}
  />
);

export default RedeliveryAddressNextButton;