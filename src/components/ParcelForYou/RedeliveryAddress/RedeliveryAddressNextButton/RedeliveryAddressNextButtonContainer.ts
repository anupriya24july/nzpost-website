import {Dispatch} from "redux";
import {connect} from "react-redux";

import {redeliveryAddressActions} from "../../external";
import RedeliveryAddressNextButtonn from "./RedeliveryAddressNextButton";

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onClick: () => dispatch(redeliveryAddressActions.submitRequested()),
});

export default connect(
  null,
  mapDispatchToProps
)(RedeliveryAddressNextButtonn);