import React from "react";

import {domainNames, getContentDomain} from "../content";
import {SinglePageContainer, Subtitle, Title} from "../external";
import {RedeliveryAddressNextButtonContainer, RedeliveryAddressPreviousButton, RedeliveryAddressSearchContainer} from "./external";

export const RedeliveryAddress = () => {
  const content = getContentDomain(domainNames.REDELIVERY_ADDRESS);

  return (
    <SinglePageContainer>
      {content.title && (
        <Title
          className="autotest-redelivery-address-title"
          color="redNzPost"
          size="lg"
        >
          {content.title}
        </Title>
      )}

      {content.subtitle && (
        <Subtitle
          className="autotest-redelivery-address-subtitle"
          size="md"
        >
          {content.subtitle}
        </Subtitle>
      )}

      <RedeliveryAddressSearchContainer/>

      <RedeliveryAddressPreviousButton/>
      <RedeliveryAddressNextButtonContainer/>
    </SinglePageContainer>
  );
}

export default RedeliveryAddress;
