import {connect} from "react-redux";
import {Dispatch} from "redux";
import {
  ApplicationState,
  deliveryDetailsActions,
  redeliveryAddressActions,
  selectAddressSearchSuggestions,
  selectRedeliveryAddress,
} from "../../external";

import AddressSearch from "../../common/AddressSearch/AddressSearch";

const mapStateToProps = (state: ApplicationState) => ({
  addressSearchSuggestions: selectAddressSearchSuggestions(state),
  selectedAddress: selectRedeliveryAddress(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onAddressChange: ({address}: {address: string}) =>
    dispatch(redeliveryAddressActions.addressChanged({address})),
  onAddressSelected: ({id}: {id: string}) =>
    dispatch(redeliveryAddressActions.addressSelected({id})),
  onAddressSubmitRequested: () => {
    dispatch(redeliveryAddressActions.submitRequested())
  },
  onAddressSuggestionsClearRequested: () =>
    dispatch(deliveryDetailsActions.addressSuggestionsClearRequested()),
  onAddressSuggestionsFetchRequested: (value: string) =>
    dispatch(deliveryDetailsActions.addressSuggestionsFetchRequested(value)),
});

const RedeliveryAddressSearchContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddressSearch);

export default RedeliveryAddressSearchContainer;