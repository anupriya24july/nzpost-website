import React from "react";
import {paths, PreviousButton} from "../external";

export const RedeliveryAddressPreviousButton = () => (
  <PreviousButton path={paths.PARCEL_REDELIVERY_OPTIONS}/>
);

export default RedeliveryAddressPreviousButton;
