import * as React from "react";
import "./MailboxPhoto.scss";

const MailboxPhoto: React.SFC = () =>
  <div className="mailbox-photo" aria-readonly role="mailbox-photo" aria-labelledby="Mailbox photo"/>
;

export default MailboxPhoto;