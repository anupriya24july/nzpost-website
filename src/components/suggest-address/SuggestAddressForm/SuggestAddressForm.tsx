import React from 'react';
import {Field, InjectedFormProps} from "redux-form";
import FormText from "../../lib/Form/Text/FormText";
import FormRadioGroup from "../../lib/Form/RadioGroup/FormRadioGroup";
import FormTextArea from "../../lib/Form/TextArea/FormTextArea";
import "./SuggestAddressForm.scss";
import StandardButton from "../../lib/StandardButton/StandardButton";

/**
 * The main form layout for suggest an address.
 * This form is rendered within a container for state management and isn't used directly.
 * @param handleSubmit
 * @param pristine
 * @param valid
 * @param submitting
 * @constructor
 */
const SuggestAddressForm = ({handleSubmit, pristine, valid, submitting}: InjectedFormProps) =>
  <div className="form-container">
    <p className="text-center">Tell us the address that you think is missing and we'll look into it for you.</p>
    <form onSubmit={handleSubmit} className="suggest-address-form auto-suggest-address-form">
      <div>
        <Field name="fullName" component={FormText} label="Your full name" className="autotest-suggest-full-name"
               autoFocus={true}/>
      </div>
      <div>
        <Field name="email" component={FormText} label="Your email" type="email" className="autotest-suggest-email"/>
      </div>
      <div>
        <Field name="streetAddress" component={FormText} label="Street, PO Box or Community Mailbox"
               className="autotest-suggest-street-address"/>
      </div>
      <div>
        <Field name="suburb" component={FormText} label="Suburb" className="autotest-suggest-suburb"/>
      </div>
      <div>
        <Field name="cityOrTown" component={FormText} label="City or Town" className="autotest-suggest-city-or-town"/>
      </div>
      <div>
        <Field
          name="addressConfirmed"
          component={FormRadioGroup}
          label="Is this your own address?"
          radios={[
            {
              "label": "Yes, this is my own address.",
              "value": "true",
              "className": "autotest-address-confirmed-true"
            },
            {
              "label": "No, this is someone else's address.",
              "value": "false",
              "className": "autotest-address-confirmed-false"
            }
          ]}
          className="autotest-suggest-address-confirmed"
        />
      </div>
      <div>
        <Field name="comments" component={FormTextArea} label="Your comments" className="autotest-suggest-comments"/>
      </div>
      <div>
        <StandardButton label="Submit" disabled={!valid || submitting || pristine}
                        className="suggest-submit autotest-suggest-submit" buttonType={"submit"}/>
      </div>
    </form>
  </div>
;

export default SuggestAddressForm;
