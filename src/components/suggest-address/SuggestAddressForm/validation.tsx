/**
 * Client side validation requirements for the suggest an address form
 * @param values
 */
import { ISuggestAddressFormValues } from "../../../redux/actions/suggestAddressActions";
import { isNil, isEmpty, trim } from "lodash";

const validation = (values: ISuggestAddressFormValues) => {
  const errors: any = {};
  const isNilorEmpty = (value: string) => isNil(value) || isEmpty(trim(value));

  // Full name validation
  if (isNilorEmpty(values.fullName)) {
    errors.fullName = "Please enter your full name."
  } else if (!/^[a-zA-Z\s-']*$/.test(values.fullName)) {
    errors.fullName = "Please enter a valid name, avoiding characters such as #$%.";
  }

  // Email validation
  if (isNilorEmpty(values.email)) {
    errors.email = "Please enter your email."
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Please enter your email address in this format: name@example.com and avoid characters such as #$%"
  }

  // Street address validation
  if (isNilorEmpty(values.streetAddress)) {
    errors.streetAddress = "Please enter the street, PO Box or Community Box number."
  } else if (!/^[a-zA-Z\s0-9\-@_/']*$/.test(values.streetAddress)) {
    errors.streetAddress = "Please enter a valid street, PO Box or Community Box, avoiding characters such as #$%."
  }

  // Suburb validation
  if (isNilorEmpty(values.suburb)) {
    errors.suburb = "Please enter the suburb."
  } else if (!/^[a-zA-Z\s0-9\-@_']*$/.test(values.suburb)) {
    errors.suburb = "Please enter a valid suburb, avoiding characters such as #$%."
  }

  // City or town validation
  if (isNilorEmpty(values.cityOrTown)) {
    errors.cityOrTown = "Please enter the city or town."
  } else if (!/^[a-zA-Z\s0-9\-@_']*$/.test(values.cityOrTown)) {
    errors.cityOrTown = "Please enter a valid city or town, avoiding characters such as #$%."
  }

  return errors;
};

export default validation;