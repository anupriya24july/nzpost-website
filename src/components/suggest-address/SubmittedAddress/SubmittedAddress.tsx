import React from 'react';
import {ISuggestAddressFormValues} from "../../../redux/actions/suggestAddressActions";
import StandardCard from '../../lib/StandardCard/StandardCard';
import HeadingParagraphGroup from "../../lib/HeadingParagraphGroup/HeadingParagraphGroup";
import './SubmittedAddress.scss';
import {Table, TableBody, TableCell, TableRow} from "@material-ui/core";
import {FaInfoCircle} from "react-icons/fa";
import StandardButton from "../../lib/StandardButton/StandardButton";
// @ts-ignore
import functional from "react-functional";

interface ISubmmitedAddressProps extends ISuggestAddressFormValues {
  resetSuggestAddressForm: Function
}

const options = {
  componentWillUnmount: (props: ISubmmitedAddressProps) => {
    props.resetSuggestAddressForm();
  }
};

/**
 * Submitted address component displays a card component based on the output of suggest address forms
 * @param fullName
 * @param email
 * @param streetAddress
 * @param suburb
 * @param cityOrTown
 * @param addressConfirmed
 * @param comments
 * @constructor
 */
const SubmittedAddress: React.SFC<ISubmmitedAddressProps> = (
  {
    fullName,
    email,
    streetAddress,
    suburb,
    cityOrTown,
    addressConfirmed,
    comments
  }) =>
  <div className="submitted-address">
    <HeadingParagraphGroup
      heading="Thanks for sending us your address suggestion."
      paragraph="We’ll be in touch shortly to let you know next steps."
    />
    <StandardCard title="Submitted Address">
      <Table>
        <TableBody>
          <TableRow key="full-name">
            <TableCell scope="row" className="title">Name:</TableCell>
            <TableCell className="submitted-fullname">{fullName}</TableCell>
          </TableRow>
          <TableRow key="email">
            <TableCell scope="row" className="title">Email:</TableCell>
            <TableCell className="submitted-email">{email}</TableCell>
          </TableRow>
          <TableRow key="street-address" className="address">
            <TableCell scope="row" className="title">Address:</TableCell>
            <TableCell className="submitted-street">{streetAddress}</TableCell>
          </TableRow>
          <TableRow key="suburb" className="address">
            <TableCell scope="row" className="title"></TableCell>
            <TableCell className="submitted-suburb">{suburb}</TableCell>
          </TableRow>
          <TableRow key="city-or-town" className="address city-or-town">
            <TableCell scope="row" className="title"></TableCell>
            <TableCell className="submitted-city">{cityOrTown}</TableCell>
          </TableRow>
          <TableRow key="addressConfirmed">
            <TableCell colSpan={2} className="address-confirmed">
              <FaInfoCircle
                className="address-icon"/>{addressConfirmed === "true" ? "This is my address." : "This isn’t my address."}
            </TableCell>
          </TableRow>
          {comments &&
          <TableRow key="comments">
            <TableCell scope="row" className="title">Comments:</TableCell>
            <TableCell className="submitted-comments">{comments}</TableCell>
          </TableRow>
          }
        </TableBody>
      </Table>
    </StandardCard>

    <div className="button-container">
      <StandardButton label="Back to Address & postcode finder" href={"/apf"} className="back-to-apf" buttonType={"submit"}/>
    </div>
  </div>;
export default functional(SubmittedAddress, options);
