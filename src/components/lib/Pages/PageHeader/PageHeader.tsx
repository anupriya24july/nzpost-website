import * as React from "react";
import "./PageHeader.scss";

interface IPageHeaderProps {
  title: string
}

const PageHeader: React.SFC<IPageHeaderProps> = ({title}) =>
  <div className="page-header">
    <h3 className="page-header-title">{title}</h3>
  </div>
;

export default PageHeader;
