import * as React from "react";
import "./TwoColumnPage.scss";
import {ScreenSize} from "../../ScreenSize/ScreenSize";
import MediaQuery from "react-responsive";

export interface ITwoColumnPageProps {
  left: JSX.Element,
  right: JSX.Element
}

const TwoColumnPage: React.SFC<ITwoColumnPageProps> = ({left, right}) =>
  <div className="two-column-page">
    <div className="two-column-page-left">
      {left}
    </div>
    <MediaQuery minWidth={ScreenSize.Desktop}>
      <div className="two-column-page-right">
        {right}
      </div>
    </MediaQuery>
  </div>
;

export default TwoColumnPage;
