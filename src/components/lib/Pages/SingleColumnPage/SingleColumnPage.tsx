import * as React from "react";
import "./SingleColumnPage.scss";

interface ISingleColumnPageProps {
  header?: JSX.Element,
  body: JSX.Element
}

const SingleColumnPage: React.SFC<ISingleColumnPageProps> = ({header, body}) => {
  return (
    <div className="single-column-page">
      {header && (
        <div className="single-column-page-header">
          {header}
        </div>
      )}
      <div className="single-column-page-body">
        {body}
      </div>
    </div>
  );
};

export default SingleColumnPage;
