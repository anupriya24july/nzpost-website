import React from 'react';
import geolocationIcon from "./location-marker.svg";
import "./GeolocationIcon.scss"

const GeolocationIcon = () =>
  <img className="geolocation-icon-image" src={geolocationIcon}/>
;

export default GeolocationIcon;
