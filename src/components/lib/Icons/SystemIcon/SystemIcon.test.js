import React from "react";
import {SystemIcon} from "./SystemIcon";
import {shallow} from "enzyme";

describe("<SystemIcon />", () => {
  it("renders without crashing", () => {
    shallow(<SystemIcon name="close-circle-s"/>);
  });

  it("sets class names properly", () => {
    const systemIconClassName = "SystemIcon__ClassName";
    const wrapper = shallow(<SystemIcon className={systemIconClassName} name="close-circle-s" />);
    expect(wrapper.hasClass(systemIconClassName)).toBeTruthy();
  });
});