import React, {SFC} from "react";

import "./SystemIcon.scss";

const svgs = {
  "arrow-right-o": require("./svg/arrow-right-o.svg"),
  "close-circle-s": require("./svg/close-circle-s.svg"),
  "close-small-o": require("./svg/close-small-o.svg"),
  "marker-map-s": require("./svg/marker-map-s.svg"),
  "times-circle-s": require("./svg/times-circle-s.svg"),
  "arrow-left-o": require("./svg/arrow-left-o.svg"),
  "calendar-s": require("./svg/calendar-s.svg"),
  "locator-s": require("./svg/locator-s.svg"),
  "parcel-s": require("./svg/parcel-s.svg"),
};

export type Props = {
  className?: string;
  name: string;
  [name: string]: any;
};

export const SystemIcon: SFC<Props> = ({className, name, ...rest}) => {
  let classNameStr = `system-icon autotest-system-icon-${name}`;

  if (className) {
    classNameStr += ` ${className}`;
  }

  const SvgIcon = svgs[name];

  return (
    <SvgIcon className={classNameStr} {...rest} />
  );
};

export default SystemIcon;