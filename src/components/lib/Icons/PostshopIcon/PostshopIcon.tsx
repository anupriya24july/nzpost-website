import React from 'react';
import shopIcon from "./shop-icon.svg";
import "./PostshopIcon.scss"

// TODO: Design how we want to standardize use of DSM mapIcons
const PostshopIcon = () =>
  <img className="postshop-icon-image" src={shopIcon}/>
;

export default PostshopIcon;