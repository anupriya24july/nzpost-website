import React from "react";
import {shallow} from "enzyme";

import {Title} from "./Title";
import {getClassNameBySize} from "./helpers";
import * as classNames from "./Title.scss";

describe("<Title />", () => {
  it("renders without crashing", () => {
    shallow(<Title />);
  });

  it("sets class names properly", () => {
    const className = "Component__text";
    const wrapper = shallow(<Title className={className} />);
    expect(wrapper.hasClass(className)).toBeTruthy();
  });

  it("renders title text", () => {
    const titleText = "The best title on Earth"
    const wrapper = shallow(<Title>{titleText}</Title>);
    expect(wrapper.text()).toContain(titleText);
  });

  it("renders large title", () => {
    const size = "lg";
    const wrapper = shallow(<Title size={size}>Large Title</Title>);
    expect(wrapper.hasClass(getClassNameBySize(size))).toBeTruthy();
  });
});