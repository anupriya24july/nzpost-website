import {getClassNameBySize, getClassNameByColor} from "./helpers";

describe("getClassNameBySize()", () => {
  it("implemented", () => {
    expect(getClassNameBySize).toBeInstanceOf(Function);
  });

  it("given \"xl\" as an argument returns proper class name", () => {
    expect(getClassNameBySize("xl")).toEqual("title-size-xl");
  });

  it("given \"lg\" as an argument returns proper class name", () => {
    expect(getClassNameBySize("lg")).toEqual("title-size-lg");
  });

  it("given \"md\" as an argument returns proper class name", () => {
    expect(getClassNameBySize("md")).toEqual("title-size-md");
  });

  it("given \"sm\" as an argument returns proper class name", () => {
    expect(getClassNameBySize("sm")).toEqual("title-size-sm");
  });

  it("given \"xs\" as an argument returns proper class name", () => {
    expect(getClassNameBySize("xs")).toEqual("title-size-xs");
  });
});

describe("getClassByColor()", () => {
  it("implemented", () => {
    expect(getClassNameByColor).toBeInstanceOf(Function);
  });

  it("given \"nzPostRed\" returns proper class name", () => {
    expect(getClassNameByColor("redNzPost")).toEqual("title-color-red-nz-post");
  });
});