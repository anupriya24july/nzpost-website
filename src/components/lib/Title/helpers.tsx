export const getClassNameBySize = (size: string): string|undefined => ({
  xl: "title-size-xl",
  lg: "title-size-lg",
  md: "title-size-md",
  sm: "title-size-sm",
  xs: "title-size-xs",
}[size]);

export const getClassNameByColor = (color: string):string|undefined => ({
  redNzPost: "title-color-red-nz-post",
}[color]);