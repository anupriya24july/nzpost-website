import React, {SFC} from "react";

import "./Title.scss";
import {getClassNameBySize, getClassNameByColor} from "./helpers";

export type Props = {
  children: JSX.Element|string;
  color: string;
  className?: string;
  size: string;
}

export const defaultProps = {
  size: "md",
  color: "redNzPost",
};

export const Title: SFC<Props> = ({ children, color, className, size }) => {
  let classNameStr = `title ${getClassNameBySize(size)} ${getClassNameByColor(color)}`;

  if (className) {
    classNameStr += ` ${className}`;
  }

  return (
    <h1 className={classNameStr}>
      {children}
    </h1>);
}

Title.defaultProps = defaultProps;

export default Title;
