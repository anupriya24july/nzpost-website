import React from 'react';
import "./NotFoundCard.scss"

interface INotFoundCardProps {
  title: string,
  children: string,
}

/**
 * Reusable info card component with custom styling
 * @param children
 * @constructor
 */
const NotFoundCard = ({title, children}: INotFoundCardProps) => (
  <div className="not-found-card">
      <div className="not-found-card-title">{title}</div>
      <div className="not-found-card-body">{children}</div>
  </div>
);

export default NotFoundCard;