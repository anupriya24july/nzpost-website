export enum ScreenSize {
  DesktopWide = 1440,
  Desktop = 1024,
  Tablet = 640,
  Mobile = 320
}
