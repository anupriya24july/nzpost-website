import React, {SFC} from "react";

import "./SinglePageContainer.scss";

export type Props = {
  children: any;
  className?: string;
}

export const SinglePageContainer: SFC<Props> = ({ children, className }) => {
  let classNameStr = "single-page-container";

  if (className) {
    classNameStr += ` ${className}`;
  }

  return (<div className={classNameStr}>{children}</div>);
};

export default SinglePageContainer;
