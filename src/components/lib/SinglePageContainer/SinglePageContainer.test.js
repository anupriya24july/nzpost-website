import React from "react";
import {shallow} from "enzyme";

import {SinglePageContainer} from "./SinglePageContainer";

describe("<SinglePageContainer />", () => {
  it("renders without crashing", () => {
    shallow(<SinglePageContainer />);
  });

  it("renders child elements", () => {
    const Child1 = () => <h1>Child1</h1>;
    const Child2 = () => <h2>Child2</h2>;

    const wrapper = shallow(
      <SinglePageContainer>
        <Child1 />
        <Child2 />
      </SinglePageContainer>
    );

    expect(wrapper.contains([<Child1 />, <Child2 />])).toBeTruthy();
  });

  it("adds custom class names properly", () => {
    const className = "App__container";
    const wrapper = shallow(<SinglePageContainer className={className} />);
    expect(wrapper.hasClass(className)).toBeTruthy();
  });
});