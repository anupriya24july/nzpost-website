import React from 'react';
import {WrappedFieldProps} from "redux-form";
import {TextField} from "@material-ui/core";
import {BaseTextFieldProps} from "@material-ui/core/TextField";
import "./FormTextArea.scss";
import {FaExclamationCircle} from "react-icons/fa";

interface IFormTextProps extends BaseTextFieldProps, WrappedFieldProps {
}

/**
 * Reusable component for a form textarea input field
 */
const FormTextArea = ({input, autoFocus, label, className, meta: {active, touched, error, warning}, ...custom}: IFormTextProps) => {
  const activeClass = active ? "form-textarea-field-input-active" : "";
  const errorClass = touched && error ? "form-textarea-field-input-error" : "";
  const warningClass = touched && warning ? "form-textarea-field-input-warning" : "";

  const errorMessage = <span><FaExclamationCircle className="error-icon"/>{error}</span>;
  const warningMessage = <span><FaExclamationCircle className="warning-icon"/>{warning}</span>;
  return (
    <div className="form-textarea-field-container">
      <TextField
        className="form-textarea-field"
        variant="filled"
        label={label}
        error={touched && error != undefined}
        fullWidth
        margin="normal"
        multiline
        rows="5"
        {...input}
        {...custom}
        InputProps={{
          className: `form-textarea-field-input-container ${activeClass} ${errorClass} ${warningClass}`
        }}
        inputProps={{
          className: `form-textarea-field-input ${className}`
        }}
        InputLabelProps={{
          className: "form-textarea-field-label"
        }}
        autoFocus={autoFocus}
      />
      <div className={`form-textarea-field-error-message ${input.name}-error-message`}>
        {touched && ((error && errorMessage) || (warning && warningMessage))}
      </div>
    </div>
  );
}

export default FormTextArea;