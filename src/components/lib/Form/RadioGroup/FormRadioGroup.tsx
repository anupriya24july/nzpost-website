import React from "react";
import {WrappedFieldProps} from "redux-form";
import {FormControl, FormControlLabel, FormLabel, Radio, RadioGroup} from "@material-ui/core";
import "./FormRadioGroup.scss";

interface IFormRadioProps {
  label: string,
  value: any,
  className: string
}

interface IFormRadioGroupProps extends WrappedFieldProps {
  label: string,
  className: string,
  radios: IFormRadioProps[]
}

/**
 * Reusable component for defining a list of form radio options
 *
 * @param input
 * @param label
 * @param className
 * @param radios
 * @constructor
 */
const FormRadioGroup = ({input, label, className, radios}: IFormRadioGroupProps) => {
  return (
    <div className={`form-radio-group-container ${className}`}>
      <FormControl component="fieldset">
        <FormLabel component="legend" className="form-radio-group-label">{label}</FormLabel>
        <RadioGroup aria-label={label} name="addressConfirmed" value={input.value} onChange={(event, value) => input.onChange(value)}
                    className="form-radio-group">
          {radios.map((radio, i) => <FormControlLabel key={i} value={radio.value}
                                                      control={<Radio color="primary" className={radio.className}/>}
                                                      label={radio.label}
                                                      className="form-radio"/>)}
        </RadioGroup>
      </FormControl>
    </div>
  );
}

export default FormRadioGroup;