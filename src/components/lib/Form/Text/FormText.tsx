import React from 'react';
import {WrappedFieldProps} from "redux-form";
import {TextField} from "@material-ui/core";
import {BaseTextFieldProps} from "@material-ui/core/TextField";
import "./FormText.scss";
// import {FaExclamationCircle} from "react-icons/fa";
import FormTextError from "./FormTextError";

interface IFormTextProps extends BaseTextFieldProps, WrappedFieldProps {
}

/**
 * Reusable component for a form text input field
 */
const FormText = ({input, autoFocus, label, className, id, meta: {active, touched, error, warning}, ...custom}: IFormTextProps) => {
  const activeClass = active ? "form-text-field-input-active" : "";
  const errorClass = touched && error ? "form-text-field-input-error" : "";
  const warningClass = touched && warning ? "form-text-field-input-warning" : "";

  const errorComponent = error || warning ? <FormTextError name={input.name} touched={touched} error={error} warning={warning}/> : "";

  return (
    <div className="form-text-field-container">
      <TextField
        className="form-text-field"
        variant="filled"
        label={label}
        error={touched && error != undefined}
        fullWidth
        margin="normal"
        {...input}
        {...custom}
        InputProps={{
          className: `form-text-field-input-container ${activeClass} ${errorClass} ${warningClass}${id}`
        }}
        inputProps={{
          className: `form-text-field-input ${className}`
        }}
        InputLabelProps={{
          className: "form-text-field-label"
        }}
        autoFocus={autoFocus}
      />
      {touched && errorComponent}
    </div>
  )
};

export default FormText;