import React from 'react';
import "./FormText.scss";
import {FaExclamationCircle} from "react-icons/fa";
import {SystemIcon} from "./external";

/**
 * Reusable component for a form text input field error message
 */
const FormTextError = ({className, name, error, touched, warning}: any) => {
  const errorMessage = (
    <React.Fragment>
      <SystemIcon className="error-icon" name="times-circle-s" />
      <span>{error}</span>
    </React.Fragment>
  );

  const warningMessage = <span><FaExclamationCircle className="warning-icon"/>{warning}</span>;

  let classNameStr = `form-text-field-error-message ${name}-error-message`;

  if (className) {
    classNameStr += ` ${className}`;
  }

  return (
    <div className={classNameStr}>
      {touched && ((error && errorMessage) || (warning && warningMessage))}
    </div>
  )
};

export default FormTextError;