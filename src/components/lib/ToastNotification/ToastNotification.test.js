import React from "react";
import {shallow} from "enzyme";
import {ToastNotification} from "./ToastNotification";

describe("<ToastNotification />", () => {
  it("renders without crashing", () => {
    shallow(<ToastNotification />);
  });

  it("renders toast notification text", () => {
    const toastNotificationText = "Toast Notification Text";
    const wrapper = shallow(<ToastNotification message={toastNotificationText}/>);
    expect(wrapper.text()).toContain(toastNotificationText);
  });

  it("sets class names properly", () => {
    const toastNotificationClassName = "ToastNotification__ClassName";
    const wrapper = shallow(<ToastNotification message="msg" className={toastNotificationClassName} />);
    expect(wrapper.hasClass(toastNotificationClassName)).toBeTruthy();
  });

  it("applies styles for error notification type", () => {
    const errorClassName="toast-notification-error";
    const wrapper = shallow(<ToastNotification message="msg" type="error" />);
    expect(wrapper.find(`.${errorClassName}`)).toHaveLength(1);
  });

  it("calls onClickClose when clicking close icon/button", () => {
    const mockOnClickClose = jest.fn();
    const wrapper = shallow(<ToastNotification message="woa!" onClickClose={mockOnClickClose}/>);
    wrapper.find(".toast-notification-close-button").simulate("click");
    expect(mockOnClickClose).toHaveBeenCalledTimes(1);
  });

  it("not rendered when no message is provided", () => {
    const wrapper = shallow(<ToastNotification message="" />);
    expect(wrapper.get(0)).toBeFalsy();
  });
});