import React, {SFC} from "react";

import "./ToastNotification.scss";
import SystemIcon from "../Icons/SystemIcon/SystemIcon";

export type Props = {
  className?: string;
  message: string;
  type?: string;
  onClickClose?: () => void;
};

export const defaultProps = {
  onClickClose: () => {},
}

export const ToastNotification: SFC<Props> = ({className, message, type, onClickClose}) => {
  if (!message) {
    return null;
  }

  let containerClassNameStr = "toast-notification-container";
  let classNameStr = "toast-notification autotest-toast-notification";

  if (className) {
    containerClassNameStr += ` ${className}`;
  }

  switch (type) {
    case "error":
      classNameStr += " toast-notification-error autotest-toast-notification-error";
      break;
    default:
  }

  return (
    <div className={containerClassNameStr}>
      <span className={classNameStr}>
        <SystemIcon
          className="toast-notification-icon autotest-toast-notification-icon"
          name="close-circle-s"
        />

        <span className="toast-notification-message autotest-toast-notification-message">
          {message}
        </span>

        <SystemIcon
          className="toast-notification-close-button autotest-toast-notification-close-button"
          name="close-small-o"
          onClick={onClickClose}
        />
      </span>
    </div>
  );
};

ToastNotification.defaultProps = defaultProps;

export default ToastNotification;
