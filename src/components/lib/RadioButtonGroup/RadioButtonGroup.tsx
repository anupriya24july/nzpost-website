import React from "react";
import {FormControl, FormControlLabel, FormLabel, Radio, RadioGroup} from "@material-ui/core";
import "./RadioButtonGroup.scss";

interface IFormRadioXProps {
  label: string,
  value: any,
  className: string,
  disabled: boolean
}

interface IFormRadioGroupXProps {

  value: any,
  label?: string,
  className: string,
  radios: IFormRadioXProps[],
  onValueChange: any
}

/**
 * Reusable component for defining a list of form radio options
 *
 * @param input
 * @param label
 * @param className
 * @param radios
 * @constructor
 */
const RadioButtonGroup = ({value, label, className, radios, onValueChange}: IFormRadioGroupXProps) => {
  return (
    <div className={`radio-button-group-container ${className}`}>
      <FormControl component="fieldset">
        <FormLabel component="legend" className="radio-button-group-label">{label}</FormLabel>
        <RadioGroup aria-label={label} name="addressConfirmed" value={value} onChange={(event, value) => onValueChange(value)}
                    className="radio-button-group">
          {radios.map((radio, i) => {
            let radioClassName = radio.disabled ? radio.className + " redelivery-options-disabled" : radio.className;
            radioClassName = radio.value == value ? radioClassName + " redelivery-options-selected" : radioClassName;
            return (<FormControlLabel key={i} value={radio.value}
                                      control={<Radio color="primary" className={radioClassName}/>}
                                      label={radio.label}
                                      className={radioClassName} disabled={radio.disabled}/>)
          })}
        </RadioGroup>
      </FormControl>
    </div>
  );
}

export default RadioButtonGroup;