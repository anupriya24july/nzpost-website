export const getClassNameBySize = (size: string): string|undefined => ({
  xl: "subtitle-size-xl",
  lg: "subtitle-size-lg",
  md: "subtitle-size-md",
  sm: "subtitle-size-sm",
  xs: "subtitle-size-xs",
}[size]);

export const getClassNameByColor = (color: string): string|undefined => ({
  mirage: "subtitle-color-mirage",
}[color]);
