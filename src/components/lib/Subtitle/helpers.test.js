import {getClassNameBySize, getClassNameByColor} from "./helpers";

describe("getClassNameBySize()", () => {
  it("implemented", () => {
    expect(getClassNameBySize).toBeInstanceOf(Function);
  });

  it("given \"xl\" as an argument returns proper class name", () => {
    expect(getClassNameBySize("xl")).toEqual("subtitle-size-xl");
  });

  it("given \"lg\" as an argument returns proper class name", () => {
    expect(getClassNameBySize("lg")).toEqual("subtitle-size-lg");
  });

  it("given \"md\" as an argument returns proper class name", () => {
    expect(getClassNameBySize("md")).toEqual("subtitle-size-md");
  });

  it("given \"sm\" as an argument returns proper class name", () => {
    expect(getClassNameBySize("sm")).toEqual("subtitle-size-sm");
  });

  it("given \"xs\" as an argument returns proper class name", () => {
    expect(getClassNameBySize("xs")).toEqual("subtitle-size-xs");
  });
});

describe("getClassNameByColor()", () => {
  it("implemented", () => {
    expect(getClassNameByColor).toBeInstanceOf(Function);
  });

  it("given \"mirage\" as an argument returns proper class name", () => {
    expect(getClassNameByColor("mirage")).toEqual("subtitle-color-mirage");
  });
});