import React from "react";
import {shallow} from "enzyme";

import {Subtitle} from "./Subtitle";
import {getClassNameBySize} from "./helpers";
import * as classNames from "./Subtitle.scss";

describe("<Subtitle />", () => {
  it("renders without crashing", () => {
    shallow(<Subtitle />);
  });

  it("sets class names properly", () => {
    const className = "Component__text";
    const wrapper = shallow(<Subtitle className={className} />);
    expect(wrapper.hasClass(className)).toBeTruthy();
  });

  it("renders Subtitle text", () => {
    const subtitleText = "The best Subtitle on Earth"
    const wrapper = shallow(<Subtitle>{subtitleText}</Subtitle>);
    expect(wrapper.text()).toContain(subtitleText);
  });

  it("renders medium subtitle", () => {
    const size = "md";
    const wrapper = shallow(<Subtitle size={size}>Medium Subtitle</Subtitle>);
    expect(wrapper.hasClass(getClassNameBySize(size))).toBeTruthy();
  });
});