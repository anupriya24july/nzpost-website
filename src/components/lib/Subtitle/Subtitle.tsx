import React, {SFC} from "react";

import "./Subtitle.scss";
import {getClassNameBySize, getClassNameByColor} from "./helpers";

export type Props = {
  children: JSX.Element|string;
  className?: string;
  color?: string;
  size?: string;
}

export const defaultProps = {
  size: "md",
  color: "mirage",
};

export const Subtitle: SFC<Props> = ({ children, className, color, size }) => {
  let classNameStr = "subtitle";

  if (size) {
    classNameStr += ` ${getClassNameBySize(size)}`;
  }

  if (color) {
    classNameStr += ` ${getClassNameByColor(color)}`;
  }

  if (className) {
    classNameStr += ` ${className}`;
  }

  return (
    <h2 className={classNameStr}>
      {children}
    </h2>);
}

Subtitle.defaultProps = defaultProps;

export default Subtitle;
