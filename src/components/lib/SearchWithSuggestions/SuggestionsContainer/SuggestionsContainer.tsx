import React, {SFC} from "react";

import "./SuggestionsContainer.scss"

export type Props = {
  children: any,
  containerProps: any,
}

export const SuggestionsContainer: SFC<Props> = ({
  children,
  containerProps,
}) => (
  children &&
  <div
    {...containerProps}
    className="suggestions-container autotest-suggestions-container"
  >
    {children}
  </div>
);

export default SuggestionsContainer;
