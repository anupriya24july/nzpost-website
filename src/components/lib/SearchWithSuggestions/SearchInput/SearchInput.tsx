import React, {SFC} from "react";
import TextField from "@material-ui/core/TextField";
import "./SearchInput.scss";

export type SearchInputProps = {
  onChange: () => void,
  onPressEnter?: Function,
  value: string,
  ref: any,
  placeholder?: string
};

export const SearchInput: SFC<SearchInputProps> = ({
  onChange,
  onPressEnter,
  value,
  ref,
  placeholder,
  ...rest
}) => (
  <TextField
    onKeyPress={(evt) => {
      if (evt.key === 'Enter' && onPressEnter) {
        onPressEnter();
      }
    }}
    autoFocus
    type="search"
    variant="outlined"
    InputProps={{
      className: "search-field autotest-search-field",
      classes: {focused: "search-field-focused"},
      onChange,
      value,
    }}
    inputProps={{
      className: "search-field-input autotest-search-field-input",
      placeholder: placeholder,
      autoFocus: true,
    }}
    {...rest}
    inputRef={ref}
  />
);

export default SearchInput;
