import React from "react";
import {shallow} from "enzyme/build";

import Suggestion from "./Suggestion";

describe("<Suggestion />", () => {
  it("renders without crashing", () => {
    shallow(<Suggestion renderSuggestion={() => <span></span>} />);
  });
});