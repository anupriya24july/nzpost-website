import React, {SFC} from "react";
import ListItem from "@material-ui/core/ListItem/ListItem";

import "./Suggestion.scss";

export type Props = {
  isHighlighted?: boolean,
  renderSuggestion: any,
  suggestion: any,
}

export const Suggestion: SFC<Props> = ({
  suggestion, isHighlighted, renderSuggestion,
}) => (
  <ListItem
    dense
    button
    selected={isHighlighted}
    classes={{
      root: "suggestion autotest-suggestion"
    }}
  >
    {renderSuggestion({...suggestion})}
  </ListItem>
)

export default Suggestion;
