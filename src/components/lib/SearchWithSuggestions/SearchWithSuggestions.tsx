import React from "react";
import Autosuggest from "react-autosuggest";

import SearchInput from "./SearchInput/SearchInput";
import SuggestionsContainer from "./SuggestionsContainer/SuggestionsContainer";
import Suggestion from "./Suggestion/Suggestion";
import "./SearchWithSuggestions.scss";

export type Props = {
  className?: string,
  getSuggestionValue: any,
  onInputChange: (evt: any, {newValue, method}: { newValue: string, method: string }) => any,
  onInputPressEnter?: Function,
  onSuggestionSelected: (evt: any, {suggestion}: any) => any,
  onSuggestionsClearRequested: () => any,
  onSuggestionsFetchRequested: ({value}: { value: string }) => string,
  renderSuggestion: any,
  suggestions: Array<any>,
  value: string,
  placeholder?: string
}

export const defaultProps = {
  getSuggestionValue: () => {
  },
  onInputChange: () => {
  },
  onInputPressEnter: () => {
  },
  onSuggestionSelected: () => {
  },
  onSuggestionsClearRequested: () => {
  },
  onSuggestionsFetchRequested: () => {
  },
  suggestions: [],
  value: "",
};

type SearchWithSuggestionsState = {
  value: any
};

export class SearchWithSuggestions extends React.Component<Props, SearchWithSuggestionsState> {
  //shouldHandlePressEnter is set to false to avoid propagating to onInputPressEnter
  //useful to avoid submission when using the enter key to select a suggestion
  shouldHandlePressEnter: boolean;

  constructor(props: Props) {
    super(props);
    this.state = {
      value: props.value,
    };
    this.shouldHandlePressEnter = false;
  }

  static defaultProps = defaultProps;

  handleInputChange = (evt: any, {newValue, method}: { newValue: string, method: string }) => {
    this.setState({value: newValue}, this.props.onInputChange(evt, {newValue, method}));
  }

  handleInputPressEnter = () => {
    if (!this.props.onInputPressEnter) return;
    //Only submitEnter when shouldHandlePressEnter is true
    //or if we haven't selected a suggestion
    if (!this.state.value || this.shouldHandlePressEnter) {
      return this.props.onInputPressEnter();
    }
    this.shouldHandlePressEnter = true;
  }

  handleSuggestionSelect = (evt: any, {suggestion}: { suggestion: any }) => {
    //we want to avoid propagating the event to onInputPressEnter
    //on selection of the suggestion using the key `enter`
    if (evt.keyCode !== 13) {
      this.shouldHandlePressEnter = true;
    }
    this.setState({value: this.props.getSuggestionValue(suggestion)},
      () => this.props.onSuggestionSelected(evt, {suggestion})
    );
  }

  get className() {
    let className = "search-with-suggestions";
    if (this.props.className) {
      className += ` ${this.props.className}`;
    }
    return className;
  }

  render() {
    const {value} = this.state;
    const {placeholder} = this.props;

    const {
      onSuggestionsClearRequested,
      onSuggestionsFetchRequested,
      renderSuggestion,
      suggestions,
    } = this.props;

    const inputProps = {
      value,
      onChange: this.handleInputChange,
      onPressEnter: this.handleInputPressEnter,
      placeholder
    };

    return (
      <div className={this.className}>
        <Autosuggest
          suggestions={suggestions}
          onSuggestionSelected={this.handleSuggestionSelect}
          onSuggestionsFetchRequested={onSuggestionsFetchRequested}
          onSuggestionsClearRequested={onSuggestionsClearRequested}
          getSuggestionValue={this.props.getSuggestionValue}
          inputProps={inputProps}
          // render props
          renderInputComponent={SearchInput}
          renderSuggestion={(suggestion, {isHighlighted}) =>
            <Suggestion
              suggestion={suggestion}
              isHighlighted={isHighlighted}
              renderSuggestion={renderSuggestion}
            />
          }
          renderSuggestionsContainer={({containerProps, children}) =>
            <SuggestionsContainer
              containerProps={containerProps}
              children={children}
            />
          }
        />
      </div>
    );
  }
};

export default SearchWithSuggestions;
