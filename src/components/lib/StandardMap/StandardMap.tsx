import React from "react";
import {loadModules} from "react-arcgis";
import './StandardMap.scss';
import {Button} from "@material-ui/core";
import {isEqual, isUndefined} from "lodash";
import {isMobile} from 'react-device-detect';
import uuidV1 from "uuid/v1";
import StandardScreenIcon from "./StandardScreenIcon/StandardScreenIcon";
import MapView = __esri.MapView;
import WebMap = __esri.WebMap;
import ScreenPoint = __esri.ScreenPoint;

export interface IStandardPoint {
  latitude: number;
  longitude: number;
}

interface IStandardMapPoint {
  latitude: number;
  longitude: number;
}

export interface IStandardMapProps {
  center: IStandardMapPoint;
  showSatellite?: boolean;
  zoom?: number;
  showMapControl?: boolean;
  className?: string;
  mapIcons?: IStandardMapIcon[];
  shouldAdjustExtentToIcons?: boolean;
  onMapExtentChange: Function;
}

interface IStandardMapState {
  showSatellite: boolean;
  screenIcons: IStandardScreenIcon[];
  extentXMin: number;
  extentXMax: number;
  extentYMin: number;
  extentYMax: number;
}

export interface IStandardScreenIcon extends IStandardMapIcon {
  postOnClick: any;
  screenPoint: ScreenPoint;
  selected: boolean;
}

export interface IStandardMapIcon {
  id: string;
  point: IStandardPoint;
  createIconComponent: Function;
  iconWidth: string;
  iconHeight: string;
}

/**
 * StandardMap is an Arcgis wrapper for displaying a map enhanced with NZPost UI requirements
 *
 * @author Anu
 * @author MaxT
 */
class StandardMap extends React.Component<IStandardMapProps, IStandardMapState> {
  private id: string;
  private map: WebMap;
  private view: MapView;
  private selectedIconId: string;

  static defaultProps = {
    showSatellite: false,
    showMapControl: true,
    showGeolocation: false,
    showFullscreen: false,
    // For backward component api compatibility default value should be set to true
    shouldAdjustExtentToIcons: true,
    onMapExtentChange: () => {},
  };

  constructor(props: IStandardMapProps) {
    super(props);

    // generate map id
    this.id = `gis-map-${uuidV1()}`;

    // internal state, this.setState does not work in constructor
    this.state = {...this.state, showSatellite: props.showSatellite || false};

    // bind methods to this object context
    this.updatePostshopLocations = this.updatePostshopLocations.bind(this);
    this.updateState = this.updateState.bind(this);
    this.iconPostOnClick = this.iconPostOnClick.bind(this);
    this.renderMapIcons = this.renderMapIcons.bind(this);
    this.renderMapControl = this.renderMapControl.bind(this);
    this.toggleSatelliteOff = this.toggleSatelliteOff.bind(this);
    this.toggleSatelliteOn = this.toggleSatelliteOn.bind(this);
    this.toggleState = this.toggleState.bind(this);
    this.handleMapExtentChange = this.handleMapExtentChange.bind(this);
  }

  componentDidMount() {
    const {center, zoom, showMapControl} = this.props;
    loadModules([
      "esri/layers/TileLayer",
      "esri/views/MapView",
      "esri/WebMap",
      "esri/widgets/Fullscreen",
      "esri/widgets/Zoom",
      "esri/core/watchUtils"
    ]).then(([TileLayer, MapView, WebMap, Fullscreen, Zoom, watchUtils]) => {
        // build map
        this.map = new WebMap({
          basemap: this.props.showSatellite ? "satellite" : "streets-navigation-vector"
        });

        // build view that contains map
        this.view = new MapView({
          map: this.map,
          container: this.id,
          center: [center.longitude, center.latitude],
          constraints: {
            minZoom: 5,
            maxZoom: 20,
          },
          zoom: zoom,
          ui: {
            components: (["attribution"])
          }
        });

        // build layers
        const transportLayer = new TileLayer({
          id: "transport-layer",
          url:
            "http://server.arcgisonline.com/ArcGIS/rest/services/Reference/World_Transportation/MapServer"
        });

        this.map.layers.add(transportLayer);

        // FIXME: label visibility should be controlled by a toggle checkbox which will be implemented in future sprint
        this.map.findLayerById("transport-layer").visible = false;

        if (showMapControl) { // Show the fullscreen option in desktop
          const fullscreen = new Fullscreen({
            view: this.view
          });

          const zoom = new Zoom({
            view: this.view
          });

          if (!isMobile) {
            this.view.ui.add(fullscreen, "top-right");
          }

          this.view.ui.add(zoom, "bottom-right");
        }

        this.view.watch("viewpoint", this.updateState);
        this.view.watch("animation", this.updateState);

        //*******************************************************************
        // Watching properties for changes
        // 1. Watch view.stationary property
        // 2. Watch visible property of popup
        //*******************************************************************
        // Watch view's stationary property for becoming true.
        watchUtils.whenTrue(this.view, "stationary", () => {
          // Get the new extent of the view only when view is stationary.
          if (this.view.extent) {
            this.handleMapExtentChange();
          }
        });

        // show the postshop location on load
        this.updatePostshopLocations();
      }
    );
  }

  componentDidUpdate(prevProps: Readonly<IStandardMapProps>): void {
    // Draw and navigate
    const {mapIcons} = this.props;

    if (isUndefined(mapIcons) || isUndefined(prevProps.mapIcons)) {
      return;
    }

    const prevPoints = prevProps.mapIcons.map(icon => icon.point).sort();
    const points = mapIcons.map(icon => icon.point).sort();
    const newLocations = !isEqual(prevPoints, points);
    if (newLocations) {
      this.updatePostshopLocations();
    }
  }

  private handleMapExtentChange(): void {
    if (!this.view.extent) {
      return;
    }

    // This code is used to help to distinguish map extent change trigged by gotTo() 
    // from user initiated one
    const {xmin, xmax, ymin, ymax} = this.view.extent;
    const {extentXMin, extentXMax, extentYMin, extentYMax} = this.state;
    const isExtentUnchanged = xmin === extentXMin
      && xmax === extentXMax
      && ymin === extentYMin
      && ymax === extentYMax;
    if (isExtentUnchanged) {
      return;
    }

    loadModules(["esri/geometry/support/webMercatorUtils"]).then(([webMercatorUtils]) => {
      const {extent} = webMercatorUtils.webMercatorToGeographic(this.view.extent);
      const {xmin: lng1, xmax: lng2, ymin: lat1, ymax: lat2} = extent;
      this.setState(() => ({
        extentXMin: xmin,
        extentXMax: xmax,
        extentYMin: ymin,
        extentYMax: ymax,
      }), () => this.props.onMapExtentChange({lat1, lat2, lng1, lng2, zoom: this.view.zoom}));
    });
  }

  private updatePostshopLocations(): void {
    // Draw and navigate
    const {mapIcons, shouldAdjustExtentToIcons, zoom} = this.props;

    if (isUndefined(mapIcons) || isUndefined(this.view)) {
      return;
    }

    this.view.when(() => {
      loadModules(["esri/geometry/Point",
        "esri/Graphic"]).then(([Point]) => {
          // remove the screen icons
          this.setState({...this.state, screenIcons: []});

          // reset selected icon
          this.selectedIconId = "";

          // zoom in
          const mapPoints = mapIcons
            .filter(mapIcon => mapIcon.id !== "geolocation")
            .map(icon => new Point({latitude: icon.point.latitude, longitude: icon.point.longitude}));
          
          if (!shouldAdjustExtentToIcons) {
            // correlates with the state type EXTENT_LOCATIONS
            this.updateState();
          } else if (mapPoints && mapPoints.length === 1) {
            // correlates with the state type EXACT_LOCATION
            this.view
              .goTo({target: mapPoints, zoom: zoom}, {duration: 1000})
              .then(() => {
                /**
                 * This code is used to help to distinguish a map extent change
                 * trigged by invoking the view instance method "goTo" ("programmatical way") from
                 * a user initiated map zooming (by clicking zoom control or similar ui interactions)
                 */
                const {xmin: extentXMin, xmax: extentXMax, ymin: extentYMin, ymax: extentYMax} = this.view.extent;
                this.setState({extentXMin, extentXMax, extentYMin, extentYMax});
              });
          } else {
            // correlates with state type NEARBY_LOCATIONS
            this.view
              .goTo({target: mapPoints}, {duration: 1000})
              .then(() => {
                /**
                 * This code is used to help to distinguish a map extent change
                 * trigged by invoking the view instance method "goTo" ("programmatical way") from
                 * a user initiated map zooming (by clicking zoom control or similar ui interactions)
                 */
                const {xmin: extentXMin, xmax: extentXMax, ymin: extentYMin, ymax: extentYMax} = this.view.extent;
                this.setState({extentXMin, extentXMax, extentYMin, extentYMax});
              });
          }
        }
      );
    });
  }

  private updateState = () => {
    const {mapIcons} = this.props;

    if (isUndefined(mapIcons) || isUndefined(this.view)) {
      return;
    }

    loadModules(["esri/geometry/Point"]).then(([Point]) => {
      const screenIcons = mapIcons.map((icon: IStandardMapIcon) => {
        const mapPoint = new Point({latitude: icon.point.latitude, longitude: icon.point.longitude});
        const screenPoint = this.view.toScreen(mapPoint);
        const selected = icon.id === this.selectedIconId;
        return {...icon, screenPoint: screenPoint, postOnClick: this.iconPostOnClick, selected: selected};
      });

      this.setState({...this.state, screenIcons: screenIcons});
    });
  };

  private iconPostOnClick = (event: any) => {
    this.selectedIconId = event.target.id;
    this.updateState();
  }

  /**
   * Turns satellite view on
   */
  private toggleSatelliteOn = () => {
    loadModules(["esri/Basemap"])
      .then(([Basemap]) => {
        if (this.map.basemap.id != "satellite") {
          this.map.basemap = Basemap.fromId("satellite");
        }
        if (!this.state.showSatellite) {
          this.toggleState();
        }
      });
  }

  /**
   * Turns satellite view off
   */
  private toggleSatelliteOff = () => {
    loadModules(["esri/Basemap"])
      .then(([Basemap]) => {
        if (this.map.basemap.id != "streets-navigation-vector") {
          this.map.basemap = Basemap.fromId("streets-navigation-vector");
        }
        if (this.state.showSatellite) {
          this.toggleState();
        }
      });
  };

  /**
   * Sets showSatellite to its negation
   */
  private toggleState = () => {
    this.setState((state: IStandardMapState) => ({
        ...state,
        showSatellite: !state.showSatellite,
      })
    );
  };

  /**
   * Fetches extra, React component based UI elements
   */
  private renderMapControl(): JSX.Element | null {
    const {showMapControl} = this.props;
    let mapControl: JSX.Element | null = null;

    if (showMapControl) {
      const mapActiveClass = this.state.showSatellite ? "toggle-button--off" : "toggle-button--on";
      const satelliteActiveClass = this.state.showSatellite ? "toggle-button--on" : "toggle-button--off";

      mapControl = <div className="map-toggle-button-container">
        <Button className={`toggle-button ${mapActiveClass}`} onClick={this.toggleSatelliteOff}>
          Map
        </Button>
        <Button className={`toggle-button ${satelliteActiveClass}`} onClick={this.toggleSatelliteOn}>
          Satellite
        </Button>
      </div>
    }

    return mapControl;
  }

  private renderMapIcons() {
    if (!this.state.screenIcons) {
      return null;
    }

    return <div>
      {this.state.screenIcons.map(
        (screenIcon: IStandardScreenIcon) => <StandardScreenIcon key={uuidV1()} {...screenIcon}/>)
      }
    </div>
  }

  render() {
    return <div className="gis-map-container" id={this.id}>
      {this.renderMapControl()}
      {this.renderMapIcons()}
    </div>
  }

}

export default StandardMap;
