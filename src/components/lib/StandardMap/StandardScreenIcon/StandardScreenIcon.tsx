import * as React from "react";
import {CSSProperties} from "react";
import {IStandardScreenIcon} from "../StandardMap";
import "./StandardScreenIcon.scss";

const StandardScreenIcon: React.SFC<IStandardScreenIcon> = ({screenPoint, createIconComponent, postOnClick, selected}) => {
  const style: CSSProperties = {
    top: screenPoint.y,
    left: screenPoint.x
  }
  const selectedClassName = selected ? "standard-map-icon-selected" : "";
  return <div className={`standard-map-icon ${selectedClassName}`} style={style}>{createIconComponent(postOnClick)}</div>;
};

export default StandardScreenIcon;
