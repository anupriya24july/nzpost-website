import React from 'react';
import Card from '@material-ui/core/Card';
import "./StandardCard.scss"
import CardContent from "@material-ui/core/CardContent/CardContent";

interface IStandardCardProps {
  title: string,
  children: JSX.Element,
}

/**
 * Reusable info card component with custom styling
 * @param children
 * @constructor
 */
const StandardCard = ({title, children}: IStandardCardProps) => (
  <Card className="card">
    <CardContent className="card-content">
      <h3 className="card-content-title">{title}</h3>
      <div className="card-content-body">{children}</div>
    </CardContent>
  </Card>
);

export default StandardCard;