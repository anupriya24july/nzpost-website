import React from 'react';
import {Button} from "@material-ui/core";
import "./StandardButton.scss";
import {ButtonProps} from "@material-ui/core/Button";
import SystemIcon from "../Icons/SystemIcon/SystemIcon";

interface IButtonProps extends ButtonProps {
  label: string
  buttonType: string
  icon?: string
}

/**
 * Reusable component for defining a form submission button
 * @param label
 * @param className
 * @param rest
 * @constructor
 */
const StandardButton = ({label, className, buttonType, icon, ...rest}: IButtonProps) => (
    <div className={`button-container ${className}`}>
      <Button className={`button`} variant="extendedFab" type={buttonType} {...rest}>
        {label}
        {icon && <SystemIcon className="button-icon" name={"arrow-right-o"}/>}
      </Button>
    </div>
  )
;

export default StandardButton;