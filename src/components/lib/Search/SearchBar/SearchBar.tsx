import React from "react";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment/InputAdornment";
import "./SearchBar.scss"
import {ClearSearchAction, locatorSuggestionActions} from "../../../../redux/actions/postshop-locator/locatorSuggestionActions";
import IconButton from "@material-ui/core/IconButton";
import {ActionCreator} from "redux";
import searchIcon from "./search-icon.svg";
import clearIcon from "./clear-icon.svg";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";

/**
 * Properties for search field component
 */
interface IInputComponentProps {
  clearAllRequested: typeof locatorSuggestionActions.clearSearchRequested;
  searchRequested: Function;
  loadingSuggestions: boolean;
  autoFocus: boolean;
  className: string;
  placeholder: string;
  onChange: any;
  value: string;
  other: any;
  ref: any;
}

/**
 * Search icon for search. Docks to the left of search bar.
 * @constructor
 */
const SearchIcon: React.SFC = () => (
  <InputAdornment position="start" className="search-start-adornment">
    <img className="search-icon-image" alt="Search Icon" src={searchIcon}/>
  </InputAdornment>
);

/**
 * Loading spinner component used for search
 * @constructor
 */
const LoadingSpinner: React.SFC = () => (
  <InputAdornment position="end" className="search-end-adornment">
    <CircularProgress size={20} className="search-loading-spinner-image"/>
  </InputAdornment>
);

/**
 * Close icon for search. Docks to the right of search bar.
 * @param value determines whether to hide or show the icon
 * @param clearAllRequested
 * @constructor
 */
const ClearIcon: React.SFC<{
  value: string,
  clearAllRequested: typeof locatorSuggestionActions.clearSearchRequested
}> = ({value, clearAllRequested}) => (
  <InputAdornment position="end" className="search-end-adornment">
    {value &&
    <IconButton className="search-end-adornment-button" onClick={clearAllRequested}>
      <img className="search-clear-icon-image" alt="Clear Search Icon" src={clearIcon}/>
    </IconButton>
    }
  </InputAdornment>
);

const EndAdornment = (props: { loadingSuggestions: boolean, value: string, clearAllRequested: ActionCreator<ClearSearchAction> }) => (
  <div className="search-end-adornment">
    {props.loadingSuggestions
      ? <LoadingSpinner/>
      : <ClearIcon value={props.value} clearAllRequested={props.clearAllRequested}/>}
  </div>
);

/**
 * Reusable search bar component.
 * Input ref is used to define the anchoring point for any potential search result dropdown.
 * @param inputProps
 */
const SearchBar = ({autoFocus, onChange, value, loadingSuggestions, clearAllRequested, searchRequested, ref, ...other}: IInputComponentProps) =>
  <TextField
    autoFocus={autoFocus}
    onKeyPress={(ev) => {
      if (ev.key === 'Enter') {
        searchRequested(value)
      }
    }}
    type="search"
    variant="outlined"
    InputProps={{
      startAdornment: <SearchIcon/>,
      endAdornment: <EndAdornment clearAllRequested={clearAllRequested}
                                  loadingSuggestions={loadingSuggestions}
                                  value={value}/>,
      className: "locator-search-field",
      onChange: onChange,
      value: value,
      classes: {focused: "locator-search-field-focused"}
    }}
    inputProps={{
      className: "search-field-input"
    }}

    {...other}

    inputRef={ref}
  />
;

export default SearchBar;