import React from "react";
import "./DropdownResultWindow.scss"

interface ISearchResultAreaProps {
  containerProps: any,
  children: JSX.Element
}

/**
 * Component for rendering a generic drop down list of children resources (getLocations results or otherwise)
 * @param props
 * @constructor
 */
const DropdownResultWindow = (props: ISearchResultAreaProps) => (
  props.children &&
  <div {...props.containerProps} className="list-section">
    {props.children}
  </div>
);

export default DropdownResultWindow;