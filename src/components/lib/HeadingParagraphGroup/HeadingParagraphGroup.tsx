import React from 'react';
import './HeadingParagraphGroup.scss';

interface IHeadingParagraphGroupProps {
  heading: string,
  paragraph: string
}

const HeadingParagraphGroup = ({heading, paragraph}: IHeadingParagraphGroupProps) =>
  <div className="heading-paragraph-group">
    <h2 className="heading-paragraph-group-heading">{heading}</h2>
    <p className="heading-paragraph-group-paragraph">{paragraph}</p>
  </div>
;

export default HeadingParagraphGroup;