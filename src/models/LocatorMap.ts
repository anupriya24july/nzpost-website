/**
 * Model for defining a Postshop location object
 */
export interface MapCoordinate {
  latitude: number;
  longitude: number;
}
