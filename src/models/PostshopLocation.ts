/**
 * Model for defining a Postshop location object
 */
export interface PostshopLocation {
  name: string;
  lat: number;
  lng: number;
  phone?: string;
  address?: string;
  id?: number;
  description?: string;
  kiwibank_personal_region?: number;
  type?: string;
  hours?: (HoursEntity)[];
  // todo: create enum of potential services
  services?: (string)[] | null;
  address_details?: AddressDetails;
}

export interface HoursEntity {
  day: number;
  open: string;
  close: string;
  closed: boolean | null;
}

export interface AddressDetails {
  address_line_1: string;
  suburb: string;
  city: string;
  post_code: string;
}
