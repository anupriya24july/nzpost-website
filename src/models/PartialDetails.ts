// Interface for mapping partial detail API calls
export interface PartialDetails {
  success: boolean;
  details: DetailsEntity[]
}

export interface DetailsEntity {
  UniqueId: number;
  NZGD2kCoord: PostshopGeolocation;
  NZGD2kBBOX: NZGD2kBBOX;
  RoadName?: null;
  RoadTypeName?: null;
  RoadSuffixName?: null;
  SuburbName?: null;
  CityTown: string;
  Postcode?: null;
  FullPartial: string;
  BoxBagPostcode: boolean;
}

export interface PostshopGeolocation {
  type: string;
  crs: Crs;
  coordinates: number[]
}

export interface Crs {
  type: string;
  properties: Properties;
}

export interface Properties {
  code: string;
}

export interface NZGD2kBBOX {
  type: string;
  crs: Crs;
  coordinates?: (((number)[] | null)[] | null)[] | null;
}
