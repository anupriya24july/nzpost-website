import {AddressDetails, HoursEntity, PostshopLocation} from "./PostshopLocation";

export class PostshopLocationImpl implements PostshopLocation {
  private _address: string;
  private _address_details: AddressDetails;
  private _description: string;
  private _hours: HoursEntity[];
  private _id: number;
  private _kiwibank_personal_region: number;
  private _lat: number;
  private _lng: number;
  private _name: string;
  private _phone: string;
  private _services: string[] | null;
  private _type: string;

  constructor(name: string) {
    this._name = name;
  }

  get address(): string {
    return this._address;
  }

  get address_details(): AddressDetails {
    return this._address_details;
  }

  get description(): string {
    return this._description;
  }

  get hours(): HoursEntity[] {
    return this._hours;
  }

  get id(): number {
    return this._id;
  }

  get kiwibank_personal_region(): number {
    return this._kiwibank_personal_region;
  }

  get lat(): number {
    return this._lat;
  }

  get lng(): number {
    return this._lng;
  }

  get name(): string {
    return this._name;
  }

  get phone(): string {
    return this._phone;
  }

  get services(): string[] | null {
    return this._services;
  }

  get type(): string {
    return this._type;
  }
}