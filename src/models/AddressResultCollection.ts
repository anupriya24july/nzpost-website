export interface AddressResultCollection {
  success: boolean;
  addresses: AddressesEntity[];
  status?: string;
}

export interface AddressesEntity {
  DPID?: number;
  UniqueId?: number;

  // Partial API uses partial field
  FullPartial: string;

  // Suggest API uses full address
  FullAddress: string;
  SourceDesc: "Postal" | "Physical";
}

export interface Suggestion {
  name: string;
  address: string;
}
