// This is a helper function reducing boilerplate code
export const generateJSendError = (err: Error) => ({
  status: "error",
  message: err.message,
  data: {
    name: err.name,
  }
});

export type JSend = {
  status: string,
  data?: any,
  code?: string,
  message?: string
}

// Helper handler function
export const handleJSendByType = (
  jsend: JSend,
  successHandler: Function,
  failHandler: Function,
  errorHandler: Function,
) => {
  switch (jsend.status) {
    case "success":
      return successHandler(jsend);

    case "fail":
      return failHandler(jsend);

    case "error":
      return errorHandler(jsend);
  }
}