export namespace DomHelper {
  export const getElementByIdNotNull = (id: string) => {
    const element = document.getElementById(id);
    if (element == null) {
      throw "Element does not exist";
    }
    return element;
  };
}


