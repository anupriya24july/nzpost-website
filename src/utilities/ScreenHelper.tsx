import scrollIntoView from "scroll-into-view";
import {DomHelper} from "./DomHelper";
import {isEdge, isIE} from "./DetectBrowser";

export namespace ScreenHelper {
  export const scrollIntoScreenById = (id: string): void => {
    /**
     * This workaround is implemented to fix scrolling to selected
     * location on IE and Edge
     */
    if (isEdge || isIE) {
      DomHelper.getElementByIdNotNull(id).scrollIntoView();
      return;
    }
    
    scrollIntoView(DomHelper.getElementByIdNotNull(id));
  }

  export const scrollIntoScreenByElement = (element: any): void => {
    scrollIntoView(element);
  }
}


