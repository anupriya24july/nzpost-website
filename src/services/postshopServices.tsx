/**
 * Defines the different types of postal services based on enum string matching
 */
import {PostshopLocation} from "../models/PostshopLocation";
import mailboxIcon from "../components/lib/Icons/MapMarkerIcons/mailbox-marker.svg"
import postshopKiwiIcon from "../components/lib/Icons/MapMarkerIcons/post-kiwi-marker.svg"
import postshopIcon from "../components/lib/Icons/MapMarkerIcons/postshop-marker.svg"
import kiwibankIcon from "../components/lib/Icons/MapMarkerIcons/kiwibank-marker.svg"
import atmIcon from "../components/lib/Icons/MapMarkerIcons/atm-marker.svg"
import parcelIcon from "../components/lib/Icons/MapMarkerIcons/parcel-marker.svg"
import depotIcon from "../components/lib/Icons/MapMarkerIcons/depot-marker.svg"
import keyIcon from "../components/lib/Icons/MapMarkerIcons/key-icon.svg"
import {includes} from "lodash";
import {IStandardMapIcon, IStandardPoint} from "../components/lib/StandardMap/StandardMap";
import PostshopLocationMapIcon from "../components/PostshopLocator/PostshopLocationMapIcon/PostshopLocationMapIcon";
import React from "react";

export enum PostshopServiceType {
  box_lobby = "Box Lobby",
  post_a_parcel = "Post a parcel/drop off",
  purchase_postage = "Purchase postage",
  dropbox = "Dropbox",
  post_a_letter_standard = "Post a letter",
  post_a_letter_fastpost = "Post a letter",
  pay_a_bill = "Pay a bill",
  business_banking_services = "Business banking",
  atm = "ATM",
  standard_banking_services = "Standard banking",
  parcel_collection = "Parcel Collect",
  parcel_pod = "ParcelPod Locker",
  poste_restante = "Poste Restante",
  counter_delivery = "Counter delivery",
  passport_and_id_photos = "Passport & ID photos",
  deposits_and_withdrawals = "Kiwibank Transactions",
}

export interface IPostshopLocationIcon {
  url: string,
  width: string,
  height: string
}

/**
 * The postshop services class provides helper functionality for dealing with postshop services including filtering,
 * fetching relevant map markers, etc.
 *
 * @author MaxT
 */
class postshopServices {
  private static mailBoxIcon: IPostshopLocationIcon = {
    url: mailboxIcon,
    width: "48px",
    height: "50px"
  };

  private static depotMarker: IPostshopLocationIcon = {
    url: depotIcon,
    width: "48px",
    height: "50px"
  };

  /**
   * Returns correct svg icon to be displayed on a map
   * @param postshop
   */
  public static getIcon(postshop: PostshopLocation): IPostshopLocationIcon {
    // New section - names
    if (this.titleIncludes(postshop, "ATM")) {
      return {
        url: atmIcon,
        width: "48px",
        height: "50px"
      };
    }

    if (this.titleIncludes(postshop, "CourierPost")) {
      return this.depotMarker
    }

    if (postshop.name === "Posting box") {
      return this.mailBoxIcon;
    }

    return this.getMarkerByServices(postshop);
  }

  /**
   * Used for fetching a postshop marker by the specific service
   * @param postshop
   */
  private static getMarkerByServices(postshop: PostshopLocation): IPostshopLocationIcon {
    const standardPostMarker = {
      url: postshopIcon,
      width: "50px",
      height: "54.22px"
    };

    if (postshop.services) {
      const moreThanOneService = postshop.services.length > 1;
      const serviceIncludesBanking = postshop.services.indexOf("standard_banking_services") > -1
        && postshop.services.indexOf("deposits_and_withdrawals") > -1;

      // Just kiwibank
      if (postshop.services.length === 2 && serviceIncludesBanking) {
        return {
          url: kiwibankIcon,
          width: "50px",
          height: "54.22px"
        }
      }

      // Kiwibank + Postshop
      if (moreThanOneService && serviceIncludesBanking) {
        return {
          url: postshopKiwiIcon,
          width: "82px",
          height: "63px"
        };
      }

      if (moreThanOneService && !serviceIncludesBanking) {
        return standardPostMarker
      }

      // Key icon
      if (postshop.services.length <= 2 &&
        (postshop.services.indexOf("box_lobby") > -1 || postshop.services.indexOf("parcel_pod") > -1)) {
        return {
          url: keyIcon,
          width: "48px",
          height: "50px"
        };
      }

      if (this.postshopServiceOnlyHas(postshop, "parcel_collection")) {
        return {
          url: parcelIcon,
          width: "50px",
          height: "54.22px"
        }
      }

      if (this.postshopServiceOnlyHas(postshop, "post_a_letter_standard")
        || this.postshopServiceOnlyHas(postshop, "post_a_letter_fastpost")) {
        return this.mailBoxIcon;
      }

      // Post a parcel also return depot marker
      if (this.postshopServiceOnlyHas(postshop, "post_a_parcel")) {
        return this.depotMarker;
      }
    }

    return standardPostMarker;
  }

  /**
   * This method returns true if a postshop has a single service with specified term
   * @param postshop
   * @param searchTerm
   */
  private static postshopServiceOnlyHas(postshop: PostshopLocation, searchTerm: string): boolean {
    if (!postshop.services) {
      return false;
    }

    return postshop.services.length === 1 && postshop.services.indexOf(searchTerm) > -1;
  }

  /**
   * Returns true if the postshop in questions has specified search term within its title
   * @param postshop
   * @param text
   */
  private static titleIncludes(postshop: PostshopLocation, text: string) {
    return includes(postshop.name, text);
  }

  public static buildPostshopId(postshop: PostshopLocation) {
    return `postshop-${postshop.id}`;
  }

  public static buildPostshopIconId(postshop: PostshopLocation) {
    return `postshop-icon-${postshop.id}`;
  }

  public static buildPostshopLocationMapIcons(postshops: PostshopLocation[]): IStandardMapIcon[] {
    return postshops.map(postshop => {
      const point: IStandardPoint = {latitude: postshop.lat, longitude: postshop.lng};
      const postshopLocationIcon: IPostshopLocationIcon = postshopServices.getIcon(postshop);
      const id = postshopServices.buildPostshopIconId(postshop);
      return {
        id: id,
        point: point,
        createIconComponent: (postOnClick: Function) => <PostshopLocationMapIcon id={id} postshop={postshop} postOnClick={postOnClick}/>,
        iconWidth: postshopLocationIcon.width,
        iconHeight: postshopLocationIcon.height
      }
    });
  }
}

export default postshopServices;
