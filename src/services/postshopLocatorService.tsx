import {config} from "./external";

export interface IMapExtentInfo {
  latitude1: number,
  latitude2: number,
  longitude1: number,
  longitude2: number,
};

export interface IGetLocationsByKeyword {
  value: string,
};

export interface IGetLocationsByMapExtent {
  value: IMapExtentInfo,
};

export const handleErrors = (response: Response) => {
  if (!response.ok) {
    throw Error(response.statusText);
  }

  return response;
};

export const _getLocations = ({query}: { query: string }): Promise<Response> => {
  return fetch(`${config.services.postshopLocator.serverUrl}/locations?` + query, {
    method: 'GET',
    headers: {'Content-Type': 'application/json'},
  })
    .then(handleErrors)
    .then(response => response.json())
    .then(responseJson => responseJson.locations);
};

/**
 * Locator service layer is used for relevant API/transformation services regarding location data
 * TODO: Discover API endpoints through Hateoas + Zuul
 * @author Max
 */
export const postshopLocatorService = {
  /**
   * Get a list of suggestions for postshop location
   *
   * Integrates with existing location API to fetch postshop locations
   * @param query
   */
  getSuggestions(query: string): Promise<Response> {
    return fetch(`${config.services.postshopLocator.serverUrl}/suggestions?query=` + encodeURIComponent(query) + "&max=10", {
      method: 'GET',
      headers: {"Content-Type": "application/json"},
    })
      .then(handleErrors)
      .then(response => response.json())
      .then(responseJson => responseJson.suggestions);
  },

  getLocationsByKeyword({value}: IGetLocationsByKeyword): Promise<Response> {
    const query = `type=KEYWORD&value=${encodeURIComponent(value)}&max=5`;
    return _getLocations({query});
  },

  getLocationsByMapExtent({value}: IGetLocationsByMapExtent): Promise<Response> {
    const query = `type=MAP_EXTENT&value=${encodeURIComponent(JSON.stringify(value))}&max=50`;
    return _getLocations({query});
  },

  getLocationsByGeolocation(query: string) {
    return fetch(`${config.services.postshopLocator.serverUrl}/locations?` + query, {
      method: 'GET',
      headers: {"Content-Type": "application/json"},
    })
      .then(handleErrors)
      .then(response => response.json())
      .then(responseJson => responseJson.locations);
  }
};