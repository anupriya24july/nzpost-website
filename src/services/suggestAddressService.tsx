import {ISuggestAddressFormValues} from "../redux/actions/suggestAddressActions";
import {config} from "./external";

const handleErrors = (response: Response) => {
  if (!response.ok) {
    throw Error(response.statusText);
  }

  return response;
};

/**
 * Suggest Address Service handles API interaction
 *
 * @author MaxT
 * @author NickT
 */
export const suggestAddressService = {
  submitForm(formValues: ISuggestAddressFormValues): Promise<Response> {
    // Todo: Don't hardcode this. Discover through hypermedia. Go through Zuul. Don't use verbs as resource names
    return fetch(config.services.suggestAddress.serverUrl, {
      method: 'POST',
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify({
        name: formValues.fullName,
        email: formValues.email,
        address: `${formValues.streetAddress}, ${formValues.suburb}, ${formValues.cityOrTown}`,
        myaddress: `${formValues.addressConfirmed === "true" ? "Yes, this is my address." : "No, this is not my address."}`,
        notes: formValues.comments
      })
    }).then(handleErrors);
  }
};
