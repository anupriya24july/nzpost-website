import {config} from "./external";

export interface ICheckParcelNumber {
  value: string;
}

export interface IFetchAddressSuggestions {
  value: string;
}

export interface IGetRedeliveryDetails {
  trackingNumber: string;
  originalAddressId: string;
  newAddressId: string;
}

export const checkTrackingNumber = ({value}: ICheckParcelNumber) => {
  // Setup url here
  const url = `${config.services.trackingNumberCheck.serverUrl}?trackingNumber=` + encodeURIComponent(value);
  return fetch(url, {
    method: "GET",
    headers: {'Content-Type': 'application/json'},
  })
    .then(response => response.json())
    .then(responseJson => responseJson.parcelCheck);
};

export const fetchAddressSuggestions = ({value}: IFetchAddressSuggestions) => {
  const url = `${config.services.parcelAddress.serverUrl}?count=10&query=` + encodeURIComponent(value);
  return fetch(url, {
    method: "GET",
    headers: {'Content-Type': 'application/json'},
  })
    .then(response => response.json());
};

export const validateAddress = ({value}: { value: string }) => {
  const url = `${config.services.parcelAddress.serverUrl}?count=1&query=` + encodeURIComponent(value);
  return fetch(url, {
    method: "GET",
    headers: {'Content-Type': 'application/json'},
  })
    .then(response => response.json());
};

export const getRedeliveryDetails = ({trackingNumber, originalAddressId, newAddressId}: IGetRedeliveryDetails) => {
  const url = `${config.services.redeliveryDetails.serverUrl}?trackingNumber=` + encodeURIComponent(trackingNumber)
    + `&originalAddressId=` + encodeURIComponent(originalAddressId)
    + `&newAddressId=` + encodeURIComponent(newAddressId)
  ;
  return fetch(url, {
    method: "GET",
    headers: {'Content-Type': 'application/json'},
  })
    .then(response => response.json());
};

export default {
  checkTrackingNumber,
  fetchAddressSuggestions,
  validateAddress,
  getRedeliveryDetails
};